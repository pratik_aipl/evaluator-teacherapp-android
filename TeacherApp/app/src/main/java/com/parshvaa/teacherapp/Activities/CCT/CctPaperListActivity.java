package com.parshvaa.teacherapp.Activities.CCT;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Utils;

public class CctPaperListActivity extends BaseActivity {
    public ImageView img_back;
    public TextView tv_title;
    public String go_back_url = "";
    WebView web_cct;
    public String PaperUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cct_generate);

        web_cct = findViewById(R.id.web_cct);
        web_cct.setWebViewClient(new myWebClient());
        Utils.setWebViewSettings(web_cct);

        PaperUrl = getIntent().getStringExtra("PaperUrl");
        go_back_url = getIntent().getStringExtra("backUrl");

        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setText("ADD CCT PAPER");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        showProgress(true);
        if (Utils.isNetworkAvailableWebView(CctPaperListActivity.this)) {
            web_cct.loadUrl(PaperUrl);
        } else {
            Utils.showNetworkAlert(CctPaperListActivity.this);
        }
    }

    @Override
    public void onBackPressed() {
        if (web_cct.canGoBack()) {
            if (Utils.isNetworkAvailableWebView(CctPaperListActivity.this)) {
                web_cct.goBack();
            } else {
                Utils.showNetworkAlert(CctPaperListActivity.this);
            }
        } else {
            Intent intent = new Intent(CctPaperListActivity.this, CctAssignedActivity.class);
            startActivity(intent);
            finish();

            //  super.onBackPressed();

        }
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgress(true);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            if (Utils.isNetworkAvailableWebView(CctGenerateActivity.this)) {
//                view.loadUrl(url);
//            } else {
//                Utils.showNetworkAlert(CctGenerateActivity.this);
//            }
            return false;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            showProgress(false);
            if (url.equalsIgnoreCase(go_back_url)) {
                Intent intent = new Intent(CctPaperListActivity.this, CctAssignedActivity.class);
                startActivity(intent);
                finish();
            }
            if (url.equalsIgnoreCase("https://test.pemevaluater.parshvaa.com/auth/login")) {
                Intent intent = new Intent(CctPaperListActivity.this, CctAssignedActivity.class);
                startActivity(intent);
                finish();

            }
        }
    }
}


