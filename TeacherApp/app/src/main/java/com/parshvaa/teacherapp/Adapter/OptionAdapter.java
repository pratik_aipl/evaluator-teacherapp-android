package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;


import com.parshvaa.teacherapp.Models.OptionBean;
import com.parshvaa.teacherapp.OptionsWebView;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pratik on 04/12/18.
 */
public class OptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "OptionAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<OptionBean> optionBeans;
    private RadioButton mCheckAddressTemp = null;
    private int lastCheckedPos = 0;
    private int answerID = 0;
    boolean isStudent;
    int isCorrect;

    public OptionAdapter(Context context, List<OptionBean> optionBeans, boolean isStudent, int answerID, int isCorrect) {
        this.context = context;
        this.optionBeans = optionBeans;
        this.isStudent = isStudent;
        this.answerID = answerID;
        this.isCorrect = isCorrect;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_question_option_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        OptionBean beanData = optionBeans.get(position);
//        holder.mOptionsText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        holder.mOptionsText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        holder.mOptionsText.setScrollbarFadingEnabled(false);
        holder.mOptionsText.setInitialScale(30);
        holder.mOptionsText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        holder.mRadioBtn.setText(Utils.getOptionText(position));
        holder.mViewContainer.bringToFront();

//        Log.d(TAG, "onBindViewHolder: " + beanData.getOptions());
//        Log.d(TAG, "onBindViewHolder: " + beanData.getOptions().replace("<p>","<p color: #5ECE88 >"));
        if (isStudent) {
            if (beanData.getIsCorrect().equalsIgnoreCase("1")) {
                holder.mAnsAction.setVisibility(View.VISIBLE);
                holder.mAnsAction.setImageResource(R.mipmap.rigth);
                holder.mRadioBtn.setTextColor(ContextCompat.getColor(context, R.color.green));
                holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions().replace("<p>", "<p style=\"color: #5ECE88;\">"), "#5ECE88");
            } else if (answerID == beanData.getMCQOptionID() && isCorrect == 0) {
                holder.mAnsAction.setVisibility(View.GONE);
                holder.mRadioBtn.setTextColor(ContextCompat.getColor(context, R.color.colorRed));
                holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions().replace("<p>", "<p style=\"color: #FF0000;\">"), "#FF0000");
            } else {
                holder.mAnsAction.setVisibility(View.GONE);
                holder.mRadioBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions().replace("<p>", "<p style=\"color: #000000;\">"), "#000000");
            }

        } else {
            holder.mAnsAction.setVisibility(View.GONE);
            if (beanData.isSelected()) {
                mCheckAddressTemp = holder.mRadioBtn;
                lastCheckedPos = position;
            } else {

                holder.mRadioBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions().replace("<p>", "<p style=\"color: #000000;\">"), "#000000");

                if (position == 0)
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
                else if (position == optionBeans.size() - 1)
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
                else
                    holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
            }
        }


    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return optionBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mViewContainer)
        public LinearLayout mViewContainer;
        @BindView(R.id.mOptionsText)
        public OptionsWebView mOptionsText;
        @BindView(R.id.mRadioBtn)
        public RadioButton mRadioBtn;
        @BindView(R.id.mOptionLabelView)
        public ImageView mOptionLabelView;
        @BindView(R.id.mAnsAction)
        public ImageView mAnsAction;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
