package com.parshvaa.teacherapp.Models;

import java.io.Serializable;

public class MainUser implements Serializable {
    public String id = "";
    public String username = "";
    public String first_name = "";
    public String last_name = "";
    public String register_mobile = "";
    public String email = "";

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String ClassID="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getRegister_mobile() {
        return register_mobile;
    }

    public void setRegister_mobile(String register_mobile) {
        this.register_mobile = register_mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getClassLogo() {
        return ClassLogo;
    }

    public void setClassLogo(String classLogo) {
        ClassLogo = classLogo;
    }

    public String getClassRoundLogo() {
        return ClassRoundLogo;
    }

    public void setClassRoundLogo(String classRoundLogo) {
        ClassRoundLogo = classRoundLogo;
    }

    public String ClassName = "";
    public String ClassLogo = "";
    public String ClassRoundLogo = "";

}
