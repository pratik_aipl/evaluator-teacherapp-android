package com.parshvaa.teacherapp.Models;

import java.io.Serializable;
import java.util.ArrayList;



public class Standard implements Serializable {
    public String StandardID = "";
    public String BoardName = "";
    public String MediumName = "";

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String StandardName = "";

    public ArrayList<Subject> subjectsArray = new ArrayList<>();
}
