package com.parshvaa.teacherapp.Activities.CCT;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.Paper.PaperAssigned;
import com.parshvaa.teacherapp.Activities.Reports.ReportActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Constant;

import java.util.List;

/**
 * Created by empiere-vaibhav on 6/12/2018.
 */

public class CctAssignedAdapter extends RecyclerView.Adapter<CctAssignedAdapter.MyViewHolder> {

    private static final String TAG = "CctAssignedAdapter";
    public Context context;
    private List<CctPaper> optList;
    String type;

    public CctAssignedAdapter(List<CctPaper> moviesList, Context context, String type) {
        this.optList = moviesList;
        this.context = context;
        this.type = type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_cct_assigned_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_standard.setText(optList.get(position).getStandardName());
        holder.tv_subject.setText(optList.get(position).getSubjectName());
        holder.tv_medium.setText(optList.get(position).getMediumName());
        holder.tv_assign_dt.setText(optList.get(position).getCreatedOn());

        Log.d(TAG, "onBindViewHolder: "+optList.get(position).getStandardName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                if (type.equalsIgnoreCase("Report")) {
                    intent = new Intent(context, ReportActivity.class);
                    intent.putExtra(Constant.title,"CCT Report");
                } else {
                    intent = new Intent(context, CctGenerateActivity.class);
                    intent.putExtra(Constant.fromAssign,true);
                    intent.putExtra("PlannerID", optList.get(position).getClassCCTPlannerID());
                }
                intent.putExtra("PaperId", optList.get(position).getClassMCQTestHDRID());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
//                ((Activity) context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return optList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_standard, tv_exam_dt, tv_assign_dt, tv_medium, tv_subject;

        public MyViewHolder(View view) {
            super(view);
            tv_standard = (TextView) view.findViewById(R.id.tv_standard);
            tv_subject = (TextView) view.findViewById(R.id.tv_subject);
            tv_medium = (TextView) view.findViewById(R.id.tv_medium);
            tv_assign_dt = (TextView) view.findViewById(R.id.tv_assign_dt);
            tv_exam_dt = (TextView) view.findViewById(R.id.tv_exam_dt);

        }
    }
}
