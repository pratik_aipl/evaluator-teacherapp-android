package com.parshvaa.teacherapp.Activities.SendApproval;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Activities.CCT.CctPaper;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssigned;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssignedAdapter;
import com.parshvaa.teacherapp.Activities.SendApproval.Fragment.CctFragment;
import com.parshvaa.teacherapp.Activities.SendApproval.Fragment.PaperFragment;
import com.parshvaa.teacherapp.Activities.SendApproval.Fragment.RACCTFragment;
import com.parshvaa.teacherapp.Activities.SendApproval.Fragment.RAPAPERFragment;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.Standard;
import com.parshvaa.teacherapp.Models.Subject;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.MyCustomTypeface;
import com.parshvaa.teacherapp.Utils.RobotoTextView;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SendApprovalActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "SendApprovalActivity";
    public SendApprovalActivity instance;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public ImageView img_back, img_filter;
    public TextView tv_title, tv_exam_date, tv_assign_date, tv_apply, tv_clear;
    public PaperAssignedAdapter adapter;
    public PaperAssigned paperAssigneds;
    public JsonParserUniversal jParser;
    public CctPaper cctPaper;
    public ArrayList<Standard> standardArray = new ArrayList<>();
    public ArrayList<String> strStandardArray = new ArrayList<>();
    public Standard standard;
    public Spinner sp_standard, sp_subject;
    public ArrayList<Subject> subjectArray = new ArrayList<>();
    public ArrayList<String> strSubjectArray = new ArrayList<>();
    public Subject subject;
    public String standard_id = "", subject_id = "", assign_date = "", exam_date = "";
    public DatePickerDialog datePickerDialog;
    public int selectedStd = 0, selectedSub = 0;
    public ViewPagerAdapter Vadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_approval);
        viewPager = findViewById(R.id.viewpager);
        instance = this;
        jParser = new JsonParserUniversal();
        setupViewPager(viewPager);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("SEND FOR APPROVAL");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        setupTabFont();
        standard = new Standard();
        subject = new Subject();

        img_filter = findViewById(R.id.img_filter);
        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Filter();

            }
        });
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        // new CallRequest(instance).readyForApproval(standard_id, subject_id, assign_date, exam_date);
    }

    public void Filter() {
        final Dialog dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_paper_assigned);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.popup_window_animation_phone;
        sp_subject = dialog.findViewById(R.id.sp_subject);
        sp_standard = dialog.findViewById(R.id.sp_standard);
        tv_assign_date = dialog.findViewById(R.id.tv_assign_date);
        tv_exam_date = dialog.findViewById(R.id.tv_exam_date);
        tv_apply = dialog.findViewById(R.id.tv_apply);
        tv_clear = dialog.findViewById(R.id.tv_clear);

        new CallRequest(instance).get_standard();
        standard.setStandardName("Select Standard");
        standard.setStandardID("0");
        strStandardArray.add(standard.getStandardName());
        standardArray.add(standard);
        sp_standard.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strStandardArray));

      /*  subject.setSubjectName("Select Subject");
        subject.setSubjectID("0");
        strSubjectArray.add(subject.getSubjectName());
        subjectArray.add(subject);
        sp_subject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strSubjectArray));
*/
        tv_assign_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) instance.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(instance,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String sdate = addZero(dayOfMonth) +
                                        "-" + addZero(monthOfYear + 1) + "-" + year;
                                tv_assign_date.setText(sdate);
                            }
                        }, mYear, mMonth, mDay);
                Calendar calendar = Calendar.getInstance();
                calendar.set(mYear, mMonth, mDay - 14);
                datePickerDialog.show();
            }
        });

        tv_exam_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) instance.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(instance,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String sdate = addZero(dayOfMonth) + "-" + addZero(monthOfYear + 1) + "-" +
                                        "-" + year;
                                tv_exam_date.setText(sdate);
                            }
                        }, mYear, mMonth, mDay);
                Calendar calendar = Calendar.getInstance();
                calendar.set(mYear, mMonth, mDay - 14);
                datePickerDialog.show();
            }
        });

        sp_standard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));

                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        standard_id = standardArray.get(position).getStandardID();
                        showProgress(true);
                        new CallRequest(instance).get_subject(standard_id);
                        selectedStd = position;

                    } else {
                        standard_id = "0";
                        subject.setSubjectName("Select subject");
                        subject.setSubjectID("0");
                        strSubjectArray.add(subject.getSubjectName());
                        subjectArray.add(subject);
                        sp_subject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strSubjectArray));
                        showProgress(true);
                        new CallRequest(instance).get_subject(standard_id);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        sp_subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        subject_id = subjectArray.get(position).getSubjectID();
                        selectedSub = position;

                    } else {
                        subject_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (!exam_date.equals("")) {
            tv_exam_date.setText(exam_date);
        }
        if (!assign_date.equals("")) {
            tv_assign_date.setText(assign_date);
        }
        tv_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                exam_date = tv_exam_date.getText().toString();
                assign_date = tv_assign_date.getText().toString();
                if (sp_standard.getSelectedItemPosition() == 0) {
                    selectedStd = 0;
                    standard_id = "";
                }
                if (sp_subject.getSelectedItemPosition() == 0) {
                    selectedSub = 0;
                    subject_id = "";
                }
                if (tv_assign_date.getText().toString().isEmpty()) {
                    assign_date = "";
                }
                if (tv_exam_date.getText().toString().isEmpty()) {
                    exam_date = "";
                }

                Log.d(TAG, "CurrentPosition>>> "+viewPager.getCurrentItem());
                if (viewPager.getCurrentItem() == 0) {
                    ((PaperFragment) Vadapter.getItem(viewPager.getCurrentItem())).callingReadyForApproval(standard_id, subject_id, assign_date, exam_date);
                }
                if (viewPager.getCurrentItem() == 1){
                    ((CctFragment) Vadapter.getItem(viewPager.getCurrentItem())).callingCCtForApproval(standard_id, subject_id, assign_date);
                }
                if (viewPager.getCurrentItem() == 2){
                    ((RACCTFragment) Vadapter.getItem(viewPager.getCurrentItem())).callingRACCT(standard_id, subject_id, assign_date);
                }
                if (viewPager.getCurrentItem() == 3){
                    ((RAPAPERFragment) Vadapter.getItem(viewPager.getCurrentItem())).callingRAPAPER(standard_id, subject_id, assign_date, exam_date);
                }

            }
        });

        tv_clear.setOnClickListener(view -> {
            selectedStd = 0;
            selectedSub = 0;
            exam_date = "";
            assign_date = "";
            tv_exam_date.setText(null);
            tv_assign_date.setText(null);
            tv_exam_date.setHint("Select exam date");
            tv_assign_date.setHint("Select assign date");
            sp_subject.setSelection(selectedSub);
            sp_standard.setSelection(selectedStd);
        });
        dialog.show();
    }

    public String addZero(int n) {
        if (n <= 9) {
            return "0" + n;
        } else {
            return "" + n;
        }
    }

    private void setupTabFont() {

        RobotoTextView mcqTest = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_report_tab, null);
        mcqTest.setText("GENERATE PAPER");
        tabLayout.getTabAt(0).setCustomView(mcqTest);

        RobotoTextView practicePaper = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_report_tab, null);
        practicePaper.setText("CCT PAPER");
        tabLayout.getTabAt(1).setCustomView(practicePaper);

        RobotoTextView RACCT = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_report_tab, null);
        RACCT.setText("REVIEW & APPROVED(CCT)");
        tabLayout.getTabAt(2).setCustomView(RACCT);

        RobotoTextView RAPAPER = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_report_tab, null);
        RAPAPER.setText("REVIEW & APPROVED(PAPER)");
        tabLayout.getTabAt(3).setCustomView(RAPAPER);
    }

    private void setupViewPager(ViewPager viewPager) {
        Vadapter = new ViewPagerAdapter(getSupportFragmentManager());
        Vadapter.addFragment(new PaperFragment(), "GENERATE PAPER");
        Vadapter.addFragment(new CctFragment(), "CCT PAPER");
        Vadapter.addFragment(new RACCTFragment(), "REVIEW & APPROVED(CCT)");
        Vadapter.addFragment(new RAPAPERFragment(), "REVIEW & APPROVED(PAPER)");

        viewPager.setAdapter(Vadapter);
        viewPager.setOnTouchListener(null);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case readyForApproval:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            App.paperAssignedArrayList.clear();
                            App.cctPaperArrayList.clear();
                            JSONArray jData = jObj.getJSONArray("assigned_paper");
                            if (jData.length() > 0 || jData != null) {
                                for (int i = 0; i < jData.length(); i++) {
                                    paperAssigneds = (PaperAssigned) jParser.parseJson(jData.getJSONObject(i), new PaperAssigned());

                                    App.paperAssignedArrayList.add(paperAssigneds);
                                }
                            }
                            finish();
                            startActivity(getIntent());
                            //   Utils.showToast(jObj.getString("message"), instance);
                        }
                        break;

                    case get_standard:
                        try {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                standardArray.clear();
                                strStandardArray.clear();
                                subjectArray.clear();
                                strSubjectArray.clear();
                                if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                    JSONArray jDataArray = jObj.getJSONArray("data");
                                    if (jDataArray != null && jDataArray.length() > 0) {
                                        standard.setStandardName("Select standard");
                                        standard.setStandardID("0");
                                        strStandardArray.add(standard.getStandardName());
                                        standardArray.add(standard);
                                        subject.setSubjectName("Select subject");
                                        subject.setSubjectID("0");
                                        strSubjectArray.add(subject.getSubjectName());
                                        subjectArray.add(subject);
                                        for (int i = 0; i < jDataArray.length(); i++) {
                                            JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                            standard = (Standard) jParser.parseJson(jStudyAbord, new Standard());
                                            strStandardArray.add(standard.getStandardName());
                                            standardArray.add(standard);
                                        }
                                        sp_standard.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strStandardArray));
                                        sp_standard.setSelection(selectedStd);

                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case get_subject:
                        try {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                subjectArray.clear();
                                strSubjectArray.clear();
                                if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                    JSONArray jDataArray = jObj.getJSONArray("data");
                                    if (jDataArray != null && jDataArray.length() > 0) {
                                        subject.setSubjectName("Select subject");
                                        subject.setSubjectID("0");
                                        strSubjectArray.add(subject.getSubjectName());
                                        subjectArray.add(subject);
                                        for (int i = 0; i < jDataArray.length(); i++) {
                                            JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                            subject = (Subject) jParser.parseJson(jStudyAbord, new Subject());
                                            strSubjectArray.add(subject.getSubjectName());
                                            subjectArray.add(subject);
                                        }
                                        sp_subject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strSubjectArray));
                                        sp_subject.setSelection(selectedSub);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
