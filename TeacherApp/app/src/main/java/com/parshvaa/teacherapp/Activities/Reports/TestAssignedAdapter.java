package com.parshvaa.teacherapp.Activities.Reports;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.CCT.CctGenerateActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Constant;

import java.util.List;


public class TestAssignedAdapter extends RecyclerView.Adapter<TestAssignedAdapter.MyViewHolder> {

    private static final String TAG = "CctAssignedAdapter";
    public Context context;
    private List<TestPaper> testPapers;
    String type;

    public TestAssignedAdapter(List<TestPaper> moviesList, Context context, String type) {
        this.testPapers = moviesList;
        this.context = context;
        this.type = type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_cct_assigned_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_standard.setText(testPapers.get(position).getStandardName());
        holder.tv_subject.setText(testPapers.get(position).getSubjectName());
        holder.tv_medium.setText(testPapers.get(position).getMediumName());
        holder.tv_assign_dt.setText(testPapers.get(position).getGenratedDate());

        Log.d(TAG, "onBindViewHolder: " + testPapers.get(position).getStandardName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(context, ReportActivity.class);
                intent.putExtra(Constant.title,"Test Report");
                intent.putExtra("PaperId", testPapers.get(position).getClassQuestionPaperID());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
//                ((Activity) context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return testPapers.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_standard, tv_exam_dt, tv_assign_dt, tv_medium, tv_subject;

        public MyViewHolder(View view) {
            super(view);
            tv_standard = (TextView) view.findViewById(R.id.tv_standard);
            tv_subject = (TextView) view.findViewById(R.id.tv_subject);
            tv_medium = (TextView) view.findViewById(R.id.tv_medium);
            tv_assign_dt = (TextView) view.findViewById(R.id.tv_assign_dt);
            tv_exam_dt = (TextView) view.findViewById(R.id.tv_exam_dt);

        }
    }
}
