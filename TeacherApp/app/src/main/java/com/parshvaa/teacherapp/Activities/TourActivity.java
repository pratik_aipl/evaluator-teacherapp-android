package com.parshvaa.teacherapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.teacherapp.Adapter.MyViewPagerAdapter;
import com.parshvaa.teacherapp.Models.Tour;
import com.parshvaa.teacherapp.R;

public class TourActivity extends AppCompatActivity {
    private ImageView[] dots;
    private int dotsCount;
    ViewPager viewPager;
    public LinearLayout pager_indicator;
    public TextView tv_skip;
    public Button btn_join;
    public TourActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour);
        instance=this;
        viewPager = findViewById(R.id.viewpager);
        tv_skip = findViewById(R.id.tv_skip);
        btn_join = findViewById(R.id.btn_join);
        viewPager.setAdapter(new MyViewPagerAdapter(this));
        pager_indicator = findViewById(R.id.viewPagerCountDots);
        setUiPageViewController();
        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, LoginActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });
        btn_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int Pos = viewPager.getCurrentItem();
                if (Pos + 1 < dotsCount) {
                    Log.i("TAG IF", "POS : " + Pos + " DOTSCOUNT : " + dotsCount);
                    viewPager.setCurrentItem(Pos + 1);
                } else {
                    Log.i("TAG ELSE", "POS : " + Pos + " DOTSCOUNT : " + dotsCount);
                    startActivity(new Intent(instance, LoginActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }


            }
        });
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 4) {
                    btn_join.setText("JOIN NOW");

                } else {
                    btn_join.setText("NEXT");
                }
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
                }
                dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void setUiPageViewController() {

        dotsCount = Tour.values().length;
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(8, 0, 8, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

}
