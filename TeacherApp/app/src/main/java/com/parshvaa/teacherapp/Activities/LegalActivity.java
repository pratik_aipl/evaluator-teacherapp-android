package com.parshvaa.teacherapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.R;

public class LegalActivity extends AppCompatActivity {
    public LegalActivity instance;
    public ImageView img_back;
    public TextView tv_title, tv_terms, tv_privacy, tv_linces;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);

        instance = this;
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_terms = (TextView) findViewById(R.id.tv_terms);
        tv_privacy = (TextView) findViewById(R.id.tv_privacy);
        tv_linces = (TextView) findViewById(R.id.tv_linces);

        tv_title.setText("Legal");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_linces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, StaticWebPageActivity.class)
                        .putExtra("action", "l"));
            }
        });
        tv_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, StaticWebPageActivity.class)
                        .putExtra("action", "p"));
            }
        });
        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, StaticWebPageActivity.class)
                        .putExtra("action", "t"));
            }
        });
    }
}
