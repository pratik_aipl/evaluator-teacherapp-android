package com.parshvaa.teacherapp.Activities.SendApproval.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Activities.CCT.CctPaper;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssigned;
import com.parshvaa.teacherapp.Activities.SendApproval.Adapter.SendCctAdapter;
import com.parshvaa.teacherapp.Activities.SendApproval.Adapter.SendPaperAdapter;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.Standard;
import com.parshvaa.teacherapp.ObservScroll.BaseFragment;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class RAPAPERFragment extends BaseFragment implements AsynchTaskListner {
    private static final String TAG = "RAPAPERFragment";
    View v;
    RAPAPERFragment instance;
    public RecyclerView rv_gen_paper;
    public SendPaperAdapter adapter;
    public PaperAssigned paperAssigneds;
    public JsonParserUniversal jParser;
    public LinearLayout empty_view;
    public TextView tv_empty;
    public Standard standard;
    public String standardid = "", subjectid = "", assigndate = "", examdate = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_paper, container, false);
        instance = RAPAPERFragment.this;
        jParser = new JsonParserUniversal();
        rv_gen_paper = v.findViewById(R.id.rv_gen_paper);
        tv_empty = v.findViewById(R.id.tv_empty);
        empty_view = v.findViewById(R.id.empty_view);
        showProgress(true);
        new CallRequest(RAPAPERFragment.this).GetREVIEWAPPROVEDPAPER("", "", "", "");
        return v;
    }

    public void setupRecyclerView() {
        final Context context = rv_gen_paper.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rv_gen_paper.setLayoutAnimation(controller);
        rv_gen_paper.scheduleLayoutAnimation();
        rv_gen_paper.setLayoutManager(new LinearLayoutManager(context));
        adapter = new SendPaperAdapter(App.paperAssignedArrayList, context);
        rv_gen_paper.setAdapter(adapter);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case GetREVIEWAPPROVEDPAPER:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            App.paperAssignedArrayList.clear();
                            App.cctPaperArrayList.clear();
                            Log.d(TAG, "RAPAPER>> "+result);
                            JSONArray jData = jObj.getJSONArray("approved_paper");
                            if (jData.length() > 0 || jData != null) {
                                for (int i = 0; i < jData.length(); i++) {
                                    paperAssigneds = (PaperAssigned) jParser.parseJson(jData.getJSONObject(i), new PaperAssigned());
                                    App.paperAssignedArrayList.add(paperAssigneds);
                                }
                                rv_gen_paper.setVisibility(View.VISIBLE);
                                setupRecyclerView();
                                empty_view.setVisibility(View.GONE);

                            } else {
                                empty_view.setVisibility(View.VISIBLE);
                                rv_gen_paper.setVisibility(View.GONE);
                                tv_empty.setText("No Data");
                                Toast.makeText(getActivity(), ""+jObj.getString("message"), Toast.LENGTH_SHORT).show();

                            }
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void callingRAPAPER(String standard_id, String subject_id, String assign_date, String exam_date) {
        standardid = standard_id;
        subjectid = subject_id;
        assigndate = assign_date;
        examdate = exam_date;
        if(App.paperAssignedArrayList.size()==0){
            Toast.makeText(getActivity(), "Data Not Available", Toast.LENGTH_SHORT).show();
        }else{
            callRequest(standardid,subjectid,assigndate,examdate);
        }

    }

    private void callRequest(String standardid, String subjectid, String assigndate, String examdate) {
        showProgress(true);
        new CallRequest(RAPAPERFragment.this).GetREVIEWAPPROVEDPAPER(standardid, subjectid, assigndate, examdate);
    }

}
