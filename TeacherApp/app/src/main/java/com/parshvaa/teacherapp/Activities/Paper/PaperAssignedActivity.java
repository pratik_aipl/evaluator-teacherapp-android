package com.parshvaa.teacherapp.Activities.Paper;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.Standard;
import com.parshvaa.teacherapp.Models.Subject;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.MyCustomTypeface;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PaperAssignedActivity extends BaseActivity implements AsynchTaskListner {

    public PaperAssignedActivity instance;
    public ImageView img_back, img_filter;
    public TextView tv_title, tv_exam_date, tv_assign_date, tv_apply, tv_clear, tv_empty;
    public RecyclerView rv_paper_assigned;
    public PaperAssignedAdapter adapter;
    public ArrayList<PaperAssigned> paperAssignedArrayList = new ArrayList<>();
    public PaperAssigned paperAssigneds;
    public JsonParserUniversal jParser;
    public ArrayList<Standard> standardArray = new ArrayList<>();
    public ArrayList<String> strStandardArray = new ArrayList<>();
    public Standard standard;
    public Spinner sp_standard, sp_subject;
    public ArrayList<Subject> subjectArray = new ArrayList<>();
    public ArrayList<String> strSubjectArray = new ArrayList<>();
    public Subject subject;
    public String standard_id = "", subject_id = "", assign_date = "", exam_date = "";
    public DatePickerDialog datePickerDialog;
    public int selectedStd = 0, selectedSub = 0;
    public LinearLayout empty_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paper_assigned);

        instance = this;
        jParser = new JsonParserUniversal();
        rv_paper_assigned = findViewById(R.id.rv_paper_assigned);
        img_back = findViewById(R.id.img_back);
        img_filter = findViewById(R.id.img_filter);
        tv_title = findViewById(R.id.tv_title);
        tv_empty = findViewById(R.id.tv_empty);
        empty_view = findViewById(R.id.empty_view);
        tv_title.setText("PAPER ASSIGNED");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Filter();
            }
        });
        standard = new Standard();
        subject = new Subject();
        showProgress(true);
        new CallRequest(instance).assignedPaper(standard_id, subject_id, assign_date, exam_date);
    }

    public void setupRecyclerView() {
        final Context context = rv_paper_assigned.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rv_paper_assigned.setLayoutAnimation(controller);
        rv_paper_assigned.scheduleLayoutAnimation();
        rv_paper_assigned.setLayoutManager(new LinearLayoutManager(context));
        adapter = new PaperAssignedAdapter(paperAssignedArrayList, context);
        rv_paper_assigned.setAdapter(adapter);

    }

    public void Filter() {
        final Dialog dialog = new Dialog(PaperAssignedActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_paper_assigned);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.popup_window_animation_phone;
        sp_subject = dialog.findViewById(R.id.sp_subject);
        sp_standard = dialog.findViewById(R.id.sp_standard);
        tv_assign_date = dialog.findViewById(R.id.tv_assign_date);
        tv_exam_date = dialog.findViewById(R.id.tv_exam_date);
        tv_apply = dialog.findViewById(R.id.tv_apply);
        tv_clear = dialog.findViewById(R.id.tv_clear);

        new CallRequest(instance).get_standard();
        standard.setStandardName("Select Standard");
        standard.setStandardID("0");
        strStandardArray.add(standard.getStandardName());
        standardArray.add(standard);
        sp_standard.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strStandardArray));

        tv_assign_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) PaperAssignedActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                final int mYear = c.get(Calendar.YEAR); // current year
                final int mMonth = c.get(Calendar.MONTH); // current month
                final int mDay = c.get(Calendar.DAY_OF_MONTH);


                datePickerDialog = new DatePickerDialog(PaperAssignedActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                             /*   mmYear = year;
                                mmMonth = monthOfYear + 1;
                                mmDay = dayOfMonth;
                            */
                                String sdate = addZero(dayOfMonth) +
                                        "-" + addZero(monthOfYear + 1) + "-" + year;
                                tv_assign_date.setText(sdate);

                            }
                        }, mYear, mMonth, mDay);
                Calendar calendar = Calendar.getInstance();
                calendar.set(mYear, mMonth, mDay - 14);
                datePickerDialog.show();
            }
        });


        tv_exam_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) PaperAssignedActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(PaperAssignedActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String sdate = addZero(dayOfMonth) + "-" + addZero(monthOfYear + 1) + "-" +
                                        "-" + year;
                                tv_exam_date.setText(sdate);
                            }
                        }, mYear, mMonth, mDay);
                Calendar calendar = Calendar.getInstance();
                calendar.set(mYear, mMonth, mDay - 14);
                datePickerDialog.show();
            }
        });

        sp_standard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(PaperAssignedActivity.this, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));

                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(PaperAssignedActivity.this, "fonts/Roboto-Regular.ttf")));
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        standard_id = standardArray.get(position).getStandardID();
                        showProgress(true);
                        new CallRequest(instance).get_subject(standard_id);
                        selectedStd = position;

                    } else {
                        standard_id = "0";
                        subject.setSubjectName("Select subject");
                        subject.setSubjectID("0");
                        strSubjectArray.add(subject.getSubjectName());
                        subjectArray.add(subject);
                        sp_subject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strSubjectArray));
                        showProgress(true);
                        new CallRequest(instance).get_subject(standard_id);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        sp_subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(PaperAssignedActivity.this, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(PaperAssignedActivity.this, "fonts/Roboto-Regular.ttf")));
                        subject_id = subjectArray.get(position).getSubjectID();
                        selectedSub = position;

                    } else {
                        subject_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (!exam_date.equals("")) {
            tv_exam_date.setText(exam_date);
        }
        if (!assign_date.equals("")) {
            tv_assign_date.setText(assign_date);
        }
        tv_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                exam_date = tv_exam_date.getText().toString();
                assign_date = tv_assign_date.getText().toString();
                if (sp_standard.getSelectedItemPosition() == 0) {
                    selectedStd = 0;
                    standard_id = "";
                }
                if (sp_subject.getSelectedItemPosition() == 0) {
                    selectedSub = 0;
                    subject_id = "";
                }
                if (tv_assign_date.getText().toString().isEmpty()) {
                    assign_date = "";
                }
                if (tv_exam_date.getText().toString().isEmpty()) {
                    exam_date = "";
                }
                showProgress(true);
                new CallRequest(instance).assignedPaper(standard_id, subject_id, assign_date, exam_date);


            }
        });
        tv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedStd = 0;
                selectedSub = 0;
                exam_date = "";
                assign_date = "";
                tv_exam_date.setText(null);
                tv_assign_date.setText(null);
                tv_exam_date.setHint("Select exam date");
                tv_assign_date.setHint("Select assign date");
                sp_subject.setSelection(selectedSub);
                sp_standard.setSelection(selectedStd);
            }

        });
        dialog.show();
    }

    public String addZero(int n) {
        if (n <= 9) {
            return "0" + n;
        } else {
            return "" + n;
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case assignedPaper:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            paperAssignedArrayList.clear();
                            JSONArray jData = jObj.getJSONArray("assigned_paper");
                            if (jData.length() > 0 && jData != null) {
                                for (int i = 0; i < jData.length(); i++) {
                                    paperAssigneds = (PaperAssigned) jParser.parseJson(jData.getJSONObject(i), new PaperAssigned());
                                    paperAssignedArrayList.add(paperAssigneds);
                                }
                                setupRecyclerView();
                                rv_paper_assigned.setVisibility(View.VISIBLE);
                                empty_view.setVisibility(View.GONE);

                            } else {
                                empty_view.setVisibility(View.VISIBLE);
                                rv_paper_assigned.setVisibility(View.GONE);
                                tv_empty.setText("No Paper Assigned by Institute");
                            }
                        }
                        break;
                    case get_standard:
                        try {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                standardArray.clear();
                                strStandardArray.clear();
                                if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                    JSONArray jDataArray = jObj.getJSONArray("data");
                                    if (jDataArray != null && jDataArray.length() > 0) {
                                        standard.setStandardName("Select standard");
                                        standard.setStandardID("0");
                                        strStandardArray.add(standard.getStandardName());
                                        standardArray.add(standard);
                                        subject.setSubjectName("Select subject");
                                        subject.setSubjectID("0");
                                        strSubjectArray.add(subject.getSubjectName());
                                        subjectArray.add(subject);
                                        sp_subject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strSubjectArray));

                                        for (int i = 0; i < jDataArray.length(); i++) {
                                            JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                            standard = (Standard) jParser.parseJson(jStudyAbord, new Standard());
                                            strStandardArray.add(standard.getStandardName());
                                            standardArray.add(standard);
                                        }
                                        sp_standard.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strStandardArray));
                                        sp_standard.setSelection(selectedStd);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case get_subject:
                        try {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                subjectArray.clear();
                                strSubjectArray.clear();
                                subject.setSubjectName("Select subject");
                                subject.setSubjectID("0");
                                strSubjectArray.add(subject.getSubjectName());
                                subjectArray.add(subject);

                                if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                    JSONArray jDataArray = jObj.getJSONArray("data");
                                    if (jDataArray != null && jDataArray.length() > 0) {
                                        for (int i = 0; i < jDataArray.length(); i++) {
                                            JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                            subject = (Subject) jParser.parseJson(jStudyAbord, new Subject());
                                            strSubjectArray.add(subject.getSubjectName());
                                            subjectArray.add(subject);
                                        }

                                        sp_subject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strSubjectArray));
                                        sp_subject.setSelection(selectedSub);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
