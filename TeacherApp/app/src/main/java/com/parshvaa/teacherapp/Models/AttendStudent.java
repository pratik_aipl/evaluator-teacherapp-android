package com.parshvaa.teacherapp.Models;

import java.io.Serializable;
import java.util.List;

public class AttendStudent implements Serializable {

    int StudentID;
    int StudentCode;
    String FirstName;
    String LastName;
    String AvgTime;
    int TotalQquestion;
    int Correct;
    int Incorrect;
    int TotalAttempt;
    List<LevelQuestion> Question;

    public String getAvgTime() {
        return AvgTime;
    }

    public void setAvgTime(String avgTime) {
        AvgTime = avgTime;
    }

    public List<LevelQuestion> getQuestion() {
        return Question;
    }

    public void setQuestion(List<LevelQuestion> question) {
        Question = question;
    }

    public int getStudentCode() {
        return StudentCode;
    }

    public void setStudentCode(int studentCode) {
        StudentCode = studentCode;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getTotalQquestion() {
        return TotalQquestion;
    }

    public void setTotalQquestion(int totalQquestion) {
        TotalQquestion = totalQquestion;
    }

    public int getCorrect() {
        return Correct;
    }

    public void setCorrect(int correct) {
        Correct = correct;
    }

    public int getIncorrect() {
        return Incorrect;
    }

    public void setIncorrect(int incorrect) {
        Incorrect = incorrect;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public int getTotalAttempt() {
        return TotalAttempt;
    }

    public void setTotalAttempt(int totalAttempt) {
        TotalAttempt = totalAttempt;
    }

}
