package com.parshvaa.teacherapp.Activities.Reports;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.CCT.CctAssignedActivity;
import com.parshvaa.teacherapp.Activities.CCT.CctAssignedAdapter;
import com.parshvaa.teacherapp.Activities.CCT.CctPaper;
import com.parshvaa.teacherapp.Activities.CCT.fragment.CctAssignedFragment;
import com.parshvaa.teacherapp.Activities.CCT.fragment.GeneratePaperFragment;
import com.parshvaa.teacherapp.Activities.Reports.fragment.CctPaperReportFragment;
import com.parshvaa.teacherapp.Activities.Reports.fragment.TestPaperReportFragment;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.Standard;
import com.parshvaa.teacherapp.Models.Subject;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.MyCustomTypeface;
import com.parshvaa.teacherapp.Utils.RobotoTextView;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CctReportListActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "CctAssignedActivity";
    public CctReportListActivity instance;
    public ImageView img_back, img_filter;
    public TextView tv_title, tv_assign_date, tv_apply, tv_clear, tv_empty;
    public RecyclerView rv_cct_assigned;
    public CctAssignedAdapter adapter;
    public ArrayList<CctPaper> cctPaperArrayList = new ArrayList<>();
    public CctPaper cctPaper;
    public JsonParserUniversal jParser;
    public Spinner sp_standard, sp_subject;
    public ArrayList<Subject> subjectArray = new ArrayList<>();
    public ArrayList<String> strSubjectArray = new ArrayList<>();
    public Subject subject;
    public String standard_id = "", subject_id = "", assign_date = "", exam_date = "";
    public DatePickerDialog datePickerDialog;
    public int selectedStd = 0, selectedSub = 0;
    public Standard standard;
    public ArrayList<Standard> standardArray = new ArrayList<>();
    public ArrayList<String> strStandardArray = new ArrayList<>();
    public LinearLayout empty_view;
    public ViewPagerAdapter Vadapter;
    RelativeLayout lin_body;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cct_assigned);
        instance = this;
        jParser = new JsonParserUniversal();
        rv_cct_assigned = findViewById(R.id.rv_cct_assigned);
        rv_cct_assigned.setVisibility(View.GONE);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        lin_body = findViewById(R.id.lin_body);

        lin_body.setVisibility(View.VISIBLE);
        img_back = findViewById(R.id.img_back);
        img_filter = findViewById(R.id.img_filter);
        tv_title = findViewById(R.id.tv_title);
        tv_empty = findViewById(R.id.tv_empty);
        empty_view = findViewById(R.id.empty_view);

        tv_title.setText("Paper Report");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_filter.setVisibility(View.GONE);
        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Filter();
            }
        });
        setupViewPager(viewPager);
    }


    private void setupViewPager(ViewPager viewPager) {
        Vadapter = new ViewPagerAdapter(getSupportFragmentManager());
        Vadapter.addFragment(new CctPaperReportFragment(), "CCT Paper");
        Vadapter.addFragment(new TestPaperReportFragment(), "Test Paper");

        viewPager.setAdapter(Vadapter);
        viewPager.setOnTouchListener(null);
        setupTabFont();
    }

    private void setupTabFont() {

        RobotoTextView mcqTest = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_report_tab, null);
        mcqTest.setText("CCT Paper");
        tabLayout.getTabAt(0).setCustomView(mcqTest);

        RobotoTextView practicePaper = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_report_tab, null);
        practicePaper.setText("Test Paper");
        tabLayout.getTabAt(1).setCustomView(practicePaper);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void Filter() {
        final Dialog dialog = new Dialog(CctReportListActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_cct_assigned);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.popup_window_animation_phone;
        sp_subject = dialog.findViewById(R.id.sp_subject);
        sp_standard = dialog.findViewById(R.id.sp_standard);
        tv_assign_date = dialog.findViewById(R.id.tv_assign_date);
        tv_apply = dialog.findViewById(R.id.tv_apply);
        tv_clear = dialog.findViewById(R.id.tv_clear);
        standard = new Standard();
        subject = new Subject();
        standard.setStandardName("Select Standard");
        standard.setStandardID("0");
        strStandardArray.add(standard.getStandardName());
        standardArray.add(standard);
        sp_standard.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strStandardArray));
        showProgress(true);
        new CallRequest(instance).get_standard();


        tv_assign_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) CctReportListActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(CctReportListActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String sdate = addZero(dayOfMonth) +
                                        "-" + addZero(monthOfYear + 1) + "-" + year;
                                tv_assign_date.setText(sdate);
                            }
                        }, mYear, mMonth, mDay);
                Calendar calendar = Calendar.getInstance();
                calendar.set(mYear, mMonth, mDay - 14);
                datePickerDialog.show();
            }
        });


        sp_standard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(CctReportListActivity.this, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));

                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(CctReportListActivity.this, "fonts/Roboto-Regular.ttf")));
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        standard_id = standardArray.get(position).getStandardID();
                        Log.i("standard_id", "==>" + standard_id);
                        showProgress(true);
                        new CallRequest(instance).get_subject(standard_id);
                        selectedStd = position;

                    } else {
                        standard_id = "0";
                        subject.setSubjectName("Select subject");
                        subject.setSubjectID("0");
                        strSubjectArray.add(subject.getSubjectName());
                        subjectArray.add(subject);
                        sp_subject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strSubjectArray));
                        showProgress(true);
                        new CallRequest(instance).get_subject(standard_id);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sp_subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(CctReportListActivity.this, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(CctReportListActivity.this, "fonts/Roboto-Regular.ttf")));
                        subject_id = subjectArray.get(position).getSubjectID();
                        selectedSub = position;

                    } else {
                        subject_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        if (!assign_date.equals("")) {
            tv_assign_date.setText(assign_date);
        }
        tv_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                assign_date = tv_assign_date.getText().toString();
                if (sp_standard.getSelectedItemPosition() == 0) {
                    selectedStd = 0;
                    standard_id = "";
                }
                if (sp_subject.getSelectedItemPosition() == 0) {
                    selectedSub = 0;
                    subject_id = "";
                }
                if (tv_assign_date.getText().toString().isEmpty()) {
                    assign_date = "";
                }
                showProgress(true);
                new CallRequest(instance).assignedCctPaper(standard_id, subject_id, assign_date);
            }
        });
        tv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedStd = 0;
                selectedSub = 0;
                assign_date = "";
                tv_assign_date.setText(null);
                tv_assign_date.setHint("Select assign date");
                sp_subject.setSelection(selectedSub);
                sp_standard.setSelection(selectedStd);
            }

        });
        dialog.show();
    }

    public String addZero(int n) {
        if (n <= 9) {
            return "0" + n;
        } else {
            return "" + n;
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {

                    case get_standard:
                        try {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                standardArray.clear();
                                strStandardArray.clear();
                                subjectArray.clear();
                                strSubjectArray.clear();
                                if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                    JSONArray jDataArray = jObj.getJSONArray("data");
                                    if (jDataArray != null && jDataArray.length() > 0) {
                                        standard.setStandardName("Select standard");
                                        standard.setStandardID("0");
                                        strStandardArray.add(standard.getStandardName());
                                        standardArray.add(standard);
                                        subject.setSubjectName("Select subject");
                                        subject.setSubjectID("0");
                                        strSubjectArray.add(subject.getSubjectName());
                                        subjectArray.add(subject);
                                        sp_subject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strSubjectArray));
                                        for (int i = 0; i < jDataArray.length(); i++) {
                                            JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                            standard = (Standard) jParser.parseJson(jStudyAbord, new Standard());
                                            strStandardArray.add(standard.getStandardName());
                                            standardArray.add(standard);
                                        }
                                        sp_standard.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strStandardArray));
                                        sp_standard.setSelection(selectedStd);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case get_subject:
                        try {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                subjectArray.clear();
                                strSubjectArray.clear();
                                if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                    JSONArray jDataArray = jObj.getJSONArray("data");
                                    if (jDataArray != null && jDataArray.length() > 0) {
                                        subject.setSubjectName("Select subject");
                                        subject.setSubjectID("0");
                                        strSubjectArray.add(subject.getSubjectName());
                                        subjectArray.add(subject);
                                        for (int i = 0; i < jDataArray.length(); i++) {
                                            JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                            subject = (Subject) jParser.parseJson(jStudyAbord, new Subject());
                                            strSubjectArray.add(subject.getSubjectName());
                                            subjectArray.add(subject);
                                        }
                                        sp_subject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strSubjectArray));
                                        sp_subject.setSelection(selectedSub);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
