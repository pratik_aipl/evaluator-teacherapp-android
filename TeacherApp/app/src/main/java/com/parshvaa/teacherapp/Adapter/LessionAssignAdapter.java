package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Activities.CCT.CctGenerateActivity;
import com.parshvaa.teacherapp.Activities.Paper.PaperGenerateActivity;
import com.parshvaa.teacherapp.MainReportsActivity;
import com.parshvaa.teacherapp.Models.AssignedData;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.RobotoBoldTextview;
import com.parshvaa.teacherapp.Utils.RobotoTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LessionAssignAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "OrderDetailsAdpater";
    Context context;
    List<AssignedData> dataList;


    public LessionAssignAdapter(Context context, List<AssignedData> dataLists) {
        this.context = context;
        this.dataList = dataLists;
        Log.d(TAG, "size>>>: "+dataList.size());

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new DashboardRowDetailsAdpater.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_raw_data, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        DashboardRowDetailsAdpater.ViewHolder holder = (DashboardRowDetailsAdpater.ViewHolder) holderIn;
        AssignedData rawModel = dataList.get(position);

            Log.d(TAG, "onBindViewHolderdataaa: "+rawModel.getBoardName());
            holder.lin_submission.setVisibility(View.GONE);
            holder.lin_hours.setVisibility(View.VISIBLE);
            holder.lin_time.setVisibility(View.VISIBLE);
            holder.imgMain.setImageResource(R.drawable.book);

            holder.tvBoard.setText(rawModel.getBoardName());
            holder.tvMedium.setText(rawModel.getMediumName());
            holder.tvStdanderd.setText(rawModel.getStandardName());
            holder.tvSubject.setText(rawModel.getSubjectName());
            holder.tv_time.setText(rawModel.getTime());


            int totalMinutesInt = Integer.parseInt(rawModel.getDuration());
            int hours = totalMinutesInt / 60;
            int hoursToDisplay = hours;
            if (hours > 12) {
                hoursToDisplay = hoursToDisplay - 12;
            }
            int minutesToDisplay = totalMinutesInt - (hours * 60);
            String minToDisplay = null;
            if(minutesToDisplay == 0 ) minToDisplay = "00";
            else if( minutesToDisplay < 10 ) minToDisplay = "00" + minutesToDisplay ;
            else minToDisplay = "" + minutesToDisplay ;
            String displayValue = hoursToDisplay + ":" + minToDisplay;

            holder.tv_hours.setText(displayValue);

        holder.itemView.setOnClickListener(v -> {
                if(!TextUtils.isEmpty(rawModel.getAssignLactureLink())){

                    Intent httpIntent = new Intent(Intent.ACTION_VIEW);
                    httpIntent.setData(Uri.parse(""+rawModel.getAssignLactureLink()));
                    context.startActivity(httpIntent);

                }else{
                    Toast.makeText(context, "Lecture was classroom, not available online", Toast.LENGTH_SHORT).show();
                }


        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_main)
        ImageView imgMain;
        @BindView(R.id.tv_type_tittle)
        TextView tv_type_tittle;
        @BindView(R.id.img_right)
        ImageView img_right;
        @BindView(R.id.tv_board)
        RobotoBoldTextview tvBoard;
        @BindView(R.id.tv_medium)
        RobotoTextView tvMedium;
        @BindView(R.id.tv_stdanderd)
        RobotoBoldTextview tvStdanderd;
        @BindView(R.id.tv_subject)
        RobotoTextView tvSubject;
        @BindView(R.id.tv_submission_dt)
        RobotoBoldTextview tvSubmissionDt;
        @BindView(R.id.tv_time)
        RobotoBoldTextview tv_time;
        @BindView(R.id.tv_hours)
        RobotoBoldTextview tv_hours;
        @BindView(R.id.lin_submission)
        LinearLayout lin_submission;
        @BindView(R.id.lin_time)
        LinearLayout lin_time;
        @BindView(R.id.lin_hours)
        LinearLayout lin_hours;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}