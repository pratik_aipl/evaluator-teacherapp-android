package com.parshvaa.teacherapp.Activities.Paper;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 6/7/2018.
 */

public class PaperAssigned implements Serializable {
    public String ClassTestPlannerID = "";
    public String AssignDate = "";
    public String PaperDate = "";
    public String PaperMark = "";
    public String BoardName = "";
    public String MediumName = "";
    public String StandardID = "";
    public String StandardName = "";
    public String SubjectID = "";
    public String SubjectName = "";
    public String AssignType = "";
    public String first_name = "";
    public String last_name = "";
    public String Status = "";
    public String PaperStatus = "";
    public String ClassQuestionPaperID = "";

    public String getClassTestPlannerID() {
        return ClassTestPlannerID;
    }

    public void setClassTestPlannerID(String classTestPlannerID) {
        ClassTestPlannerID = classTestPlannerID;
    }

    public String getAssignDate() {
        return AssignDate;
    }

    public void setAssignDate(String assignDate) {
        AssignDate = assignDate;
    }

    public String getPaperDate() {
        return PaperDate;
    }

    public void setPaperDate(String paperDate) {
        PaperDate = paperDate;
    }

    public String getPaperMark() {
        return PaperMark;
    }

    public void setPaperMark(String paperMark) {
        PaperMark = paperMark;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getAssignType() {
        return AssignType;
    }

    public void setAssignType(String assignType) {
        AssignType = assignType;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getPaperStatus() {
        return PaperStatus;
    }

    public void setPaperStatus(String paperStatus) {
        PaperStatus = paperStatus;
    }

    public String getClassQuestionPaperID() {
        return ClassQuestionPaperID;
    }

    public void setClassQuestionPaperID(String classQuestionPaperID) {
        ClassQuestionPaperID = classQuestionPaperID;
    }


}
