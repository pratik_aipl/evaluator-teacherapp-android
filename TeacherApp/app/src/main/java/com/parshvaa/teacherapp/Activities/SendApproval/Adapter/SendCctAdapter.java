package com.parshvaa.teacherapp.Activities.SendApproval.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.CCT.CctAssignedAdapter;
import com.parshvaa.teacherapp.Activities.CCT.CctGenerateActivity;
import com.parshvaa.teacherapp.Activities.CCT.CctPaper;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssigned;
import com.parshvaa.teacherapp.R;

import java.util.List;

/**
 * Created by empiere-vaibhav on 6/27/2018.
 */

public class SendCctAdapter extends RecyclerView.Adapter<SendCctAdapter.MyViewHolder> {

    public Context context;
    private List<CctPaper> optList;

    public SendCctAdapter(List<CctPaper> moviesList, Context context) {
        this.optList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_cct_assigned_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_standard.setText(optList.get(position).getStandardName());
        holder.tv_subject.setText(optList.get(position).getSubjectName());
        holder.tv_medium.setText(optList.get(position).getMediumName());
        holder.tv_assign_dt.setText(optList.get(position).getCreatedOn());
    }

    @Override
    public int getItemCount() {
        return optList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_standard, tv_exam_dt, tv_assign_dt, tv_medium, tv_subject;

        public MyViewHolder(View view) {
            super(view);
            tv_standard = (TextView) view.findViewById(R.id.tv_standard);
            tv_subject = (TextView) view.findViewById(R.id.tv_subject);
            tv_medium = (TextView) view.findViewById(R.id.tv_medium);
            tv_assign_dt = (TextView) view.findViewById(R.id.tv_assign_dt);
        }
    }
}

