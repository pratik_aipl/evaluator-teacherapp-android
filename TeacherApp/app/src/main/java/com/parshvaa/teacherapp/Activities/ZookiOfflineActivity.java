package com.parshvaa.teacherapp.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Models.Zooki;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CustPagerTransformer;

import java.util.ArrayList;
import java.util.List;

public class ZookiOfflineActivity extends AppCompatActivity {
    public App app;
    public ImageView img_back;
    public Zooki zookiObj;
    public ArrayList<Zooki> zookiList = new ArrayList<>();
    ZookiOfflineActivity instance;
    private int Postion;
    private TextView indicatorTv;
    private View close, positionView;
    private ViewPager viewPager;
    private List<ZookiFragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zooki_offline);
        instance = this;

        positionView = findViewById(R.id.position_view);

        close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        zookiList = (ArrayList<Zooki>) getIntent().getExtras().getSerializable("zooki");
        try {
            Postion = getIntent().getExtras().getInt("pos");
        } catch (Exception e) {
            e.printStackTrace();
        }
        fillViewPager();


    }


    private void fillViewPager() {
        indicatorTv = (TextView) findViewById(R.id.indicator_tv);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setPageTransformer(false, new CustPagerTransformer(this));

        for (int i = 0; i < zookiList.size(); i++) {

            fragments.add(new ZookiFragment());
        }
        // 1. viewPager添加parallax效果，使用PageTransformer就足够了

        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                ZookiFragment fragment = fragments.get(position % 10);
                fragment.bindData(zookiList.get(position % zookiList.size()));
                return fragment;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return super.isViewFromObject(view, object);
            }

            @Override
            public int getCount() {
                return zookiList.size();
            }
        });

        viewPager.setCurrentItem(Postion);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateIndicatorTv();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateIndicatorTv();
    }

    private void updateIndicatorTv() {
        int totalNum = viewPager.getAdapter().getCount();
        int currentItem = viewPager.getCurrentItem() + 1;
        indicatorTv.setText(Html.fromHtml("<font color='#12edf0'>" + currentItem + "</font>  /  " + totalNum));
    }
}
