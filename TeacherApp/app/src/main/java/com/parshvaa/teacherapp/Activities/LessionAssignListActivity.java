package com.parshvaa.teacherapp.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.teacherapp.Adapter.LessionAssignAdapter;
import com.parshvaa.teacherapp.Adapter.NotiAdapter;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.AssignedData;
import com.parshvaa.teacherapp.Models.NotificationList;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LessionAssignListActivity extends  BaseActivity implements AsynchTaskListner {

    private static final String TAG = "LessionAssignListActivi";
    public LessionAssignListActivity instance;
    public ImageView img_back;
    public TextView tv_title, tv_empty;
    public RecyclerView rv_notification;
    public LessionAssignAdapter adapter;
    public ArrayList<AssignedData> lessionList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public LinearLayout empty_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        instance = this;
        jParser = new JsonParserUniversal();
        rv_notification = findViewById(R.id.rv_notification);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_empty = findViewById(R.id.tv_empty);
        empty_view = findViewById(R.id.empty_view);
        tv_title.setText("LESSON ASSIGNED");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        showProgress(true);
        new CallRequest(instance).GetlessonPlannerList();
    }

    public void setupRecyclerView() {
        final Context context = rv_notification.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rv_notification.setLayoutAnimation(controller);
        rv_notification.scheduleLayoutAnimation();
        rv_notification.setLayoutManager(new LinearLayoutManager(context));
        Collections.reverse(lessionList);
        adapter = new LessionAssignAdapter(context,lessionList);
        rv_notification.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case GetlessonPlannerList:
                        jObj = new JSONObject(result);
                        Log.d(TAG, "lessionassign: "+result);
                        if (jObj.getBoolean("status")) {
                            JSONArray lession = jObj.getJSONArray("data");

                            if (lession.length() > 0) {
                                for (int i = 0; i < lession.length(); i++) {
                                    AssignedData assignedData=new AssignedData();
                                    assignedData.setClassLessonPlannerID(lession.getJSONObject(i).getString("ClassLessonPlannerID"));
                                    assignedData.setClassID(lession.getJSONObject(i).getString("ClassID"));
                                    assignedData.setClassName(lession.getJSONObject(i).getString("ClassName"));
                                    assignedData.setTeacherID(lession.getJSONObject(i).getString("TeacherID"));
                                    assignedData.setSubjectID(lession.getJSONObject(i).getString("SubjectID"));
                                    assignedData.setBranchID(lession.getJSONObject(i).getString("BranchID"));
                                    assignedData.setBatchID(lession.getJSONObject(i).getString("BatchID"));
                                    assignedData.setDuration(lession.getJSONObject(i).getString("Duration"));
                                    assignedData.setStatus(lession.getJSONObject(i).getString("Status"));
                                    assignedData.setPaperDate(lession.getJSONObject(i).getString("PaperDate"));
                                    assignedData.setCreatedBy(lession.getJSONObject(i).getString("CreatedBy"));
                                    assignedData.setCreatedOn(lession.getJSONObject(i).getString("CreatedOn"));
                                    assignedData.setModifiedBy(lession.getJSONObject(i).getString("ModifiedBy"));
                                    assignedData.setModifiedOn(lession.getJSONObject(i).getString("ModifiedOn"));
                                    assignedData.setSubjectName(lession.getJSONObject(i).getString("SubjectName"));
                                    assignedData.setBoardName(lession.getJSONObject(i).getString("BoardName"));
                                    assignedData.setMediumName(lession.getJSONObject(i).getString("MediumName"));
                                    assignedData.setStandardName(lession.getJSONObject(i).getString("StandardName"));
                                    assignedData.setTime(lession.getJSONObject(i).getString("Time"));
                                    assignedData.setLectureStatus(lession.getJSONObject(i).getString("LectureStatus"));
                                    assignedData.setHomeWork(lession.getJSONObject(i).getString("HomeWork"));
                                    assignedData.setAssignLactureLink(lession.getJSONObject(i).getString("AssignLactureLink"));
                                    lessionList.add(assignedData);
                                }
                                setupRecyclerView();
                                rv_notification.setVisibility(View.VISIBLE);
                                empty_view.setVisibility(View.GONE);

                            } else {
                                empty_view.setVisibility(View.VISIBLE);
                                rv_notification.setVisibility(View.GONE);
                                tv_empty.setText("No Data");
                            }

                            Log.d(TAG, "size>>>: "+lessionList.size());
                        }
                        break;


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
