package com.parshvaa.teacherapp.calendar.component;

/**
 * Created by ldf on 17/6/26.
 */

public class CalendarAttr {
    /**
     * How do you arrange the week?:<br/>
     * {@link WeekArrayType} <br/>
     */
    private WeekArrayType weekArrayType;

    /**
     * Calendar week layout or month layout:<br/>
     * {@link CalendarType} Layout type<br/>
     */
    private CalendarType calendarType;

    /**
     * Date grid height
     */
    private int cellHeight;

    /**
     * Date grid width
     */
    private int cellWidth;

    public WeekArrayType getWeekArrayType() {
        return weekArrayType;
    }

    public void setWeekArrayType(WeekArrayType weekArrayType) {
        this.weekArrayType = weekArrayType;
    }

    public CalendarType getCalendarType() {
        return calendarType;
    }

    public void setCalendarType(CalendarType calendarType) {
        this.calendarType = calendarType;
    }

    public int getCellHeight() {
        return cellHeight;
    }

    public void setCellHeight(int cellHeight) {
        this.cellHeight = cellHeight;
    }

    public int getCellWidth() {
        return cellWidth;
    }

    public void setCellWidth(int cellWidth) {
        this.cellWidth = cellWidth;
    }

    public enum WeekArrayType {
        /*Sunday as the first day of the week*/Sunday,
        /*Monday as the first day of the week*/Monday
    }

    public enum CalendarType {
        WEEK, MONTH
    }
}
