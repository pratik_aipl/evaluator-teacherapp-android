package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.teacherapp.Models.StudentMarks;
import com.parshvaa.teacherapp.R;

import java.util.List;


public class StudentMarksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "StudentMarksAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<StudentMarks> studentMarksList;
    String totalMark;
    // StudentOnClick studentOnClick;

    // Provide a suitable constructor (depends on the kind of dataset)
    public StudentMarksAdapter(Context context, List<StudentMarks> studentMarksList, String totalMark) {
        this.context = context;
        this.studentMarksList = studentMarksList;
        this.totalMark = totalMark;
        //this.studentOnClick = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_student_mark_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;

        StudentMarks studentMarks = studentMarksList.get(position);

        holder.mStudentName.setText(studentMarks.getFirstName() + " " + studentMarks.getLastName());
        String form = context.getString(R.string.mark_string, studentMarks.getStudentMark(), totalMark);
        holder.mStudentMarks.setText(form);

        //  holder.mStudentName.setOnClickListener(v -> studentOnClick.callbackStudent(position, attendStudent));

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: "+studentMarksList.size());
        return studentMarksList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mStudentName, mStudentMarks;

        public ViewHolder(View v) {
            super(v);

            mStudentName = v.findViewById(R.id.mStudentName);
            mStudentMarks = v.findViewById(R.id.mStudentMarks);


        }
    }
}
