package com.parshvaa.teacherapp.Activities.Profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class EditProfileActivity extends BaseActivity implements AsynchTaskListner {

    public EditProfileActivity instance;
    public Button btn_done;
    public TextView tv_username, tv_email;
    public EditText et_first_name, et_last_name, et_contact;
    public ImageView img_back,img_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        instance = EditProfileActivity.this;
        tv_username = findViewById(R.id.tv_username);
        tv_email = findViewById(R.id.tv_email);
        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_contact = findViewById(R.id.et_contact);
        btn_done = findViewById(R.id.btn_done);
        img_profile = findViewById(R.id.img_profile);

        tv_username.setText(App.userDetail.getUsername());
        tv_email.setText(App.userDetail.getEmail());
        et_first_name.setText(App.userDetail.getFirst_name());
        et_last_name.setText(App.userDetail.getLast_name());
        et_contact.setText(App.userDetail.getRegister_mobile());
        if(!TextUtils.isEmpty(App.userDetail.getClassRoundLogo())) {
            Picasso.with(instance)
                    .load(App.userDetail.getClassRoundLogo())
                    .error(R.drawable.profile) //this is optional the image to display while the url image is downloading
                    .into(img_profile);
        }
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_first_name.getText().toString().equals("")) {
                    et_first_name.setError("Please enter first name");
                } else if (et_last_name.getText().toString().equals("")) {
                    et_last_name.setError("Please enter last name");
                } else if (et_contact.getText().toString().equals("")) {
                    et_contact.setError("Please enter mobile number");
                } else {
                    new CallRequest(instance).editProfile(et_first_name.getText().toString(), et_last_name.getText().toString(), et_contact.getText().toString());
                }
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                switch (request) {
                    case editProfile:
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            onBackPressed();
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                        break;
                }
            } catch (JSONException e) {
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }

    }
}
