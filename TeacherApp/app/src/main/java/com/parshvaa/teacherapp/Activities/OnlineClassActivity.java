package com.parshvaa.teacherapp.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.OnlineClassDemoActivity;
import com.parshvaa.teacherapp.R;

public class OnlineClassActivity extends AppCompatActivity {
    public ImageView img_back;
    public TextView tv_title;
    Button btnload;
    EditText edt_link;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_class_demo);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        btnload = findViewById(R.id.btnload);
        edt_link = findViewById(R.id.edt_link);

        tv_title.setText("ONLINE CLASS");
        img_back.setOnClickListener(v -> onBackPressed());

        btnload.setOnClickListener(view -> {
                if(TextUtils.isEmpty(edt_link.getText().toString().trim())){
                    Toast.makeText(this, "Please Enter Link", Toast.LENGTH_SHORT).show();
                }else{
                    startActivity(new Intent(OnlineClassActivity.this, OnlineClassDemoActivity.class)
                            .putExtra("CLASSLINK",edt_link.getText().toString().trim()));
                }
        });
    }
}
