package com.parshvaa.teacherapp.calendar.interf;


import com.parshvaa.teacherapp.calendar.model.CalendarDate;

public interface OnSelectDateListener {
    void onSelectDate(CalendarDate date);

    void onSelectOtherMonth(int offset);//点击其它月份日期
}
