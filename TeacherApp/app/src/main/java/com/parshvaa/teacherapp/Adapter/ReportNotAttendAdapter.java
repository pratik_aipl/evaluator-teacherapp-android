package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Models.AttendStudent;
import com.parshvaa.teacherapp.R;

import java.util.List;

public class ReportNotAttendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<AttendStudent> attendStudentList;

    public ReportNotAttendAdapter(Context context, List<AttendStudent> attendStudentList) {
        this.context = context;
        this.attendStudentList = attendStudentList;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_not_attend_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        AttendStudent attendStudent = attendStudentList.get(position);

        holder.mStudentName.setText(attendStudent.getFirstName() + " " + attendStudent.getLastName());

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return attendStudentList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mStudentName;

        public ViewHolder(View v) {
            super(v);
            mStudentName = v.findViewById(R.id.mnotStudentName);


        }
    }
}
