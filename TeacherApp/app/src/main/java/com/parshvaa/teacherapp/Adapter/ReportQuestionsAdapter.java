package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.teacherapp.Models.LevelQuestion;
import com.parshvaa.teacherapp.OptionsWebView;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Constant;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportQuestionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ReportQuestionsAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<LevelQuestion> answerBeans;
    String page;

    public ReportQuestionsAdapter(Context context, List<LevelQuestion> answerBeans, String page) {
        this.context = context;
        this.answerBeans = answerBeans;
        this.page = page;
        Log.d(TAG, "questionsize: "+answerBeans.size());

    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_questions_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        Log.d(TAG, "onBindViewHolder: " + page);
        if (page.equalsIgnoreCase(Constant.report1)) {
            holder.mQuestionsItem.setBackground(null);
            if (position == answerBeans.size() - 1) {
                holder.mLine.setVisibility(View.GONE);
            } else {
                holder.mLine.setVisibility(View.VISIBLE);
            }
        } else {
            if (position == 0) {
                holder.mQuestionsItem.setBackground(ContextCompat.getDrawable(context, R.drawable.top_cornor_round));
            } else if (position == answerBeans.size() - 1) {
                holder.mLine.setVisibility(View.GONE);
                holder.mQuestionsItem.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_cornor_round));
            } else {
                holder.mLine.setVisibility(View.VISIBLE);
                holder.mQuestionsItem.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
            }
//            holder.mQuestionsItem.setBackground(null);
        }
        holder.mQueNo.setText((position + 1) + ")");
        holder.mQuestionText.loadHtmlFromLocal(answerBeans.get(position).getQuestion(), "#000000");
        holder.mQuestionText.setInitialScale(30);
        holder.mQuestionText.setBackgroundColor(Color.TRANSPARENT);
        holder.mQuestionText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        holder.mQuestionText.setScrollbarFadingEnabled(false);
        holder.mQuestionText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        // TODO: 1/4/2019  Question average textview.
        holder.mCorrect.setText(answerBeans.get(position).getCorrect() + "%");
        holder.mInCorrect.setText(answerBeans.get(position).getInCorrect() + "%");
        holder.mNotAttempt.setText(answerBeans.get(position).getNotAttempt() + "%");

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        if (page.equalsIgnoreCase(Constant.report1)) {
            return answerBeans.size() > 2 ? 2 : answerBeans.size();
        } else {
            return answerBeans.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mQueNo)
        public TextView mQueNo;
        @BindView(R.id.mQuestionsItem)
        public LinearLayout mQuestionsItem;
        @BindView(R.id.mCorrect)
        public TextView mCorrect;
        @BindView(R.id.mInCorrect)
        public TextView mInCorrect;
        @BindView(R.id.mNotAttempt)
        public TextView mNotAttempt;
        @BindView(R.id.mLine)
        public View mLine;
        @BindView(R.id.mQuestionText)
        public OptionsWebView mQuestionText;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
