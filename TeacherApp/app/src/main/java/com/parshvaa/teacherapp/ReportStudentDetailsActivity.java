package com.parshvaa.teacherapp;

import android.net.NetworkRequest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.parshvaa.teacherapp.Adapter.ReportAttendAdapter;
import com.parshvaa.teacherapp.Adapter.StudentQuestionsAdapter;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.AttendStudent;
import com.parshvaa.teacherapp.Models.LevelQuestion;
import com.parshvaa.teacherapp.Models.StudentDetailsBean;
import com.parshvaa.teacherapp.Models.TestDetails;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportStudentDetailsActivity extends BaseActivity {

    private static final String TAG = "ReportStudentDetailsAct";

    @BindView(R.id.img_back)
    ImageView mBackBtn;
    @BindView(R.id.tv_title)
    TextView mPageTitle;
    @BindView(R.id.mStudent)
    TextView mStudent;
    @BindView(R.id.mTimeTaken)
    TextView mTimeTaken;
    @BindView(R.id.mStudentView)
    LinearLayout mStudentView;
    @BindView(R.id.mStudentViewLine)
    View mStudentViewLine;
    @BindView(R.id.mCorrectAns1)
    TextView mCorrectAns1;
    @BindView(R.id.mInCorrectAns)
    TextView mInCorrectAns;
    @BindView(R.id.mNotAns)
    TextView mNotAns;
    @BindView(R.id.mQuestionsList)
    NonScrollRecyclerView mQuestionsList;
    String PlannerID;
    AttendStudent student;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_student_2);
        ButterKnife.bind(this);
        PlannerID = getIntent().getStringExtra(Constant.PlannerID);
        student = (AttendStudent) getIntent().getSerializableExtra(Constant.StudentID);

        mPageTitle.setText("Student");
        mStudent.setText(student.getFirstName());

        mTimeTaken.setText(student.getAvgTime());

        mCorrectAns1.setText(String.format(getString(R.string.summary), student.getCorrect(), student.getTotalQquestion()));
        mInCorrectAns.setText(String.format(getString(R.string.summary), student.getIncorrect(), student.getTotalQquestion()));
        mNotAns.setText(String.format(getString(R.string.summary), (student.getTotalQquestion() - student.getTotalAttempt()), student.getTotalQquestion()));

        Log.d(TAG, "Student Details: "+student.getQuestion().size());
        mQuestionsList.setAdapter(new StudentQuestionsAdapter(this, student.getQuestion()));


    }

    @OnClick({R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
