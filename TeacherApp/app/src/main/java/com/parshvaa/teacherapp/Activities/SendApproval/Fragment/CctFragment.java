package com.parshvaa.teacherapp.Activities.SendApproval.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Activities.CCT.CctPaper;
import com.parshvaa.teacherapp.Activities.SendApproval.Adapter.SendCctAdapter;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.ObservScroll.BaseFragment;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 6/27/2018.
 */

public class CctFragment extends BaseFragment implements AsynchTaskListner {
    View v;
    public RecyclerView rv_gen_paper;
    public SendCctAdapter adapter;
    public CctPaper cctPaper;
    public JsonParserUniversal jParser;
    public ArrayList<CctPaper> cctPaperArrayList = new ArrayList<>();
    public LinearLayout empty_view;
    public TextView tv_empty;
    public String standardid = "", subjectid = "", assigndate = "", examdate = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_paper, container, false);
        jParser = new JsonParserUniversal();
        rv_gen_paper = v.findViewById(R.id.rv_gen_paper);
        tv_empty = (TextView)v. findViewById(R.id.tv_empty);
        empty_view = v.findViewById(R.id.empty_view);
        showProgress(true);
        new CallRequest(CctFragment.this).cctReadyForApproval("", "", "");
        return v;
    }

    public void setupRecyclerView() {
        final Context context = rv_gen_paper.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rv_gen_paper.setLayoutAnimation(controller);
        rv_gen_paper.scheduleLayoutAnimation();
        rv_gen_paper.setLayoutManager(new LinearLayoutManager(context));
        adapter = new SendCctAdapter(cctPaperArrayList, context);
        rv_gen_paper.setAdapter(adapter);

    }
    public void callingCCtForApproval(String standard_id, String subject_id, String assign_date) {
        standardid = standard_id;
        subjectid = subject_id;
        assigndate = assign_date;

        if(cctPaperArrayList.size()==0){
            Toast.makeText(getActivity(), "Data Not Available", Toast.LENGTH_SHORT).show();
        }else{
            callRequest(standardid,subjectid,assigndate);
        }

    }

    private void callRequest(String standardid, String subjectid, String assigndate) {
        new CallRequest(CctFragment.this).cctReadyForApproval(standardid, subjectid, assigndate);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case cctReadyForApproval:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            cctPaperArrayList.clear();

                            JSONArray jCctArray = jObj.getJSONArray("assigned_cct_paper");
                            if (jCctArray.length() > 0) {
                                for (int j = 0; j < jCctArray.length(); j++) {
                                    cctPaper = (CctPaper) jParser.parseJson(jCctArray.getJSONObject(j), new CctPaper());
                                    cctPaperArrayList.add(cctPaper);
                                }
                                setupRecyclerView();
                                rv_gen_paper.setVisibility(View.VISIBLE);
                                empty_view.setVisibility(View.GONE);

                            }else{
                                empty_view.setVisibility(View.VISIBLE);
                                rv_gen_paper.setVisibility(View.GONE);
                                tv_empty.setText("No Data");
                                Toast.makeText(getActivity(), ""+jObj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                         }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
