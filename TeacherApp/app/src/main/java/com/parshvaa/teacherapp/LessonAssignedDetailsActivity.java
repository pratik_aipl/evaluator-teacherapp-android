package com.parshvaa.teacherapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Adapter.SelectedChapterAdapter;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.AssignedData;
import com.parshvaa.teacherapp.Models.SelectedChapters;
import com.parshvaa.teacherapp.Models.Status;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.RobotoBoldTextview;
import com.parshvaa.teacherapp.Utils.RobotoTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LessonAssignedDetailsActivity extends BaseActivity implements AsynchTaskListner {

    private static final String TAG = "LessonAssignedDetailsAc";
    List<AssignedData> dataList = new ArrayList<>();
    AssignedData assignedData;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    RobotoBoldTextview tvTitle;
    @BindView(R.id.img_main)
    ImageView imgMain;
    @BindView(R.id.tv_type_tittle)
    TextView tvTypeTittle;
    @BindView(R.id.tv_board)
    RobotoBoldTextview tvBoard;
    @BindView(R.id.tv_medium)
    RobotoTextView tvMedium;
    @BindView(R.id.tv_stdanderd)
    RobotoBoldTextview tvStdanderd;
    @BindView(R.id.tv_subject)
    RobotoTextView tvSubject;
    @BindView(R.id.tv_submission_dt)
    RobotoBoldTextview tvSubmissionDt;
    @BindView(R.id.lin_submission)
    LinearLayout linSubmission;
    @BindView(R.id.tv_time)
    RobotoBoldTextview tvTime;
    @BindView(R.id.lin_time)
    LinearLayout linTime;
    @BindView(R.id.tv_hours)
    RobotoBoldTextview tvHours;
    @BindView(R.id.lin_hours)
    LinearLayout linHours;
    @BindView(R.id.img_right)
    ImageView imgRight;
    @BindView(R.id.sp_standard)
    Spinner spStatus;
    @BindView(R.id.addr_edittext)
    EditText addrEdittext;
    @BindView(R.id.btn_start_lact)
    Button btnStartLact;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.lin_recycler)
    LinearLayout linRecycler;

    Status status;
    public ArrayList<String> strStatusArray = new ArrayList<>();
    public ArrayList<Status> statusArray = new ArrayList<>();
    public JsonParserUniversal jParser;

    String STATUS;

    public ArrayList<SelectedChapters> selectedChapters = new ArrayList<>();
    SelectedChapters readyCct;
    SelectedChapterAdapter selectedChapterAdapter;
    @BindView(R.id.mQuestionsList)
    NonScrollRecyclerView recylacture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_assigned_details);
        ButterKnife.bind(this);
        jParser = new JsonParserUniversal();

        assignedData = (AssignedData) getIntent().getSerializableExtra("DATALIST");
        dataList.add(assignedData);
        Log.d(TAG, "onCreate: " + dataList.size());
        tvTitle.setText("LECTURE ASSIGNED");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (dataList.size() != 0) {
            AssignedData rawModel = dataList.get(0);
            linSubmission.setVisibility(View.GONE);
            linHours.setVisibility(View.VISIBLE);
            linTime.setVisibility(View.VISIBLE);
            imgMain.setImageResource(R.drawable.book);

            tvBoard.setText(dataList.get(0).getBoardName());
            tvMedium.setText(rawModel.getMediumName());
            tvStdanderd.setText(rawModel.getStandardName());
            tvSubject.setText(rawModel.getSubjectName());
            tvTime.setText(rawModel.getTime());


            int totalMinutesInt = Integer.parseInt(rawModel.getDuration());
            int hours = totalMinutesInt / 60;
            int hoursToDisplay = hours;
            if (hours > 12) {
                hoursToDisplay = hoursToDisplay - 12;
            }
            int minutesToDisplay = totalMinutesInt - (hours * 60);
            String minToDisplay = null;
            if (minutesToDisplay == 0) minToDisplay = "00";
            else if (minutesToDisplay < 10) minToDisplay = "00" + minutesToDisplay;
            else minToDisplay = "" + minutesToDisplay;
            String displayValue = hoursToDisplay + ":" + minToDisplay;

            tvHours.setText(displayValue);

            if (!TextUtils.isEmpty(rawModel.getHomeWork())) {
                addrEdittext.setText(rawModel.getHomeWork());
            }
            Log.d(TAG, "getHomeWork: "+rawModel.getHomeWork());

        }

        btnStartLact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(dataList.get(0).getAssignLactureLink())) {

                    Intent httpIntent = new Intent(Intent.ACTION_VIEW);
                    httpIntent.setData(Uri.parse("" + dataList.get(0).getAssignLactureLink()));
                    startActivity(httpIntent);

                } else {
                    Toast.makeText(LessonAssignedDetailsActivity.this, "Lecture was classroom, not available online", Toast.LENGTH_SHORT).show();
                }
            }
        });


        status = new Status();
        status.setStatusName("Select Status");
        strStatusArray.add(status.getStatusName());
        statusArray.add(status);
        spStatus.setAdapter(new ArrayAdapter<String>(this, R.layout.custom_spinner_row, strStatusArray));
        showProgress(true);
        new CallRequest(this).getStatus();
        new CallRequest(this).getChapters(dataList.get(0).getClassLessonPlannerID());

        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                STATUS = spStatus.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(spStatus.getSelectedItemPosition()==0){
                    Toast.makeText(LessonAssignedDetailsActivity.this, "Please Select Status", Toast.LENGTH_SHORT).show();
                }else{
                    new CallRequest(LessonAssignedDetailsActivity.this).updatePlanerRemark(STATUS,addrEdittext.getText().toString(),dataList.get(0).getClassLessonPlannerID());
                }

            }
        });


    }

    public void setupRecyclerView() {
        final Context context = recylacture.getContext();

        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recylacture.setLayoutAnimation(controller);
        recylacture.scheduleLayoutAnimation();
        recylacture.setLayoutManager(new LinearLayoutManager(context));
        selectedChapterAdapter = new SelectedChapterAdapter(selectedChapters, context);
        recylacture.setAdapter(selectedChapterAdapter);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case getstatus:
                        try {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                statusArray.clear();
                                strStatusArray.clear();
                                if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                    JSONArray jDataArray = jObj.getJSONArray("data");
                                    if (jDataArray != null && jDataArray.length() > 0) {
                                        status.setStatusName("Select Status");
                                        strStatusArray.add(status.getStatusName());
                                        statusArray.add(status);
                                        for (int i = 0; i < jDataArray.length(); i++) {
                                            JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                            status = (Status) jParser.parseJson(jStudyAbord, new Status());
                                            strStatusArray.add(status.getStatusName());
                                            statusArray.add(status);
                                        }
                                        spStatus.setAdapter(new ArrayAdapter<String>(this, R.layout.custom_spinner_row, strStatusArray));
                                        //spStatus.setSelection(selectedStd);
                                          if(!TextUtils.isEmpty(dataList.get(0).getLectureStatus())){
                                                 for (int i = 0; i <statusArray.size() ; i++) {
                                                     if(statusArray.get(i).getStatusName().equalsIgnoreCase(dataList.get(0).getLectureStatus())){
                                                         spStatus.setSelection(i);
                                                         STATUS=dataList.get(0).getLectureStatus();
                                                     }

                                                 }
                                            }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getChapters:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            selectedChapters.clear();
                            JSONArray jData = jObj.getJSONArray("data");
                            if (jData.length() > 0) {
                                for (int i = 0; i < jData.length(); i++) {
                                    readyCct = (SelectedChapters) jParser.parseJson(jData.getJSONObject(i), new SelectedChapters());
                                    selectedChapters.add(readyCct);
                                }
                                linRecycler.setVisibility(View.VISIBLE);
                                setupRecyclerView();
                            } else {
                                linRecycler.setVisibility(View.GONE);
                            }
                        }
                        break;

                        case getplanerremark:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Toast.makeText(this, ""+jObj.getString("message"), Toast.LENGTH_SHORT).show();
                            onBackPressed();

                        }else{
                            Toast.makeText(this, ""+jObj.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
