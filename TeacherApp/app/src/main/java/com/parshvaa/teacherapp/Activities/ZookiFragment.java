package com.parshvaa.teacherapp.Activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.Models.Zooki;
import com.parshvaa.teacherapp.ObservScroll.ObservableScrollView;
import com.parshvaa.teacherapp.ObservScroll.ObservableScrollViewCallbacks;
import com.parshvaa.teacherapp.ObservScroll.ScrollState;
import com.parshvaa.teacherapp.ObservScroll.ViewHelper;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.Utils;

import java.io.File;



public class ZookiFragment extends Fragment implements ObservableScrollViewCallbacks {
    public Zooki zookiObj;
    public int mParallaxImageHeight;
    public Button btn_viewmore;
    public FrameLayout frame_content, frame_viewMore;
    private TextView tv_FullDesc, tv_title, tv_date, tv_header_title;
    private ImageView mImageView;
    private ObservableScrollView mScrollView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_common, null);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_FullDesc = (TextView) rootView.findViewById(R.id.tv_FullDesc);
        tv_date = (TextView) rootView.findViewById(R.id.tv_date);
        btn_viewmore = (Button) rootView.findViewById(R.id.btn_viewmore);
        frame_content = (FrameLayout) rootView.findViewById(R.id.frame_content);
        frame_viewMore = (FrameLayout) rootView.findViewById(R.id.frame_viewMore);
        mImageView = rootView.findViewById(R.id.image);


        if (zookiObj.getTitle().equals("View More")) {
            frame_content.setVisibility(View.GONE);
            frame_viewMore.setVisibility(View.VISIBLE);
            btn_viewmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isNetworkAvailable(getActivity())) {
                        startActivity(new Intent(getActivity(), ZookiWebActivity.class));
                    } else {
                        Utils.showToast(getResources().getString(R.string.conect_internet), getActivity());
                    }
                }
            });
        } else {
            tv_title.setText(zookiObj.getTitle());
            String date = zookiObj.getCreatedOn();
            try {
                date = date.substring(0, 10);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tv_date.setText(Utils.changeDateToDDMMYYYY(date));
            tv_FullDesc.setText(zookiObj.getDesc());
            File image = new File(Constant.LOCAL_IMAGE_PATH + "/Zooki/" + zookiObj.getImage());
            // Log.i("Image Path :::: ", image.getPath());
            if (image.exists()) {
                try {
                    mImageView.setImageBitmap(BitmapFactory.decodeFile(image.getPath()));
                } catch (NullPointerException e) {
                }
            }


            mScrollView = (ObservableScrollView) rootView.findViewById(R.id.scroll);
            mScrollView.setScrollViewCallbacks(this);

            mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen._180sdp);
            onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
        }


        return rootView;
    }

    public void bindData(Zooki obj) {
        this.zookiObj = obj;
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ViewHelper.setTranslationY(mImageView, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }
}
