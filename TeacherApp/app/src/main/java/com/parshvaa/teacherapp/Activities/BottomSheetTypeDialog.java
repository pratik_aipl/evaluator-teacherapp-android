package com.parshvaa.teacherapp.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.parshvaa.teacherapp.Interface.ChangeReportClick;
import com.parshvaa.teacherapp.R;


public class BottomSheetTypeDialog extends BottomSheetDialogFragment {

    private static final String TAG = "BottomSheet3DialogFragm";
    Button mAttend;
    Button mNotAttend;
    Button mReset;
    LinearLayout bottomSheetLayout;
    ChangeReportClick changeReportClick;

    Context context;

    public static BottomSheetTypeDialog newInstance() {
        BottomSheetTypeDialog f = new BottomSheetTypeDialog();
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fillter_attempt_dailog, container, false);
        mAttend = view.findViewById(R.id.mAttend);
        mNotAttend = view.findViewById(R.id.mNotAttend);
        mReset = view.findViewById(R.id.mClose);
        bottomSheetLayout = view.findViewById(R.id.bottomSheetLayout);
        changeReportClick = (ChangeReportClick) getContext();


        mAttend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //EventBus.getDefault().post(new ChangeTypeEvent(true));
                changeReportClick.ChangeTypeEvent(true);
                dismiss();
            }
        });
        mNotAttend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // EventBus.getDefault().post(new ChangeTypeEvent(false));
                changeReportClick.ChangeTypeEvent(false);
                dismiss();
            }
        });
        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();

            }
        });

        return view;
    }


}
