package com.parshvaa.teacherapp;

import android.content.Context;
import android.content.Intent;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.parshvaa.teacherapp.Activities.CCT.CctAssignedActivity;
import com.parshvaa.teacherapp.Activities.LessionAssignListActivity;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssignedActivity;
import com.parshvaa.teacherapp.Activities.ReadyReviewCct.ReadyReviewCctActivity;
import com.parshvaa.teacherapp.Activities.ReadyReviewPaper.ReadyReviewPaperActivity;
import com.parshvaa.teacherapp.Activities.SendApproval.SendApprovalActivity;
import com.parshvaa.teacherapp.ObservScroll.BaseFragment;
import com.parshvaa.teacherapp.Utils.Constant;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SearchFragment extends BaseFragment {


    View view;
    public RelativeLayout rel_gen_paper, rel_gen_cct, rel_send_for_approvel, rel_lession_assign, rel_review_paper, rel_review_cct;


    public SearchFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.searchfregment, container, false);
        rel_gen_paper = view.findViewById(R.id.rel_gen_paper);
        rel_gen_cct = view.findViewById(R.id.rel_gen_cct);
        rel_review_paper = view.findViewById(R.id.rel_review_paper);
        rel_review_cct = view.findViewById(R.id.rel_review_cct);
        rel_send_for_approvel = view.findViewById(R.id.rel_send_for_approvel);
        rel_lession_assign = view.findViewById(R.id.rel_lession_assign);


        rel_gen_paper.setOnClickListener(view ->
                startActivity(new Intent(getActivity(), PaperAssignedActivity.class)));
        rel_lession_assign.setOnClickListener(view ->
                startActivity(new Intent(getActivity(), LessionAssignListActivity.class)));
        rel_gen_cct.setOnClickListener(view ->
                startActivity(new Intent(getActivity(), CctAssignedActivity.class)));
        rel_review_paper.setOnClickListener(view ->
                startActivity(new Intent(getActivity(), ReadyReviewPaperActivity.class)));
        rel_review_cct.setOnClickListener(view ->
                startActivity(new Intent(getActivity(), ReadyReviewCctActivity.class)));
        rel_send_for_approvel.setOnClickListener(view -> startActivity(new Intent(getActivity(), SendApprovalActivity.class)));
        return view;
    }



    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
