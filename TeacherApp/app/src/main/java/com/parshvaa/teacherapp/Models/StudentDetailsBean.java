package com.parshvaa.teacherapp.Models;

import java.io.Serializable;
import java.util.List;

public class StudentDetailsBean implements Serializable {

    TestDetails testDetails;
    List<LevelQuestion> appearedQuestions;
    List<LevelQuestion> notAppearedQuestions;

    public TestDetails getTestDetails() {
        return testDetails;
    }

    public void setTestDetails(TestDetails testDetails) {
        this.testDetails = testDetails;
    }

    public List<LevelQuestion> getAppearedQuestions() {
        return appearedQuestions;
    }

    public void setAppearedQuestions(List<LevelQuestion> appearedQuestions) {
        this.appearedQuestions = appearedQuestions;
    }

    public List<LevelQuestion> getNotAppearedQuestions() {
        return notAppearedQuestions;
    }

    public void setNotAppearedQuestions(List<LevelQuestion> notAppearedQuestions) {
        this.notAppearedQuestions = notAppearedQuestions;
    }
}
