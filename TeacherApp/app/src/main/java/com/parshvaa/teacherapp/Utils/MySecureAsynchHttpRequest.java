package com.parshvaa.teacherapp.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;

import com.parshvaa.teacherapp.Activities.LoginActivity;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sagar Sojitra on 3/8/2016.
 */
public class MySecureAsynchHttpRequest {

    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsynchTaskListner aListner;
    public Constant.POST_TYPE post_type;
    public boolean header = false;
    public String tempData = "";

    String api_key = "admin";
    String api_password = "API@TEACHER!#APP$";
    String encodeString = (api_key + ":" + api_password);
    public String token, user_id;

    public MySharedPref mySharedPref;

    public MySecureAsynchHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;
        mySharedPref = new MySharedPref(ft.getActivity());

        if (Utils.isNetworkAvailable(ft.getActivity())) {
//            if (!map.containsKey("show")) {
//                Utils.showProgressDialog(ft.getActivity());
//            } else {
//                this.map.remove("show");
//            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ft.getActivity());
        }
    }

    public Context ct;

    public MySecureAsynchHttpRequest(Context ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;
        mySharedPref = new MySharedPref(ft);

        if (Utils.isNetworkAvailable(ct)) {
//            if (!map.containsKey("show")) {
//                Utils.showProgressDialog(ct);
//            }
            if (map.containsKey("header")) {
                header = true;
                token = map.get("token");
                user_id = map.get("user_id");
            }

            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ct);
        }
    }


    class MyRequest extends AsyncTask<Map<String, String>, Void, String>

    {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));

            final String basicAuth = "Basic " + Base64.encodeToString("admin:API@TEACHER!#APP$".getBytes(), Base64.NO_WRAP);

            String responseBody = "";
            try {
                switch (post_type) {
                    case GET:
                        DefaultHttpClient httpClient = getNewHttpClient();
                        String query = map[0].get("url");
                        System.out.println();
                        tempData += "\n\n\n\n URL : " + map[0].get("url");
                        map[0].remove("url");

                        List<String> values = new ArrayList<String>(map[0].values());
                        List<String> keys = new ArrayList<String>(map[0].keySet());

                        for (int i = 0; i < values.size(); i++) {
                            System.out.println();

                            System.out.println(keys.get(i) + "====>" + values.get(i));
                            query = query + keys.get(i) + "=" + values.get(i);
                            tempData += "\n" + keys.get(i) + "====>" + values.get(i);
                            if (i < values.size() - 1) {
                                query += "&";
                            }
                        }

                        System.out.println("USER-ID::" + mySharedPref.getUserId());
                        System.out.println("LOGIN-TOKEN::" + mySharedPref.getLoginToken());

                        System.out.println("URL" + "====>" + query);
                        HttpGet httpGet = new HttpGet(query);
                        httpGet.setHeader("Authorization", basicAuth);
                        httpGet.addHeader("X-TEACHER-APP-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                        httpGet.addHeader("X-TEACHER-APP-LOGIN-TOKEN", mySharedPref.getLoginToken());
                        httpGet.addHeader("USER-ID", mySharedPref.getUserId());

                        HttpResponse httpResponse = httpClient.execute(httpGet);

                        int respCode = httpResponse.getStatusLine().getStatusCode();
                        HttpEntity httpEntity = httpResponse.getEntity();

                        if (respCode == 401) {
                            responseBody = EntityUtils.toString(httpEntity);

                            System.out.println("Responce" + "====>" + responseBody);

                            mySharedPref.clearApp();
                            Intent i = new Intent(ct, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ct.startActivity(i);
                            ((Activity)ct).finish();
                            return "401";
                        } else {

                            responseBody = EntityUtils.toString(httpEntity);

                            System.out.println("Responce" + "====>" + responseBody);
                            tempData += "\n" + "Responce" + "====>" + responseBody;
                            //writeToSDFile(tempData);
                            return responseBody;
                        }

                    case POST:
                        tempData = "";
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        tempData += "\n\n\n\n URL : " + map[0].get("url");
                        HttpPost postRequest = new HttpPost(map[0].get("url"));
                        postRequest.setHeader("Authorization", basicAuth);
                        postRequest.addHeader("X-TEACHER-APP-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                        httpClient = getNewHttpClient();
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            tempData += "\n" + key + "====>" + map[0].get(key);
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                        System.out.println("Final URI::" + postRequest.getEntity().toString());
                        HttpResponse response = httpClient.execute(postRequest);

                        //  writeToSDFile(tempData);

                        respCode = response.getStatusLine().getStatusCode();


                        if (respCode == 401) {
                            mySharedPref.clearApp();
                            Intent i = new Intent(ct, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ct.startActivity(i);
                            ((Activity)ct).finish();

                            return "401";
                        } else {

                            httpEntity = response.getEntity();
                            responseBody = EntityUtils.toString(httpEntity);
                            tempData += "\n" + "Responce" + "====>" + responseBody;
                            System.out.println("Responce" + "====>" + responseBody);

                            return responseBody;

                        }
                        //
                        // return responseBody;

                    case POST_WITH_HEADER:
                        tempData = "";
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        tempData += "\n\n\n\n URL : " + map[0].get("url");
                        postRequest = new HttpPost(map[0].get("url"));
                        postRequest.setHeader("Authorization", basicAuth);
                        postRequest.addHeader("X-TEACHER-APP-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                        postRequest.addHeader("X-TEACHER-APP-LOGIN-TOKEN", mySharedPref.getLoginToken());
                        postRequest.addHeader("USER-ID", mySharedPref.getUserId());
                        httpClient = getNewHttpClient();
                        nameValuePairs = new ArrayList<NameValuePair>();

                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            tempData += "\n" + key + "====>" + map[0].get(key);
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        System.out.println("USER-ID::" + mySharedPref.getUserId());
                        System.out.println("LOGIN-TOKEN::" + mySharedPref.getLoginToken());

                        System.out.println("Final URI::" + postRequest.getEntity().toString());
                        response = httpClient.execute(postRequest);

                        //  writeToSDFile(tempData);

                        respCode = response.getStatusLine().getStatusCode();

                        System.out.println("respCode::" + respCode);

                        if (respCode == 401) {
                            mySharedPref.clearApp();
                            Intent i = new Intent(ct, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ct.startActivity(i);
                            ((Activity)ct).finish();

                            return "401";
                             } else {

                            httpEntity = response.getEntity();
                            responseBody = EntityUtils.toString(httpEntity);
                            tempData += "\n" + "Responce" + "====>" + responseBody;
                            System.out.println("Responce" + "====>" + responseBody);

                            return responseBody;

                        }
                    case POST_WITH_IMAGE:

                        String charset = "UTF-8";
                        try {
                            httpClient = getNewHttpClient();
                            postRequest = new HttpPost(map[0].get("url"));
                            postRequest.setHeader("Authorization", basicAuth);
                            postRequest.addHeader("X-TEACHER-APP-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                            if (header) {
                                postRequest.addHeader("X-TEACHER-APP-LOGIN-TOKEN", token);
                                postRequest.addHeader("USER-ID", user_id);
                            }
                            reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                                @Override
                                public void transferred(long num) {
                                }
                            });
                            BasicHttpContext localContext = new BasicHttpContext();
                            for (String key : map[0].keySet()) {
                                System.out.println();
                                System.out.println(key + "====>" + map[0].get(key));
                                if (matchKeysForImages(key)) {
                                    if (map[0].get(key) != null) {
                                        if (map[0].get(key).length() > 1) {
                                            String type = "image/png";
                                            File f = new File(map[0].get(key));
                                            FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                            reqEntity.addPart(key, fbody);
                                        }
                                    }
                                } else {
                                    if (map[0].get(key) == null) {
                                        reqEntity.addPart(key, new StringBody(""));
                                    } else {
                                        reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                    }
                                }
                            }
                            postRequest.setEntity(reqEntity);
                            HttpResponse responses = httpClient.execute(postRequest, localContext);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),
                                    "UTF-8"));
                            String sResponse;
                            while ((sResponse = reader.readLine()) != null) {
                                responseBody = responseBody + sResponse;
                            }
                            System.out.println("Response ::" + responseBody);
                            return responseBody;

                        } catch (IOException e) {
                            Log.d("ExPostActivity", e.getMessage());
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
                aListner.onTaskCompleted(null, request);
            }
            return null;

        }


        @Override
        protected void onPostExecute(String result) {


            aListner.onTaskCompleted(result, request);
            super.onPostExecute(result);
        }
    }

    public boolean matchKeysForImages(String key) {

        if (Pattern.compile(Pattern.quote("image"), Pattern.CASE_INSENSITIVE).matcher(key).find()) {
            return true;
        }

        if (key.contains("image") ||
                key.equalsIgnoreCase("profile_picture") ||
                key.equalsIgnoreCase("photo_id") ||
                key.equalsIgnoreCase("Image") ||
                key.equalsIgnoreCase("profile_pic") ||
                key.equalsIgnoreCase("ProfilePic")) {
            return true;
        } else {
            return false;
        }
    }

    public DefaultHttpClient HostVerifier() {
        HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

        DefaultHttpClient client = new DefaultHttpClient();

        SchemeRegistry registry = new SchemeRegistry();
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
        registry.register(new Scheme("https", socketFactory, 443));
        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

// Set verifier
        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
        return httpClient;
    }

    public DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
}
