package com.parshvaa.teacherapp.Activities.Profile;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.CCT.CctAssignedAdapter;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.R;

/**
 * Created by empiere-vaibhav on 7/26/2018.
 */

public class ProfileFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private int sectionNumber;
    public RecyclerView rv_subject;
    public TextView tv_board, tv_medium, tv_standard;
    public SubjectAdapter adapter;

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance(int sectionNumber) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
        rv_subject = (RecyclerView) view.findViewById(R.id.rv_subject);
        tv_board = (TextView) view.findViewById(R.id.tv_board);
        tv_medium = (TextView) view.findViewById(R.id.tv_medium);
        tv_standard = (TextView) view.findViewById(R.id.tv_standard);
        tv_board.setText(App.userDetail.standardArray.get(sectionNumber).getBoardName());
        tv_medium.setText(App.userDetail.standardArray.get(sectionNumber).getMediumName());
        tv_standard.setText(App.userDetail.standardArray.get(sectionNumber).getStandardName());
        setupRecyclerView();

        return view;
    }

    public void setupRecyclerView() {
        final Context context = rv_subject.getContext();

        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rv_subject.setLayoutAnimation(controller);
        rv_subject.scheduleLayoutAnimation();
        rv_subject.setLayoutManager(new LinearLayoutManager(context));
        adapter = new SubjectAdapter(App.userDetail.standardArray.get(sectionNumber).subjectsArray, context);
        rv_subject.setAdapter(adapter);

    }
}
