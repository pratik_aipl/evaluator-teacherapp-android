package com.parshvaa.teacherapp.Utils;

import android.os.Environment;

/**
 * Created by admin on 10/12/2016.
 */
public class Constant {
    public static int EXTERNAL_STORAGE_PERMISSION = 9999;
    public static int READ_PHONE_STATE = 568;
    public static String PREFRENCE = "shared_pref";

    public static String report1="report1";
    public static String questionsList="questionsList";
    public static final String PlannerID = "PlannerID";
    public static final String StudentID = "StudentID";
    public static final String CONNECT_TO_WIFI = "WIFI";
    public static final String CONNECT_TO_MOBILE = "MOBILE";
    public static final String NOT_CONNECT = "NOT_CONNECT";
    public final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    public static String LOCAL_IMAGE_PATH = Environment.getExternalStorageDirectory() + "/.iScore";
    public static String LOCAL_Library_PATH = Environment.getExternalStorageDirectory() + "/Download";
    public static final int READ_WRITE_EXTERNAL_STORAGE_CAMERA = 2355;


    //------------------- PAYTM TEST Cradantial--------------------------------------
    /*
    public static final String M_ID = "PARSHV66654838019682"; //Paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WAP"; //Paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail"; //Paytm industry type got it in paytm credential
    public static final String WEBSITE = "APPSTAGING";
    public static final String CALLBACK_URL = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
    public static String TXN_STATUS = "https://securegw-stage.paytm.in/merchant-status/getTxnStatus?";
*/

    //------------------- PAYTM LIVE Cradantial--------------------------------------

    public static final String M_ID = "Parshv15070673965035"; //Paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WAP"; //Paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail109"; //Paytm industry type got it in paytm credential
    public static final String WEBSITE = "APPPROD";
    public static final String CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback.jsp";
    public static String TXN_STATUS_URL = "https://securegw.paytm.in/merchant-status/getTxnStatus?";

    //------------------- LIVE SERVER--------------------------------------

    public static String URL = "https://evaluater.parshvaa.com/web_services/version_2/web_services/";
    public static String BASIC_VIEW_PAPER_EVA_URL = "https://evaluater.parshvaa.com/";
    public static String URL_VERSION = "version_56/";

    //------------------- PEMP SERVER--------------------------------------
  /*  public static String BASIC_URL = "https://test.pemiscore.parshvaa.com/web_services/";
    public static String BASIC_VIEW_PAPER_EVA_URL = "https://test.pemevaluater.parshvaa.com/";
    public static String URL_VERSION = "version_39/";
 */   //------------------- TEST SERVER--------------------------------------
    /*
     public static String BASIC_URL = "https://test.escore.parshvaa.com/web_services/";
    public static String BASIC_VIEW_PAPER_EVA_URL = "https://test.evaluator.parshvaa.com/";
    public static String URL_VERSION = "version_39/";
    */

    public static String EVEL_LIB_SYSTEM_IMAGE_PATH = BASIC_VIEW_PAPER_EVA_URL + "library_system/";
    public static String fromAssign="fromAssign";
    public static String title="title";

    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE, POST_WITH_JSON, Eval_POST_WITH_JSON, POST_WITH_HEADER;
    }


    public enum REQUESTS {
        resend_otp, confirmOtp, getDashboardCount, assignedPaper, assignedCctPaper,getDate,getDataByDate,getReport,getstudentwisecctreport,getstudentSummary,getplanerremark, generateCctPaperUrl,generatedCctUrls,getChapters, readyReviewPaper, generateReadyReviewUrl, readyReviewCct, generateCctReadyReview, get_standard, get_subject, generatePaperUrl, readyForApproval, cctReadyForApproval,GetREVIEWAPPROVEDCCT,GetREVIEWAPPROVEDPAPER,genratedCctPaper, forgotPassword, getNotifition, getstatus, qurySendMail, getUserDetails, editProfile, getLogin, getcctPaperUrl, getReports, getReportsTest, getReportsDetails, teacherCctReport,GetlessonPlannerList, teacherTestReport
    }

    public static int SUBJECT_TABLE = 2;
    public static int CHAPTER_TABLE = 3;

    /**
     * This is for PUSH Notification
     **/

    public static final String SENDER_ID = "";
    public static final String DISPLAY_ACTION = "com.markteq.wms";

    public static final int TWITTER_LOGIN_REQUEST_CODE = 1;
}
