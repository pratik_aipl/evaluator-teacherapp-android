package com.parshvaa.teacherapp.Models;

import java.io.Serializable;

public class TestDetails implements Serializable {
    int StudentMCQTestID;
    int StudentID;
    int PlannerID;
    int TotalQuestion;
    int TotalRight;
    int TotalWrong;
    int TotalAttempt;
    int Accuracy;
    int TakenTime;
    int AvgTime;
    String SubmitDate;

    public int getStudentMCQTestID() {
        return StudentMCQTestID;
    }

    public void setStudentMCQTestID(int studentMCQTestID) {
        StudentMCQTestID = studentMCQTestID;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public int getPlannerID() {
        return PlannerID;
    }

    public void setPlannerID(int plannerID) {
        PlannerID = plannerID;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public int getTotalRight() {
        return TotalRight;
    }

    public void setTotalRight(int totalRight) {
        TotalRight = totalRight;
    }

    public int getTotalWrong() {
        return TotalWrong;
    }

    public void setTotalWrong(int totalWrong) {
        TotalWrong = totalWrong;
    }

    public int getTotalAttempt() {
        return TotalAttempt;
    }

    public void setTotalAttempt(int totalAttempt) {
        TotalAttempt = totalAttempt;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }

    public int getTakenTime() {
        return TakenTime;
    }

    public void setTakenTime(int takenTime) {
        TakenTime = takenTime;
    }

    public int getAvgTime() {
        return AvgTime;
    }

    public void setAvgTime(int avgTime) {
        AvgTime = avgTime;
    }

    public String getSubmitDate() {
        return SubmitDate;
    }

    public void setSubmitDate(String submitDate) {
        SubmitDate = submitDate;
    }
}
