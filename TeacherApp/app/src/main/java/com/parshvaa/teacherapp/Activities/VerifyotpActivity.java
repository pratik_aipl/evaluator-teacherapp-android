package com.parshvaa.teacherapp.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.BuildConfig;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Interface.OTPListener;
import com.parshvaa.teacherapp.Models.MainUser;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.MySMSBroadCastReceiver;
import com.parshvaa.teacherapp.Utils.OtpView;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class VerifyotpActivity extends BaseActivity implements OTPListener, AsynchTaskListner {
    public VerifyotpActivity instance;
    public TextView tv_resend_otp, tv_resend_otp_time;
    public Button btn_confirm;
    public String Msg_otp = "", PleyaerID = "", device_id = "";
    public boolean isFromMobileNoLogin = false;
    public Handler customHandler = new Handler();
    public MainUser mainUser;
    public JsonParserUniversal jParser;
    private OtpView mOtpView;

    TextView tv_mobileno;
    String mobileno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifyotp);

        instance = VerifyotpActivity.this;
        jParser = new JsonParserUniversal();
        tv_mobileno = findViewById(R.id.tv_mobileno);

        mobileno = getIntent().getStringExtra("MobileNo");
        tv_mobileno.setText("xxxxxxx" + (mobileno.length() >= 3 ? mobileno.substring(mobileno.length() - 3) : ""));

        device_id = Utils.getIMEInumber(instance);
        PleyaerID = Utils.OneSignalPlearID();

        tv_resend_otp = findViewById(R.id.tv_resend_otp);

        tv_resend_otp_time = findViewById(R.id.tv_resend_otp_time);
        btn_confirm = findViewById(R.id.btn_confirm);

        String htmlString = "<u>RESEND OTP</u>";
        tv_resend_otp.setText(Html.fromHtml(htmlString));
        mOtpView = (OtpView) findViewById(R.id.otp_view);
        if (BuildConfig.DEBUG){
            mOtpView.setOTP(getIntent().getStringExtra("otp"));
        }
        MySMSBroadCastReceiver.bind(instance, "KARAN");
        if (getIntent().hasExtra("MobLogin")) {
            PleyaerID = getIntent().getStringExtra("PleyaerID");
        }
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(instance, DashboardActivity.class);
                startActivity(intent);
            }
        });

        tv_resend_otp.setClickable(false);
        tv_resend_otp.setAlpha(0.5f);
        new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                SimpleDateFormat tempDf = new SimpleDateFormat("mm:ss", Locale.ENGLISH);
                tv_resend_otp_time.setText(" ( " + String.format("%02d : %02d" + " )",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

            }

            public void onFinish() {
                tv_resend_otp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        start();
                        new CallRequest(instance).resendOtp();
                    }
                });

                tv_resend_otp.setClickable(true);
                tv_resend_otp.setAlpha(1f);

            }
        }.start();


        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(instance)) {
                    verfyOTPandFrowedNext();
                } else {
                    Utils.showToast("Please connect internet to Verify OTP", instance);
                }
            }


        });

    }

    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            if (!PleyaerID.equalsIgnoreCase("")) {
                new CallRequest(instance).confirmOtp(mOtpView.getOTP().toString(), PleyaerID, device_id);
                customHandler.removeCallbacks(updateTimerThread);

            } else {
                PleyaerID = Utils.OneSignalPlearID();
                customHandler.postDelayed(this, 1000);
            }

        }
    };

    public void verfyOTPandFrowedNext() {
        if (mOtpView.getOTP().isEmpty()) {
            Utils.showToast("Please enter otp", instance);
        } else {
            if (PleyaerID.equalsIgnoreCase("")) {
                PleyaerID = Utils.OneSignalPlearID();
                showProgress(true);
                customHandler.postDelayed(updateTimerThread, 0);

            } else {
                new CallRequest(instance).confirmOtp(mOtpView.getOTP().toString(), PleyaerID, device_id);
            }
        }
    }

    @Override
    public void onBackPressed() {

        this.finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void otpReceived(String messageText) {
        Msg_otp = messageText.substring(0, 4);
        mOtpView.setOTP(messageText.substring(0, 4));
        if (!this.isDestroyed()) {
            if (Utils.isNetworkAvailable(instance)) {
                verfyOTPandFrowedNext();
            } else {
                Utils.showToast("Please connect internet to Verify OTP", instance);
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
            showProgress(false);
        if (result != null && !result.isEmpty()) {
            Log.i(result, "RESULT");

            switch (request) {

                case resend_otp:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            tv_resend_otp.setClickable(false);
                            tv_resend_otp.setAlpha(0.5f);


                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case confirmOtp:
                    customHandler.removeCallbacks(updateTimerThread);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            JSONObject jDAta = jObj.getJSONObject("data");
                            JSONObject jUser = jDAta.getJSONObject("user");
                            Utils.showToast(jObj.getString("message"), this);

                            mySharedPref.setIsLoggedIn(true);
                           // App.login_token = jDAta.getString("login_token");
                           // mySharedPref.setLoginToken(App.login_token);
                            mySharedPref.setClassId(jUser.getString("ClassID"));

                            mainUser = (MainUser) jParser.parseJson(jUser, new MainUser());
                            Gson gson = new Gson();
                            String json = gson.toJson(mainUser);
                            mySharedPref.setUserModel(json);

                            mainUser = mainUser;
                            Intent intent = new Intent(instance, DashboardActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Utils.showToast(jObj.getString("message"), this);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
