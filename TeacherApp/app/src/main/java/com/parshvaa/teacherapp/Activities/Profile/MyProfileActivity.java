package com.parshvaa.teacherapp.Activities.Profile;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.Standard;
import com.parshvaa.teacherapp.Models.Subject;
import com.parshvaa.teacherapp.Models.UserDetail;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyProfileActivity extends BaseActivity implements AsynchTaskListner {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    public MyProfileActivity instance;
    public TextView tv_username, tv_name, tv_contact, tv_email;
    public UserDetail userDetail;
    public JsonParserUniversal jParser;
    public Standard standard;
    public Subject subject;
    public ImageView img_back,img_edit,img_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile2);
        instance = MyProfileActivity.this;
        jParser = new JsonParserUniversal();
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewpager);
        img_back = findViewById(R.id.img_back);
        img_edit = findViewById(R.id.img_edit);
        img_profile = findViewById(R.id.img_profile);
        tv_username = findViewById(R.id.tv_username);
        tv_name = findViewById(R.id.tv_name);
        tv_contact = findViewById(R.id.tv_contact);
        tv_email = findViewById(R.id.tv_email);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(instance, EditProfileActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        for (int i = 0; i < userDetail.standardArray.size(); i++) {
            ProfileFragment fView = new ProfileFragment();
            adapter.addFrag(fView, userDetail.standardArray.get(i).getStandardName());
        }

        viewPager.setAdapter(adapter);
    }

    private void setupTabFont() {
        for (int j = 0; j < userDetail.standardArray.size(); j++) {
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View OverAll = layoutInflater.inflate(R.layout.custom_profile_tab, null, false);

            TextView tv_standard = OverAll.findViewById(R.id.custom_text);
            TextView tv_meduim = OverAll.findViewById(R.id.tv_meduim);

            tv_standard.setText(userDetail.standardArray.get(j).getStandardName());
            tv_meduim.setText(userDetail.standardArray.get(j).getMediumName());
            tabLayout.getTabAt(j).setCustomView(OverAll);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onResume() {
        super.onResume();
        showProgress(true);
        new CallRequest(instance).getUserDetails();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case getUserDetails:
                        jObj = new JSONObject(result);


                        if (jObj.getBoolean("status")) {
                             JSONObject jData = jObj.getJSONObject("data");
                            userDetail = (UserDetail) jParser.parseJson(jData, new UserDetail());
                            tv_name.setText(userDetail.getFirst_name() + " " + userDetail.getLast_name());
                            tv_contact.setText(userDetail.getRegister_mobile());
                            tv_email.setText(userDetail.getEmail());
                            tv_username.setText(userDetail.getUsername());
                            if(!TextUtils.isEmpty(userDetail.getClassRoundLogo())){
                                Picasso.with(instance)
                                        .load(userDetail.getClassRoundLogo())
                                        .error(R.mipmap.ic_logo) //this is optional the image to display while the url image is downloading
                                        .into(img_profile);
                            }


                            JSONArray jStdArray = jData.getJSONArray("standard");
                            for (int i = 0; i < jStdArray.length(); i++) {
                                JSONObject jStdObj = jStdArray.getJSONObject(i);
                                standard = (Standard) jParser.parseJson(jStdObj, new Standard());
                                JSONArray jSubArray = jStdObj.getJSONArray("subjects");
                                for (int j = 0; j < jSubArray.length(); j++) {
                                    JSONObject jSubObj = jSubArray.getJSONObject(j);
                                    subject = (Subject) jParser.parseJson(jSubObj, new Subject());
                                    standard.subjectsArray.add(subject);
                                }
                                userDetail.standardArray.add(standard);
                                App.userDetail = userDetail;

                            }
                            setupViewPager(viewPager);
                            setupTabFont();

                        }

                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        public Fragment getItem(int position) {
            return ProfileFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
