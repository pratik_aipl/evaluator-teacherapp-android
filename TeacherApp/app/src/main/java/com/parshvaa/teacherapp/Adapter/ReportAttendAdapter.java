package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.teacherapp.Interface.StudentOnClick;
import com.parshvaa.teacherapp.Models.AttendStudent;
import com.parshvaa.teacherapp.R;

import java.util.List;


public class ReportAttendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<AttendStudent> attendStudentList;
     StudentOnClick studentOnClick;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReportAttendAdapter(Context context, List<AttendStudent> attendStudentList) {
        this.context = context;
        this.attendStudentList = attendStudentList;
        this.studentOnClick = (StudentOnClick) context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_attend_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;

        AttendStudent attendStudent = attendStudentList.get(position);

        holder.mStudentName.setText(attendStudent.getFirstName());
        holder.mCorrectAns1.setText("" + attendStudent.getCorrect());
        holder.mInCorrectAns.setText("" + attendStudent.getIncorrect());
        holder.mNotAns.setText("" + (attendStudent.getTotalQquestion() - attendStudent.getTotalAttempt()));
        holder.mTotalQuestion1.setText("" + attendStudent.getTotalQquestion());
        holder.mTotalQuestion2.setText("" + attendStudent.getTotalQquestion());
        holder.mTotalQuestion3.setText("" + attendStudent.getTotalQquestion());

        holder.mStudentName.setOnClickListener(v -> studentOnClick.callbackStudent(position, attendStudent));

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return attendStudentList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mStudentName, mCorrectAns1, mTotalQuestion1, mInCorrectAns, mTotalQuestion2, mNotAns, mTotalQuestion3;

        public ViewHolder(View v) {
            super(v);

            mStudentName = v.findViewById(R.id.mStudentName);
            mCorrectAns1 = v.findViewById(R.id.mCorrectAns1);
            mTotalQuestion1 = v.findViewById(R.id.mTotalQuestion1);
            mInCorrectAns = v.findViewById(R.id.mInCorrectAns);
            mTotalQuestion2 = v.findViewById(R.id.mTotalQuestion2);
            mNotAns = v.findViewById(R.id.mNotAns);
            mTotalQuestion3 = v.findViewById(R.id.mTotalQuestion3);

        }
    }
}
