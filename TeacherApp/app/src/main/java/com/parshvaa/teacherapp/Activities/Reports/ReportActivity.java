package com.parshvaa.teacherapp.Activities.Reports;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.BottomSheetTypeDialog;
import com.parshvaa.teacherapp.Adapter.LevelAdapter;
import com.parshvaa.teacherapp.Adapter.ReportAttendAdapter;
import com.parshvaa.teacherapp.Adapter.ReportNotAttendAdapter;
import com.parshvaa.teacherapp.Adapter.StudentMarksAdapter;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Interface.ChangeReportClick;
import com.parshvaa.teacherapp.Models.AttendStudent;
import com.parshvaa.teacherapp.Models.LevelBean;
import com.parshvaa.teacherapp.Models.StudentMarks;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ReportActivity extends BaseActivity implements AsynchTaskListner, ChangeReportClick {

    TextView mAccuracy, mNotAttemptStudent, mComplete, tv_title, mCompleteLabel, mNotCompleteLabel;
    RecyclerView mBatchList, mAttemptStudents, mStudentMarksList, mNotAttemptStudents;
    LinearLayout mStudentMarksView;
    ImageView mEmptyData;
    RelativeLayout mCCTReport1, mCCTReport2;
    public ArrayList<LevelBean> levellist = new ArrayList<>();
    List<AttendStudent> attendStudentList = new ArrayList<>();
    List<AttendStudent> notAttendStudentList = new ArrayList<>();
//    AttendStudent attendStudent;
//    AttendStudent notAttendStudent;

    public JsonParserUniversal jParser;
    public String PaperID = "";
    public ImageView img_back;
    private static final String TAG = "ReportActivity";
    public BottomSheetTypeDialog bottomSheetTypeDialog;
    String pageTitle = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        PaperID = getIntent().getStringExtra("PaperId");
        pageTitle = getIntent().getStringExtra(Constant.title);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_title.setText(pageTitle);

        mEmptyData = findViewById(R.id.mEmptyData);
        mStudentMarksView = findViewById(R.id.mStudentMarksView);
        mCompleteLabel = findViewById(R.id.mCompleteLabel);
        mNotCompleteLabel = findViewById(R.id.mNotCompleteLabel);
        mCCTReport1 = findViewById(R.id.mCCTReport1);
        mCCTReport2 = findViewById(R.id.mCCTReport2);


        mAccuracy = findViewById(R.id.mAccuracy);
        mBatchList = findViewById(R.id.mBatchList);
        mStudentMarksList = findViewById(R.id.mStudentMarksList);
        mAttemptStudents = findViewById(R.id.mAttemptStudents);
        mNotAttemptStudents = findViewById(R.id.mNotAttemptStudents);
        mNotAttemptStudent = findViewById(R.id.mNotAttemptStudent);
        mComplete = findViewById(R.id.mComplete);
        jParser = new JsonParserUniversal();

        if (pageTitle.equalsIgnoreCase("CCT Report")) {
            mStudentMarksView.setVisibility(View.GONE);
            mCCTReport1.setVisibility(View.VISIBLE);
            mCCTReport2.setVisibility(View.VISIBLE);
            if (Utils.isNetworkAvailable(this)) {
                showProgress(true);
                new CallRequest(ReportActivity.this).getReports(PaperID);
                new CallRequest(ReportActivity.this).getReposrtsDetails(PaperID);
            }
        } else {
            mCCTReport1.setVisibility(View.GONE);
            mCCTReport2.setVisibility(View.GONE);
            mStudentMarksView.setVisibility(View.VISIBLE);
            if (Utils.isNetworkAvailable(this)) {
                showProgress(true);
                new CallRequest(ReportActivity.this).getReportsTest(PaperID);
            }
        }


        mCompleteLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetTypeDialog = BottomSheetTypeDialog.newInstance();
                bottomSheetTypeDialog.show(getSupportFragmentManager(), "Custom Bottom Sheet");
            }
        });
        mNotCompleteLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetTypeDialog = BottomSheetTypeDialog.newInstance();
                bottomSheetTypeDialog.show(getSupportFragmentManager(), "Custom Bottom Sheet");
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case getReports:
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {

                            mAccuracy.setText(jObj.getString("Performance") + "%");
                            mNotAttemptStudent.setText(jObj.getString("TotalStudents"));
                            mComplete.setText(jObj.getString("ToalStudentsAppeared"));

                            levellist.add(new LevelBean(1, "Level1", 0, 0, 0, jObj.getInt("Level1")));
                            levellist.add(new LevelBean(2, "Level2", 0, 0, 0, jObj.getInt("Level2")));
                            levellist.add(new LevelBean(3, "Level3", 0, 0, 0, jObj.getInt("Level3")));
                            mBatchList.setAdapter(new LevelAdapter(this, levellist));

                            ReportAttendAdapter reportAttendAdapter = new ReportAttendAdapter(this, attendStudentList);
                            mAttemptStudents.setAdapter(reportAttendAdapter);
                        }
                        break;
                    case getReportsTest:
                        jObj = new JSONObject(result);
                        Log.d(TAG, "onTaskCompleted: " + result);
                        if (jObj.getBoolean("status")) {
                            mAccuracy.setText(jObj.getString("paper_accuracy") + "%");
                            String totalMarks = jObj.getString("paper_marks");
                            List<StudentMarks> studentMarksList = new ArrayList<>();
                            JSONArray studentMarkList = jObj.getJSONArray("report_data");
                            for (int i = 0; i < studentMarkList.length(); i++) {
                                JSONObject detail = studentMarkList.getJSONObject(i);
                                StudentMarks studentMarks = new StudentMarks();
                                // attendStudent = (AttendStudent) jParser.parseJson(complete.getJSONObject(i), new AttendStudent());
                                studentMarks.setFirstName(detail.getString("FirstName"));
                                studentMarks.setLastName(detail.getString("LastName"));
                                studentMarks.setStudentMark(detail.getString("StudentMark"));
                                studentMarksList.add(studentMarks);
                            }
                            StudentMarksAdapter studentMarksAdapter = new StudentMarksAdapter(this, studentMarksList, totalMarks);
                            mStudentMarksList.setAdapter(studentMarksAdapter);

                            if (studentMarksList.size() == 0) {
                                mStudentMarksList.setVisibility(View.GONE);
                                mEmptyData.setVisibility(View.VISIBLE);
                            } else {
                                mStudentMarksList.setVisibility(View.VISIBLE);
                                mEmptyData.setVisibility(View.GONE);
                            }
                        }
                        break;
                    case getReportsDetails:
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {

                            JSONArray complete = jObj.getJSONArray("student_complete_data");
                            for (int i = 0; i < complete.length(); i++) {
                                JSONObject detail = complete.getJSONObject(i);
                                AttendStudent attendStudent = new AttendStudent();
                                // attendStudent = (AttendStudent) jParser.parseJson(complete.getJSONObject(i), new AttendStudent());
                                attendStudent.setFirstName(detail.getString("FirstName"));
                                attendStudent.setLastName(detail.getString("LastName"));
                                attendStudent.setCorrect(detail.getInt("Correct"));
                                attendStudent.setIncorrect(detail.getInt("Incorrect"));
                                attendStudent.setTotalQquestion(detail.getInt("TotalQquestion"));
                                attendStudent.setTotalAttempt(detail.getInt("TotalAttempt"));
                                attendStudentList.add(attendStudent);
                            }
                            ReportAttendAdapter reportAttendAdapter = new ReportAttendAdapter(this, attendStudentList);
                            mAttemptStudents.setAdapter(reportAttendAdapter);

                            JSONArray notcomplete = jObj.getJSONArray("student_incomplete_data");
                            for (int j = 0; j < notcomplete.length(); j++) {
                                JSONObject notdetail = notcomplete.getJSONObject(j);
                                AttendStudent notAttendStudent = new AttendStudent();

                                notAttendStudent.setFirstName(notdetail.getString("FirstName"));
                                notAttendStudent.setLastName(notdetail.getString("LastName"));
                                notAttendStudentList.add(notAttendStudent);
                            }

                            ReportNotAttendAdapter reportNotAttendAdapter = new ReportNotAttendAdapter(this, notAttendStudentList);
                            mNotAttemptStudents.setAdapter(reportNotAttendAdapter);

                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void ChangeTypeEvent(boolean isAttend) {
        showView(isAttend);
    }

    private void showView(boolean isAttend) {
        if (isAttend) {
            mCompleteLabel.setVisibility(View.VISIBLE);
            mAttemptStudents.setVisibility(View.VISIBLE);
            mNotCompleteLabel.setVisibility(View.GONE);
            mNotAttemptStudents.setVisibility(View.GONE);
        } else {
            mCompleteLabel.setVisibility(View.GONE);
            mAttemptStudents.setVisibility(View.GONE);
            mNotCompleteLabel.setVisibility(View.VISIBLE);
            mNotAttemptStudents.setVisibility(View.VISIBLE);
        }
    }
}
