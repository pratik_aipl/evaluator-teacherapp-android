package com.parshvaa.teacherapp.Activities;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.common.Util;
import com.parshvaa.teacherapp.Activities.Profile.MyProfileActivity;
import com.parshvaa.teacherapp.AddNewFragment;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.DashbordFragment;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.SearchFragment;
import com.parshvaa.teacherapp.SplashActivity;
import com.parshvaa.teacherapp.Utils.Utils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;


public class DashboardActivity extends BaseActivity {


    private static final String TAG = "DashboardActivity";
    @BindView(R.id.img_drawer)
    ImageView imgDrawer;
    @BindView(R.id.tv_dashboard)
    TextView tvDashboard;
    @BindView(R.id.rel)
    LinearLayout rel;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.framelayout)
    FrameLayout framelayout;
    @BindView(R.id.navigation)
    NavigationView navigation;
    @BindView(R.id.activity_main)
    DrawerLayout drawerLayout;

    ActionBarDrawerToggle t;

    TextView nav_userName;
    ImageView img_profile;
   // Subscription subscription;

    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_add)
    ImageView imgAdd;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);



        drawerLayout.addDrawerListener(t);
        selectItem(0);
        bottomNavigation.setVisibility(View.VISIBLE);

        View headerView = navigation.getHeaderView(0);
        img_profile = headerView.findViewById(R.id.img_profile);
        nav_userName = headerView.findViewById(R.id.nav_userName);
        nav_userName.setText(mainUser.getFirst_name() + " " + mainUser.getLast_name());
        if(!TextUtils.isEmpty(mainUser.getClassRoundLogo())){
            Picasso.with(this)
                    .load(mainUser.getClassRoundLogo())
                    .error(R.drawable.profile) //this is optional the image to display while the url image is downloading
                    .into(img_profile);
        }

     /*   if(L.isNetworkAvailable(this)){
            CheckUserLogin();
        }*/

        Log.d(TAG, "getLoginToken: "+mySharedPref.getLoginToken());

        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            switch (id) {
                case R.id.home:
                    selectItem(0);
                    break;
                    case R.id.myprofile:
                    selectItem(1);
                    break;
                    case
                        R.id.query:
                    selectItem(2);
                    break;
                    case
                        R.id.refer:
                    selectItem(3);
                    break;
                    case
                        R.id.rate:
                    selectItem(4);
                    break;
                    case
                            R.id.LOGOUT:
                            selectItem(5);
                    break;


                default:
                    break;
            }
            return true;

        });


    }

   /* private void CheckUserLogin() {
       // showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getUserLogin(), (data) -> {
            //showProgress(false);

            if (data.code() == 200) {

                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if(!jsonResponse.getBoolean("status")){
                        Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        L.logout(this);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
           // showProgress(false);
            e.printStackTrace();
        });

    }
*/
    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.framelayout, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.B_navigation_home:

                    if(!item.isChecked()){
                        selectItem(0);
                        tvDashboard.setText("DASHBOARD");
                    }


                    return true;
                case R.id.B_navigation_search:
                    imgAdd.setVisibility(View.VISIBLE);
                    imgSearch.setVisibility(View.GONE);
                    tvDashboard.setText("SEARCH");
                    openFragment(new SearchFragment());
                    return true;
                case R.id.B_navigation_report:
                   // openFragment(new ProspectFragment());
                    tvDashboard.setText("REPORT");
                    new AlertDialog.Builder(DashboardActivity.this)
                            .setMessage(("COMING SOON...!"))
                            .setPositiveButton("ok", (dialog, which) -> {
                                drawerLayout.closeDrawer(Gravity.LEFT);
                                dialog.dismiss();
                            })
                            .show();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
    }


    @OnClick({R.id.img_drawer, R.id.img_add, R.id.img_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_drawer:
                //L.hideKeyboard(this, drawerLayout);
                drawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.img_add:
                imgAdd.setVisibility(View.GONE);
                imgSearch.setVisibility(View.GONE);
                openFragment(new AddNewFragment());
                bottomNavigation.setVisibility(View.GONE);
                tvDashboard.setText("ADD NEW");
                break;
            case R.id.img_search:
            //    startActivity(new Intent(this, SearchActivity.class));
                break;
        }
    }

    void showheader(){
        imgAdd.setVisibility(View.GONE);
        imgSearch.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {
        if (bottomNavigation.getVisibility() == View.GONE) {
            //showheader();
            selectItem(0);
            tvDashboard.setText("DASHBOARD");
            imgAdd.setVisibility(View.VISIBLE);
        } else {
            exitalert();
        }
    }

    public void exitalert() {
        new AlertDialog.Builder(DashboardActivity.this)
                .setMessage(("Are you sure to Exit?"))
                .setPositiveButton("YES", (dialog, which) -> {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                    finish();
                })
                .setNegativeButton("NO", null)
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       /* if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    private void selectItem(int position) {

        Fragment fragment = null;
        Bundle bundle = new Bundle();

        switch (position) {
            case 0:
                //showheader();
                 fragment = new DashbordFragment();
//                fragment = new LeadsFragment();
                fragment.setArguments(bundle);
                bottomNavigation.setVisibility(View.VISIBLE);
                imgAdd.setVisibility(View.VISIBLE);

                // tvDashboard.setText(user.getUsername());

                // tvDashboard.setText(getString(R.string.buyer_dashboard));
                break;
                case 1:
                   startActivity(new Intent(DashboardActivity.this, MyProfileActivity.class));
                  //  startActivity(new Intent(DashboardActivity.this, OnlineClassDemoActivity.class));
                break;
                case 2:
                    startActivity(new Intent(this, QueryCallUsActivity.class));
                break;
                case 3:
                    ReferFriend();
                break;
                case 4:
                    launchMarket();
                break;
                case 5:
                   // startActivity(new Intent(DashboardActivity.this, OnlineClassActivity.class));
                    logout();
                break;


            default:
                break;
        }

        if (fragment != null) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.framelayout, fragment).commit();
        } else {
            Log.e("Dash", "Error in creating fragment");

        }
    }

    public void logout() {
        new AlertDialog.Builder(DashboardActivity.this)
                .setMessage(("Are you sure to Logout?"))
                .setPositiveButton("YES", (dialog, which) -> {
                    mySharedPref.clearApp();
                    mySharedPref.setIsLoggedIn(false);
                    mySharedPref.setIsLoggedIn(false);
                    mainUser=null;
                    startActivity(new Intent(this, TourActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                })
                .setNegativeButton("NO", null)
                .show();
    }
    public void ReferFriend() {
        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.parshvaa.iscore&hl=en");
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "iSocre");
        i.putExtra(Intent.EXTRA_TEXT, String.valueOf(uri));
        startActivity(Intent.createChooser(i, "Share via"));
    }

    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Utils.Log("TAG url ::--> ", String.valueOf(uri));
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }
}
