package com.parshvaa.teacherapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.parshvaa.teacherapp.App;


public class MySharedPref {
    public App app;
    SharedPreferences shared;
    SharedPreferences.Editor et;
    public final String isLoggedIn = "isLoggedIn";
    public final String user_id = "user_id";
    public final String login_token = "token";
    public final String userModel = "userModel";
    public final String classId = "class_id";

    public MySharedPref(Context ct) {

        shared = ct.getSharedPreferences(App.myPref, 0);
        et = shared.edit();
    }

    public boolean getIsLoggedIn() {
        return shared.contains(isLoggedIn) ? shared.getBoolean(isLoggedIn, false) : false;
    }

    public void setIsLoggedIn(boolean isfirst) {
        et.putBoolean(isLoggedIn, isfirst);
        et.commit();
    }

    public String getUserModel() {
        return shared.contains(userModel) ? shared.getString(userModel, "") : "";
    }

    public void setUserModel(String UserModel) {
        et.putString(userModel, UserModel);
        et.commit();
    }

    public String getClassId() {
        return shared.contains(classId) ? shared.getString(classId, "") : "";
    }

    public void setClassId(String ClassId) {
        et.putString(classId, ClassId);
        et.commit();
    }

    public String getUserId() {
        return shared.contains(user_id) ? shared.getString(user_id, "") : "";
    }

    public void setUserId(String UserId) {
        et.putString(user_id, UserId);
        et.commit();
    }

    public String getLoginToken() {
        return shared.contains(login_token) ? shared.getString(login_token, "") : "";
    }

    public void setLoginToken(String Login_token) {
        et.putString(login_token, Login_token);
        et.commit();
        et.apply();
    }
    public void clearApp() {
        et.clear();
        et.apply();
        et.commit();
    }
}
