package com.parshvaa.teacherapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Adapter.LevelAdapter;
import com.parshvaa.teacherapp.Adapter.ReportAttendAdapter;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.AttendStudent;
import com.parshvaa.teacherapp.Models.LevelBean;
import com.parshvaa.teacherapp.Models.LevelQuestion;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.RobotoBoldTextview;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainReportsActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "ReportActivity";

  //  PaperBean paperBean;
    boolean isAttend = true;
    //PlannerSummary plannerSummary;

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    RobotoBoldTextview tvTitle;
    @BindView(R.id.mTop)
    LinearLayout mTop;
    @BindView(R.id.mPerformance)
    TextView mPerformance;
    @BindView(R.id.mAccuracy)
    TextView mAccuracy;
    @BindView(R.id.mBatchList)
    RecyclerView mBatchList;
    @BindView(R.id.mContainer)
    LinearLayout mContainer;
    @BindView(R.id.mTopView)
    LinearLayout mTopView;
    @BindView(R.id.mStudent)
    TextView mStudent;
    @BindView(R.id.lin_stu)
    LinearLayout linStu;
    @BindView(R.id.mStudentViewLine)
    View mStudentViewLine;
    @BindView(R.id.mNotAttemptStudent)
    TextView mNotAttemptStudent;
    @BindView(R.id.mComplete)
    TextView mComplete;
    @BindView(R.id.mCCTReport1)
    RelativeLayout mCCTReport1;
    @BindView(R.id.mquestion)
    TextView mquestion;
    @BindView(R.id.lin_question)
    LinearLayout linQuestion;
    @BindView(R.id.mFirstquestion)
    TextView mFirstquestion;
    @BindView(R.id.mLine)
    View mLine;
    @BindView(R.id.mCorrectAns1)
    TextView mCorrectAns1;
    @BindView(R.id.mInCorrectAns)
    TextView mInCorrectAns;
    @BindView(R.id.mNotAns)
    TextView mNotAns;
    @BindView(R.id.mne)
    View mne;
    @BindView(R.id.mavgtime)
    TextView mavgtime;
    @BindView(R.id.mEmptyData)
    ImageView mEmptyData;
    @BindView(R.id.mStudentMarksView)
    LinearLayout mStudentMarksView;
    @BindView(R.id.mDataView)
    NestedScrollView mDataView;

    String CLASSMCQTESTHDRID;
  public ArrayList<LevelBean> levellist = new ArrayList<>();
  List<LevelQuestion> QuestionList = new ArrayList<>();

  @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_reports);
        ButterKnife.bind(this);

        tvTitle.setText("Reports");
      CLASSMCQTESTHDRID=getIntent().getStringExtra("CLASSMCQTESTHDRID");
      Log.d(TAG, "IDD: "+CLASSMCQTESTHDRID);
       // paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.paper);

        if (Utils.isNetworkAvailable(MainReportsActivity.this)){
          showProgress(true);
          new CallRequest(MainReportsActivity.this).getReport(CLASSMCQTESTHDRID);
        }

        imgBack.setOnClickListener(view -> onBackPressed());
        linStu.setOnClickListener(view -> startActivity(new Intent(this, ReportStudentActivity.class)
                .putExtra("CLASSMCQTESTHDRID", CLASSMCQTESTHDRID)));
        linQuestion.setOnClickListener(view -> {
          if(QuestionList.size()!=0){
            startActivity(new Intent(this, ReportQuestionsActivity.class)
                    .putExtra(Constant.questionsList, (Serializable) QuestionList));
          }else{
            Toast.makeText(this, "QuestionList Not Available", Toast.LENGTH_SHORT).show();
          }

        });



    }

  @Override
  public void onTaskCompleted(String result, Constant.REQUESTS request) {
    showProgress(false);
    JSONObject jObj;
    Log.d(TAG, "onTaskCompleted: "+result);
    if (result != null && !result.isEmpty()) {
      switch (request) {

        case getReport:
          try {
            jObj = new JSONObject(result);
            Log.d(TAG, "onTaskCompletedresult: "+result);
            if (jObj.getBoolean("status")) {
              JSONObject data=jObj.getJSONObject("Data");
              JSONArray question=data.getJSONArray("Question");

              mAccuracy.setText(data.getString("OverallAccuracy") + "%");
              mNotAttemptStudent.setText(data.getString("StudentNotCompleted"));
              mComplete.setText(data.getString("StudentCompleted"));
              mavgtime.setText(data.getString("AvgTakenTime")+"\nTotal Time\nTaken");

              levellist.add(new LevelBean(1, "Level 1", 0, 0, 0, data.getInt("Level1Question")));
              levellist.add(new LevelBean(2, "Level 2", 0, 0, 0, data.getInt("Level2Question")));
              levellist.add(new LevelBean(3, "Level 3", 0, 0, 0, data.getInt("Level3Question")));
              mBatchList.setAdapter(new LevelAdapter(this, levellist));

              mCorrectAns1.setText(question.getJSONObject(0).getString("CorrectAccuracy"));
              mInCorrectAns.setText(question.getJSONObject(0).getString("InCorrectAccuracy"));
              mNotAns.setText(question.getJSONObject(0).getString("NotAttemptAccuracy"));

              mFirstquestion.setText("1) "+Html.fromHtml(question.getJSONObject(0).getJSONObject("Question").getString("Question")));


              for (int i = 0; i < question.length(); i++) {
                JSONObject detail = question.getJSONObject(i);
                JSONObject quest = detail.getJSONObject("Question");
                LevelQuestion levelQuestion = new LevelQuestion();

                levelQuestion.setQuestion(quest.getString("Question"));
                levelQuestion.setCorrect(detail.getInt("CorrectAccuracy"));
                levelQuestion.setInCorrect(detail.getInt("InCorrectAccuracy"));
                levelQuestion.setNotAttempt(detail.getInt("NotAttemptAccuracy"));
                QuestionList.add(levelQuestion);
              }
              Log.d(TAG, "oquestionsize: "+QuestionList.size());

            }
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
      }

    }
  }
}
