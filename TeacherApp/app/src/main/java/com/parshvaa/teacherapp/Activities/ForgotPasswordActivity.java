package com.parshvaa.teacherapp.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.RobotoBoldButton;
import com.parshvaa.teacherapp.Utils.RobotoEditTextView;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends BaseActivity implements AsynchTaskListner {
   // public ForgotPasswordActivity instance;
    //public TextView tv_login;
    public RobotoBoldButton btn_forgot;
    public RobotoEditTextView et_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
     //   instance = this;
      //  tv_login = findViewById(R.id.tv_login);
        btn_forgot = findViewById(R.id.btn_forgot);
        et_email = findViewById(R.id.et_email);

//        tv_login.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(instance, LoginActivity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.rotate_in, R.anim.rotate_out);
//                finishAffinity();
//            }
//        });

        btn_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_email.getText().toString().equals("")) {
                    et_email.setError("Please enter registered email-id");
                } else if (!Utils.isValidEmail(et_email.getText().toString())) {
                    et_email.setError("Please enter validate email-id");

                } else {
                    if (Utils.isNetworkAvailable(ForgotPasswordActivity.this)) {
                        showProgress(true);
                        new CallRequest(ForgotPasswordActivity.this).forgotPassword(et_email.getText().toString());
                    }
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            Log.i(result, "RESULT");
            try {
                switch (request) {
                    case forgotPassword:
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast(jObj.getString("message"), this);

                            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.rotate_in, R.anim.rotate_out);
                            finishAffinity();
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                        break;
                }
            } catch (JSONException e) {
                Utils.showToast("Invalid Login ID or Password", this);
                e.printStackTrace();
            }
        }
    }
}
