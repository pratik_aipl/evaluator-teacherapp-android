package com.parshvaa.teacherapp.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Utils;

public class ZookiWebActivity extends BaseActivity {
    public WebView web_newslastter;
    String url = "";

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zooki_web);
        showProgress(true);
        Intent i = getIntent();
        if (i.hasExtra("url")) {
            url = i.getExtras().getString("url");
        }
        Log.i("TAG", "URL::->" + url);
        web_newslastter = findViewById(R.id.web_newslastter);
        web_newslastter.setWebViewClient(new myWebClient());
        Utils.setWebViewSettings(web_newslastter);

        if (!TextUtils.isEmpty(url)) {
            web_newslastter.loadUrl(url);
        } else {
            web_newslastter.loadUrl("http://parshvaa.com/blog_copy/");
        }
    }


    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
//            view.loadUrl(url);
            return false;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            showProgress(false);
        }
    }

}

