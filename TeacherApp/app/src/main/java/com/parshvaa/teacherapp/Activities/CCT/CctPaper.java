package com.parshvaa.teacherapp.Activities.CCT;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 6/12/2018.
 */

public class CctPaper implements Serializable {
    public String ClassCCTPlannerID = "";
    public String ClassMCQTestHDRID = "";
    public String CreatedOn = "";
    public String PaperMark = "";
    public String BoardName = "";
    public String MediumName = "";
    public String StandardID = "";
    public String StandardName = "";
    public String SubjectID = "";
    public String SubjectName = "";
    public String first_name = "";
    public String AssignType = "";
    public String last_name = "";
    public String Status = "";
    public String PaperStatus = "";

    public String getClassCCTPlannerID() {
        return ClassCCTPlannerID;
    }

    public void setClassCCTPlannerID(String classCCTPlannerID) {
        ClassCCTPlannerID = classCCTPlannerID;
    }

    public String getClassMCQTestHDRID() {
        return ClassMCQTestHDRID;
    }

    public void setClassMCQTestHDRID(String classMCQTestHDRID) {
        ClassMCQTestHDRID = classMCQTestHDRID;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getPaperMark() {
        return PaperMark;
    }

    public void setPaperMark(String paperMark) {
        PaperMark = paperMark;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getAssignType() {
        return AssignType;
    }

    public void setAssignType(String assignType) {
        AssignType = assignType;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getPaperStatus() {
        return PaperStatus;
    }

    public void setPaperStatus(String paperStatus) {
        PaperStatus = paperStatus;
    }
}
