package com.parshvaa.teacherapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;


import com.parshvaa.teacherapp.Activities.CCT.CctPaperListActivity;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.Standard;
import com.parshvaa.teacherapp.Models.Subject;
import com.parshvaa.teacherapp.ObservScroll.BaseFragment;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddNewFragment extends BaseFragment implements AsynchTaskListner {


    @BindView(R.id.img_gnrtcct)
    ImageView img_gnrtcct;
    @BindView(R.id.img_gnrtppr)
    ImageView img_gnrtppr;

    private Unbinder unbinder;
    //Subscription subscription;


    View view;

    public AddNewFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.add_form_fregment, container, false);
        unbinder = ButterKnife.bind(this, view);


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.img_gnrtcct, R.id.img_gnrtppr})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_gnrtcct:
                showProgress(true);
                new CallRequest(AddNewFragment.this).fregcctAssginUrl();
                break;
            case R.id.img_gnrtppr:
               // startActivity(new Intent(getActivity(), ProspectActivity.class).putExtra(Constant.IsFrom,"ADD"));
                break;

        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case getcctPaperUrl:
                        try {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                if (jObj.length() > 0) {

                                    Intent intent = new Intent(getActivity(), CctPaperListActivity.class);
                                    intent.putExtra("PaperUrl", jObj.getString("cct_url"));
                                    intent.putExtra("backUrl", jObj.getString("go_back_url"));
                                    startActivity(intent);

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
