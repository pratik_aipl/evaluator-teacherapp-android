package com.parshvaa.teacherapp.Models;

import com.parshvaa.teacherapp.R;


public enum Tour {
    Intro1(R.string.tour1, R.layout.activity_solution1),
    Intro2(R.string.tour2, R.layout.activity_solution2),
    Intro3(R.string.tour3, R.layout.activity_solution3),
    Intro4(R.string.tour4, R.layout.activity_solution4),
    Intro5(R.string.tour5, R.layout.activity_solution5);

    private int mTitleResId;
    private int mLayoutResId;

    Tour(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}
