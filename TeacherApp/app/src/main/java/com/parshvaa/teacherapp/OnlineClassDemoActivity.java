package com.parshvaa.teacherapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Activities.Paper.PaperAssignedActivity;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class OnlineClassDemoActivity extends BaseActivity {
    private static final String TAG = "OnlineClassDemoActivity";
    public String PlannerID = "", PaperID = "";
    public ImageView img_back;
    public TextView tv_title;
    public String go_back_url = "";
    WebView myWebView;
    public ProgressDialog progressDialog;
    String CLASSLINK;

    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 101;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 102;
    private PermissionRequest myRequest;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        showProgress(true);
        CLASSLINK = getIntent().getStringExtra("CLASSLINK");
        PlannerID = getIntent().getStringExtra("PlannerID");
        PaperID = getIntent().getStringExtra("PaperID");
        myWebView = findViewById(R.id.webview);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);


        myWebView.setWebViewClient(new myWebClient());
        Utils.setWebViewSettings(myWebView);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        myWebView.setWebViewClient(new WebViewClient());

        myWebView.getSettings().setSaveFormData(true);
        myWebView.getSettings().setSupportZoom(false);
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        myWebView.getSettings().setPluginState(WebSettings.PluginState.ON);


        myWebView.setWebChromeClient(new WebChromeClient() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                myRequest = request;

                for (String permission : request.getResources()) {
                    switch (permission) {
                        case "android.webkit.resource.AUDIO_CAPTURE": {
                            askForPermission(request.getOrigin().toString(), Manifest.permission.RECORD_AUDIO, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
                            break;
                        }
                        case "android.webkit.resource.CAMERA": {
                            askForPermission(request.getOrigin().toString(), Manifest.permission.CAMERA, MY_PERMISSIONS_REQUEST_CAMERA);
                            break;
                        }
                    }
                }
            }
        });
        tv_title.setText("ONLINE CLASS");
        img_back.setOnClickListener(v -> onBackPressed());
       // Toast.makeText(this, ""+CLASSLINK, Toast.LENGTH_SHORT).show();
        //myWebView.loadUrl("https://api.braincert.com/html5/build/whiteboard.php?token=bRa3YxHOVEYtsfjtcS5vJyhkJutHghdfYxJyHnSJwBcBKHpwnttIC0WgRrXA6UdOTHVWpGdJItLMqXy6jyb6NJpf73GrBvLfDUWF0DN--2FirI0DNW--2Fu--2B7CepjRmbkiO0Om0sSnXQc9WpFF--2FCZQsaGBGziqtcuIpQQdj5cFnmQOc9XrdyBocMjLTzfufHAFqpN6ctQAA1XBXsK4ciaWpkOHqA--3D--3D");
        myWebView.loadUrl(CLASSLINK);
       // new CallRequest(OnlineClassDemoActivity.this).generatePaperUrl(PlannerID, PaperID);

    }

  /*  @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
                showProgress(false);
        if (result != null && !result.isEmpty()) {
            Log.i(result, "RESULT");
            try {

                switch (request) {

                    case generatePaperUrl:
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            if (Utils.isNetworkAvailableWebView(OnlineClassDemoActivity.this)) {
                                myWebView.loadUrl(jObj.getString("generate_paper_url"));
                            } else {
                                Utils.showNetworkAlert(OnlineClassDemoActivity.this);
                            }
                            go_back_url = jObj.getString("go_back_url");
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
*/ @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  public void askForPermission(String origin, String permission, int requestCode) {
      Log.d("WebView", "inside askForPermission for" + origin + "with" + permission);

      if (ContextCompat.checkSelfPermission(getApplicationContext(),
              permission)
              != PackageManager.PERMISSION_GRANTED) {

          // Should we show an explanation?
          if (ActivityCompat.shouldShowRequestPermissionRationale(OnlineClassDemoActivity.this,
                  permission)) {

              // Show an expanation to the user *asynchronously* -- don't block
              // this thread waiting for the user's response! After the user
              // sees the explanation, try again to request the permission.

          } else {

              // No explanation needed, we can request the permission.

              ActivityCompat.requestPermissions(OnlineClassDemoActivity.this,
                      new String[]{permission},
                      requestCode);
          }
      } else {
          myRequest.grant(myRequest.getResources());
      }
  }
    @Override
    public void onBackPressed() {
        if (myWebView.canGoBack()) {
            if (Utils.isNetworkAvailableWebView(OnlineClassDemoActivity.this)) {
                myWebView.goBack();
            } else {
                Utils.showNetworkAlert(OnlineClassDemoActivity.this);
            }
        } else {
            Intent intent = new Intent(OnlineClassDemoActivity.this, PaperAssignedActivity.class);
            startActivity(intent);
            finish();
            //super.onBackPressed();

        }
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
           /* if (progressDialog == null) {
                // in standard case YourActivity.this
                progressDialog = new ProgressDialog(PaperGenerateActivity.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();
            }*/
            if (Utils.isNetworkAvailableWebView(OnlineClassDemoActivity.this)) {
                view.loadUrl(url);
            } else {
                Utils.showNetworkAlert(OnlineClassDemoActivity.this);
            }
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            showProgress(false);

            try {
                if (url.equalsIgnoreCase(go_back_url)) {
                    Intent intent = new Intent(OnlineClassDemoActivity.this, PaperAssignedActivity.class);
                    startActivity(intent);
                    finish();
                }
                if (url.equalsIgnoreCase("https://test.pemevaluater.parshvaa.com/auth/login")) {
                    Intent intent = new Intent(OnlineClassDemoActivity.this, PaperAssignedActivity.class);
                    startActivity(intent);
                    finish();

                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

}

