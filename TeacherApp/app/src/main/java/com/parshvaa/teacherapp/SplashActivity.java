package com.parshvaa.teacherapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.parshvaa.teacherapp.Activities.CCT.CctAssignedActivity;
import com.parshvaa.teacherapp.Activities.ConcernActivity;
import com.parshvaa.teacherapp.Activities.DashboardActivity;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssignedActivity;
import com.parshvaa.teacherapp.Activities.ReadyReviewCct.ReadyReviewCctActivity;
import com.parshvaa.teacherapp.Activities.ReadyReviewPaper.ReadyReviewPaperActivity;
import com.parshvaa.teacherapp.DB.DBTables;
import com.parshvaa.teacherapp.DB.MyDBManager;
import com.parshvaa.teacherapp.Models.MainUser;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;

public class SplashActivity extends BaseActivity {

    private static int SPLASH_TIME_OUT = 3000;
    public MyDBManager mDb;
    public String title = "", bigPicture = "", created_on = "", body = "";
    private static final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        try {
            title = getIntent().getStringExtra("title");
            bigPicture = getIntent().getStringExtra("bigPicture");
            body = getIntent().getStringExtra("body");
            created_on = getIntent().getStringExtra("created_on");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "getIsLoggedIn: "+mySharedPref.getIsLoggedIn());
        if (mySharedPref.getIsLoggedIn()) {
            Gson gson = new Gson();

            String userModel = mySharedPref.getUserModel();
            MainUser user = gson.fromJson(userModel, MainUser.class);
            mainUser = user;

            if (App.isNoti) {
                if (App.isType.equalsIgnoreCase("NewTestPlannerCreated")) {
                    startActivity(new Intent(this, PaperAssignedActivity.class));
                    finish();

                } else if (App.isType.equalsIgnoreCase("NewCctPlannerCreated")) {
                    startActivity(new Intent(this, CctAssignedActivity.class));
                    finish();

                } else if (App.isType.equalsIgnoreCase("GeneratedPaperAssigned")) {
                    startActivity(new Intent(this, ReadyReviewPaperActivity.class));
                    finish();

                } else if (App.isType.equalsIgnoreCase("GeneratedCCTAssigned")) {
                    startActivity(new Intent(this, ReadyReviewCctActivity.class));
                    finish();

                } else if (App.isType.equalsIgnoreCase("zooki")) {
                    Intent intent = new Intent(this, ZookiNotificationActivity.class);
                    intent.putExtra("title", title);
                    intent.putExtra("bigPicture", bigPicture);
                    intent.putExtra("created_on", created_on);
                    intent.putExtra("body", body);
                    startActivity(intent);
                    finish();

                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(SplashActivity.this, DashboardActivity.class);
                            startActivity(i);
                            finish();
                        }


                    }, SPLASH_TIME_OUT);
                }
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(SplashActivity.this, DashboardActivity.class);
                        startActivity(i);
                        finish();
                    }


                }, SPLASH_TIME_OUT);
            }
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashActivity.this, ConcernActivity.class);
                    startActivity(i);
                    finish();
                }


            }, SPLASH_TIME_OUT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumeManually();
    }

    public void resumeManually() {
        DBTables dbTables = new DBTables();
        dbTables.createAll(mDb);
    }

}
