package com.parshvaa.teacherapp.Models;


import java.io.Serializable;

public class OptionBean implements Serializable {

    int MCQOptionID;
    int MCQQuestionID;
    String Options;
    String OptionNo;
    String IsCorrect;
    boolean isSelected;
    boolean isAttempt =false;

    public int getMCQOptionID() {
        return MCQOptionID;
    }

    public void setMCQOptionID(int MCQOptionID) {
        this.MCQOptionID = MCQOptionID;
    }

    public int getMCQQuestionID() {
        return MCQQuestionID;
    }

    public void setMCQQuestionID(int MCQQuestionID) {
        this.MCQQuestionID = MCQQuestionID;
    }

    public String getOptions() {
        return Options;
    }

    public void setOptions(String options) {
        Options = options;
    }

    public String getOptionNo() {
        return OptionNo;
    }

    public void setOptionNo(String optionNo) {
        OptionNo = optionNo;
    }

    public String getIsCorrect() {
        return IsCorrect;
    }

    public void setIsCorrect(String isCorrect) {
        IsCorrect = isCorrect;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isAttempt() {
        return isAttempt;
    }

    public void setAttempt(boolean attempt) {
        isAttempt = attempt;
    }
}
