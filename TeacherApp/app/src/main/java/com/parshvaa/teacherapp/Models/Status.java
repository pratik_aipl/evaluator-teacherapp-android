package com.parshvaa.teacherapp.Models;

import java.io.Serializable;
import java.util.ArrayList;


public class Status implements Serializable {
    public String StatusName = "";

    public String getStatusName() {
        return StatusName;
    }

    public void setStatusName(String statusName) {
        StatusName = statusName;
    }
}
