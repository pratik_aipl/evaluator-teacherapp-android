package com.parshvaa.teacherapp.Activities.Profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.CCT.CctAssignedAdapter;
import com.parshvaa.teacherapp.Activities.CCT.CctGenerateActivity;
import com.parshvaa.teacherapp.Activities.CCT.CctPaper;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssigned;
import com.parshvaa.teacherapp.Models.Subject;
import com.parshvaa.teacherapp.R;

import java.util.List;

/**
 * Created by empiere-vaibhav on 7/26/2018.
 */

public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.MyViewHolder> {

    public Context context;
    PaperAssigned subObj;
    private List<Subject> optList;

    public SubjectAdapter(List<Subject> moviesList, Context context) {
        this.optList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_subject_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_subject.setText(optList.get(position).getSubjectName());
    }

    @Override
    public int getItemCount() {
        return optList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject;

        public MyViewHolder(View view) {
            super(view);
            tv_subject = (TextView) view.findViewById(R.id.tv_subject);
        }
    }
}

