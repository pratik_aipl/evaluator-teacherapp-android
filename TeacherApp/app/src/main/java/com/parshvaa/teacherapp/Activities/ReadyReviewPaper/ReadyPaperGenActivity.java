package com.parshvaa.teacherapp.Activities.ReadyReviewPaper;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.CCT.CctAssignedActivity;
import com.parshvaa.teacherapp.Activities.Paper.PaperGenerateActivity;
import com.parshvaa.teacherapp.Activities.ReadyReviewCct.ReadyReviewGenCctActivity;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ReadyPaperGenActivity extends BaseActivity implements AsynchTaskListner {
    public String PlannerID = "";
    public ImageView img_back;
    public TextView tv_title;
    public String go_back_url = "";
    public ReadyPaperGenActivity instance;
    WebView web_ready_paper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ready_paper_gen);
        instance = ReadyPaperGenActivity.this;
        showProgress(true);
        PlannerID = getIntent().getStringExtra("PlannerID");
        web_ready_paper = (WebView) findViewById(R.id.web_ready_paper);
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);

        web_ready_paper.setWebViewClient(new myWebClient());
        Utils.setWebViewSettings(web_ready_paper);

        tv_title.setText("GENERATE PAPER");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        showProgress(true);
        new CallRequest(instance).generateReadyReviewUrl(PlannerID);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
            showProgress(false);
        if (result != null && !result.isEmpty()) {
            Log.i(result, "RESULT");
            try {
                switch (request) {
                    case generateReadyReviewUrl:
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
//                            web_ready_paper.setWebViewClient(new myWebClient());
                            if (Utils.isNetworkAvailableWebView(ReadyPaperGenActivity.this)) {
                                web_ready_paper.loadUrl(jObj.getString("generate_paper_url"));
                            } else {
                                Utils.showNetworkAlert(ReadyPaperGenActivity.this);
                            }
                            go_back_url = jObj.getString("go_back_url");
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (web_ready_paper.canGoBack()) {
            if (Utils.isNetworkAvailableWebView(ReadyPaperGenActivity.this)) {
                web_ready_paper.goBack();

            } else {
                Utils.showNetworkAlert(ReadyPaperGenActivity.this);
            }
        } else {
         //   super.onBackPressed();
            Intent intent = new Intent(instance, ReadyReviewPaperActivity.class);
            startActivity(intent);
            finish();

        }
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
//            if (Utils.isNetworkAvailableWebView(ReadyPaperGenActivity.this)) {
//                view.loadUrl(url);
//
//            } else {
//                Utils.showNetworkAlert(ReadyPaperGenActivity.this);
            //}
            return false;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            showProgress(false);
            try {
                if (url.equalsIgnoreCase(go_back_url)) {
                    Intent intent = new Intent(instance, ReadyReviewPaperActivity.class);
                    startActivity(intent);
                    finish();
                }
                if (url.equalsIgnoreCase("https://test.pemevaluater.parshvaa.com/auth/login")) {
                    Intent intent = new Intent(instance, ReadyReviewPaperActivity.class);
                    startActivity(intent);
                    finish();
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

}

