package com.parshvaa.teacherapp.Models;


import java.io.Serializable;
import java.util.List;

public class DashboardMainModel implements Serializable {

    private String title;
    private List<AssignedData> Assigned ;

    public DashboardMainModel(String title, List<AssignedData> assigned) {
        this.title = title;
        Assigned = assigned;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<AssignedData> getAssigned() {
        return Assigned;
    }

    public void setAssigned(List<AssignedData> assigned) {
        Assigned = assigned;
    }
}
