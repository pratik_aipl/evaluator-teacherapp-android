package com.parshvaa.teacherapp.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class QueryCallUsActivity extends BaseActivity implements AsynchTaskListner{
    public ImageView img_callNow;
    public Button btn_send;
    public EditText et_message;
    public QueryCallUsActivity instance;
    public App app;
    public ImageView img_back;
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query_call_us);

        instance = this;
        app = App.getInstance();
        img_callNow = (ImageView) findViewById(R.id.img_callNow);
        btn_send = (Button) findViewById(R.id.btn_send);
        et_message = (EditText) findViewById(R.id.et_message);
        img_callNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = "+917710012112";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }
        });

        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("Query");

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_message.getText().toString().equals("")) {
                    Utils.showToast("Please fill the message", instance);
                } else {
                    showProgress(true);
                      new CallRequest(instance).querySendMail(mainUser.getEmail(), et_message.getText().toString());
                }

            }
        });
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
            showProgress(false);
        if (result != null && !result.isEmpty()) {
            Utils.Log(result, "RESULT");
            try {
                switch (request) {

                    case qurySendMail:
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            et_message.setText("");
                            Utils.showAlert("Thank you for contacting us. We will shortly get back to you.", instance);
                        } else {
                            Utils.showAlert(jObj.getString("message"), instance);

                        }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
