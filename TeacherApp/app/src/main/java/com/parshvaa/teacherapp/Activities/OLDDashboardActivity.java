package com.parshvaa.teacherapp.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Activities.CCT.CctAssignedActivity;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssignedActivity;
import com.parshvaa.teacherapp.Activities.Profile.MyProfileActivity;
import com.parshvaa.teacherapp.Activities.ReadyReviewCct.ReadyReviewCctActivity;
import com.parshvaa.teacherapp.Activities.ReadyReviewPaper.ReadyReviewPaperActivity;
import com.parshvaa.teacherapp.Activities.Reports.CctReportListActivity;
import com.parshvaa.teacherapp.Activities.SendApproval.SendApprovalActivity;
import com.parshvaa.teacherapp.Adapter.ZookiAdapter;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.DB.MyDBManager;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.Zooki;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.FragmentDrawer;
import com.parshvaa.teacherapp.Utils.ItemOffsetDecoration;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.NetworkUtil;
import com.parshvaa.teacherapp.Utils.Utils;
import com.squareup.picasso.Picasso;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class OLDDashboardActivity extends BaseActivity implements FragmentDrawer.FragmentDrawerListener, AsynchTaskListner {

    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
    public static MyDBManager mDb;
    public static LinkedHashMap<String, String> mapImages = new LinkedHashMap<>();
    public static ThinDownloadManager downloadManager;
    public DashboardActivity instance;
    public RelativeLayout rel_gen_paper, rel_gen_cct, rel_report, rel_approval, rel_review_paper, rel_review_cct;
    public LinearLayout mainLay;
    public ImageView drawerButton, img_noti, img_class_logo, img_profile;
    public ArrayList<Zooki> zookiList = new ArrayList<>();
    public Zooki zooki;
    public JsonParserUniversal jParser;
    public ProgressDialog pDialog;
    public RetryPolicy retryPolicy;
    public MyDownloadDownloadStatusListenerV1 myDownloadStatusListener = new MyDownloadDownloadStatusListenerV1();
    public RecyclerView.LayoutManager mLayoutManager1;
    public RecyclerView rvZooki;
    public boolean zookiIsFirstTie = false;
    public ZookiAdapter zookiAdapter;
    public String assign_paper = "", assign_cct = "", review_ready = "", send_approval = "", ready_cct = "";
    private FragmentDrawer drawerFragment;
    private TextView tv_visit_site, tv_send_approval, tv_ready_review, tv_assign_cct, tv_assign_paper, tv_teacher_name, tv_username, nav_userName, tv_ready_cct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_dashboard);

        jParser = new JsonParserUniversal();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        retryPolicy = new DefaultRetryPolicy();
        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);

        mainLay = findViewById(R.id.mainLay);
        rel_gen_paper = findViewById(R.id.rel_gen_paper);
        rel_gen_cct = findViewById(R.id.rel_gen_cct);
        rel_report = findViewById(R.id.rel_report);
        rel_approval = findViewById(R.id.rel_approval);
        rel_review_paper = findViewById(R.id.rel_review_paper);
        rel_review_cct = findViewById(R.id.rel_review_cct);
        rvZooki = findViewById(R.id.rvRelated);
        tv_visit_site = findViewById(R.id.tv_visit_site);
        tv_send_approval = findViewById(R.id.tv_send_approval);
        tv_ready_review = findViewById(R.id.tv_ready_review);
        tv_assign_cct = findViewById(R.id.tv_assign_cct);
        tv_assign_paper = findViewById(R.id.tv_assign_paper);
        tv_teacher_name = findViewById(R.id.tv_teacher_name);
        tv_ready_cct = findViewById(R.id.tv_ready_cct);
        nav_userName = findViewById(R.id.nav_userName);
        img_profile = findViewById(R.id.img_profile);
        tv_username = findViewById(R.id.tv_username);
        img_noti = findViewById(R.id.img_noti);
        img_class_logo = findViewById(R.id.img_class_logo);
        drawerFragment = (FragmentDrawer) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerButton = findViewById(R.id.drawerButton);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), drawerButton, mainLay);
        drawerFragment.setDrawerListener(this);


        rel_gen_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, PaperAssignedActivity.class));
            }
        });
        rel_gen_cct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, CctAssignedActivity.class));
            }
        });
        rel_review_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReadyReviewPaperActivity.class));
            }
        });
        rel_review_cct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReadyReviewCctActivity.class));
            }
        });
        rel_approval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, SendApprovalActivity.class));
            }
        });
        img_noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, NotificationActivity.class));
            }
        });
        rel_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  startActivity(new Intent(instance, ReportActivity.class));
                startActivity(new Intent(instance, CctReportListActivity.class));

            }
        });
        try {
            tv_teacher_name.setText(mainUser.getFirst_name() + " " + mainUser.getLast_name());
            nav_userName.setText(mainUser.getFirst_name() + " " + mainUser.getLast_name());
            tv_username.setText(mainUser.getUsername());
            Picasso.with(instance)
                    .load(mainUser.getClassLogo())
                    .error(R.drawable.profile) //this is optional the image to display while the url image is downloading
                    .into(img_class_logo);
            Picasso.with(instance)
                    .load(mainUser.getClassRoundLogo())
                    .error(R.drawable.profile) //this is optional the image to display while the url image is downloading
                    .into(img_profile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);

    }

    public void displayView(int position) {

        switch (position) {
            case 0:
                break;
            case 1:
                startActivity(new Intent(instance, MyProfileActivity.class));
                break;
            case 2:
                startActivity(new Intent(instance, QueryCallUsActivity.class));
                break;

            case 3:
                ReferFriend();
                break;

            case 4:
                launchMarket();
                break;

          /*  case 5:
                startActivity(new Intent(instance, LegalActivity.class));
                break;
*/
            default:
                break;
        }

    }

    public void ReferFriend() {
        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.parshvaa.iscore&hl=en");
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "iSocre");
        i.putExtra(Intent.EXTRA_TEXT, String.valueOf(uri));
        startActivity(Intent.createChooser(i, "Share via"));
    }

    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Utils.Log("TAG url ::--> ", String.valueOf(uri));
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerFragment.mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerFragment.mDrawerLayout.closeDrawer(Gravity.LEFT);
        }

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setMessage("Are you sure you want to exit the Teacher APP")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", null)
                .show();

    }

    public void manageImagesDownload(LinkedHashMap<String, String> map) {

        Set<String> keys = map.keySet();
        int i = 0;
        for (String url : keys) {
            final DownloadRequest downloadRequest1 = new DownloadRequest(Uri.parse(url.replace(" ", "%20")))
                    .setDestinationURI(Uri.parse(map.get(url))).setPriority(DownloadRequest.Priority.HIGH)
                    .setRetryPolicy(retryPolicy)
                    .setDownloadContext(url)
                    .setDeleteDestinationFileOnFailure(false)
                    .setStatusListener(myDownloadStatusListener);
            downloadManager.add(downloadRequest1);
        }
        RecyclerViewZooki();

    }

    public void SyncData() {
        if (NetworkUtil.getConnectivityStatus(instance) > 0) {
            System.out.println("Connect");
            Map<String, String> map = new HashMap<String, String>();
            map.put("url", Constant.URL + "dashboard");
        /*    map.put("header", "");
            map.put("token", mySharedPref.getLoginToken());
            map.put("user_id", mySharedPref.getUserId());
*/
            new CallRequest(OLDDashboardActivity.this).getDashboardCount(map);

        }
    }

    @Override
    protected void onResume() {

        SyncData();
        super.onResume();
    }

    public void RecyclerViewZooki() {

        mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvZooki.setLayoutManager(mLayoutManager1);
        rvZooki.setHasFixedSize(true);
        if (!zookiIsFirstTie) {
            zookiIsFirstTie = true;
            final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
            rvZooki.addItemDecoration(new ItemOffsetDecoration(spacing));
        }
        RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                rvZooki.removeOnScrollListener(this);

                rvZooki.addOnScrollListener(this);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        rvZooki.addOnScrollListener(scrollListener);
        zookiAdapter = new ZookiAdapter(instance, zookiList);
        rvZooki.setAdapter(zookiAdapter);
        tv_visit_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ZookiWebActivity.class));
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case getDashboardCount:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            zookiList.clear();
                            String folderPath = jObj.getString("folder_path");
                            assign_paper = jObj.getString("assigned_paper_total");
                            assign_cct = jObj.getString("assigned_cct_paper_total");
                            send_approval = String.valueOf(jObj.getInt("send_for_approval_total"));
                            ready_cct = jObj.getString("cct_review_total");
                            review_ready = jObj.getString("paper_review_total");

                            tv_ready_review.setText(review_ready);
                            tv_assign_cct.setText(assign_cct);
                            tv_assign_paper.setText(assign_paper);
                            tv_ready_cct.setText(ready_cct);
                            tv_send_approval.setText(send_approval);
                            JSONArray jData = jObj.getJSONArray("zooki");
                            if (jData.length() > 0 || jData != null) {
                                String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/" + "Zooki";
                                File f = new File(SDCardPath);
                                if (!f.exists()) {
                                    f.mkdirs();
                                }
                                for (int i = 0; i < jData.length(); i++) {
                                    zooki = (Zooki) jParser.parseJson(jData.getJSONObject(i), new Zooki());
                                    mDb.insertRow(zooki, "zooki");
                                    zookiList.add(zooki);

                                    String url = folderPath + zooki.getImage();
                                    String localUrl = SDCardPath + "/" + zooki.getImage();
                                    Log.i("TAG", " IMAGE PATH ::::>" + localUrl);
                                    File lf = new File(localUrl);
                                    if (!lf.exists()) {
                                        mapImages.put(url, localUrl);
                                    }
                                }
                                Zooki obj = new Zooki();
                                obj.setTitle("View More");
                                zookiList.add(obj);
                                manageImagesDownload(mapImages);
                            }
                        }

                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class MyDownloadDownloadStatusListenerV1 implements DownloadStatusListenerV1 {
        @Override
        public void onDownloadComplete(DownloadRequest request) {
            Log.i("TAG", "Download Complete  ZOOKI:: ");
        }

        @Override
        public void onDownloadFailed(DownloadRequest request, int errorCode, String errorMessage) {

            Log.i("TAG", "Download Failed  ZOOKI:: ");
        }

        @Override
        public void onProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
            int id = request.getDownloadId();


        }
    }

}
