package com.parshvaa.teacherapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.parshvaa.teacherapp.Adapter.ReportQuestionsAdapter;
import com.parshvaa.teacherapp.Models.LevelQuestion;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.Utils.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportQuestionsActivity extends BaseActivity {

    private static final String TAG = "ReportStudentActivity";
    @BindView(R.id.img_back)
    ImageView mBackBtn;
    @BindView(R.id.tv_title)
    TextView mPageTitle;

   /* @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;*/
    @BindView(R.id.mQuestionsList)
    RecyclerView mQuestionsList;

    List<LevelQuestion> levelQuestion=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_questions);
        ButterKnife.bind(this);

        mPageTitle.setText("Questions");

        levelQuestion= (List<LevelQuestion>) getIntent().getSerializableExtra(Constant.questionsList);
        mQuestionsList.setAdapter(new ReportQuestionsAdapter(this,levelQuestion, Constant.questionsList));
        Log.d(TAG, "questionsize: "+levelQuestion.size());

    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
