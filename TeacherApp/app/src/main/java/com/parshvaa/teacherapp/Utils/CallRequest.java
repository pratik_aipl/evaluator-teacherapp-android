package com.parshvaa.teacherapp.Utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Models.MainUser;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 10/11/2016.
 */
public class CallRequest {

   //  public static String URL = "https://test.pemevaluater.parshvaa.com/web_services/version_1/web_services/";
    //public static String URL = "https://evaluater.parshvaa.com/web_services/version_1/web_services/";
    public static String URL = "https://evaluater.parshvaa.com/web_services/version_2/Web_services/";
    public App app;
    public Context ct;
    public Fragment ft;
    public MySharedPref mySharedPref;
    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
        mySharedPref = new MySharedPref(ft.getActivity());

    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
        mySharedPref = new MySharedPref(ct);

    }

    public void getLogin(String login, String password, String device_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "login");
        map.put("login", login);
        map.put("password", password);
        map.put("device_id", device_id);

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getLogin, Constant.POST_TYPE.POST, map);
    }

    public void editProfile(String first_name, String last_name, String register_mobile) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "edit_profile");
        map.put("first_name", first_name);
        map.put("last_name", last_name);
        map.put("register_mobile", register_mobile);

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.editProfile, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void forgotPassword(String email) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "forgot_password");
        map.put("email", email);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.forgotPassword, Constant.POST_TYPE.POST, map);
    }

    public void resendOtp() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "resend_otp");
        map.put("user_id", App.user_id.toString());

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.resend_otp, Constant.POST_TYPE.POST, map);
    }

    public void confirmOtp(String otp, String player_id, String device_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "confirm_otp");
        map.put("user_id", App.user_id.toString());
        map.put("otp", otp);
        map.put("player_id", player_id);
        map.put("device_id", device_id);

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.confirmOtp, Constant.POST_TYPE.POST, map);
    }

    public void querySendMail(String from, String QueryMessage) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "query_message");
        map.put("email", from);
        map.put("query_message", QueryMessage);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.qurySendMail, Constant.POST_TYPE.POST_WITH_HEADER, map);

    }

    public void getDashboardCount(Map<String, String> map) {
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getDashboardCount, Constant.POST_TYPE.GET, map);

    }

    public void getNotifition() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.URL + "get_notification_list");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getNotifition, Constant.POST_TYPE.GET, map);

    }
    public void getStatus() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.URL + "planner_status");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getstatus, Constant.POST_TYPE.GET, map);

    }

    public void getUserDetails() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.URL + "get_user_details");
        map.put("class_id", mySharedPref.getClassId());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getUserDetails, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void assignedPaper(String standard_id, String subject_id, String assign_date, String paper_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "assigned_paper");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        map.put("paper_date", Utils.changeDateToYYYYMMDD(paper_date));
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.assignedPaper, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void generatePaperUrl(String planner_id, String paper_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "generate_paper_urls");
        map.put("planner_id", planner_id);
        map.put("paper_id", paper_id);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.generatePaperUrl, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void getReports(String paper_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "cct_report_details");
        //  map.put("planner_id", planner_id);
        map.put("PaperID", paper_id);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getReports, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }
    public void getReportsTest(String paper_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "teacher_test_paper_report");
        map.put("PaperID", paper_id);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getReportsTest, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void getReposrtsDetails(String paper_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "cct_report_student_details");
        //  map.put("planner_id", planner_id);
        map.put("PaperID", paper_id);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getReportsDetails, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void assignedCctPaper(String standard_id, String subject_id, String assign_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "assigned_cct_paper");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.assignedCctPaper, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void cctAssginUrl() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.URL + "cct_paper_urls");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getcctPaperUrl, Constant.POST_TYPE.GET, map);

    }
    public void fregcctAssginUrl() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.URL + "cct_paper_urls");
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.getcctPaperUrl, Constant.POST_TYPE.GET, map);

    }

    public void teacherCctReport() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.URL + "teacher_cct_report");
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.teacherCctReport, Constant.POST_TYPE.GET, map);
    }

    public void GetlessonPlannerList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.URL + "lesson_planner_list");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.GetlessonPlannerList, Constant.POST_TYPE.GET, map);
    }

    public void teacherTestPaper() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.URL + "teacher_test_paper");
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.teacherTestReport, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }


    public void generateCctPaperUrl(String planner_id, String paper_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "generate_cct_paper_urls");
        map.put("planner_id", planner_id);
        map.put("paper_id", paper_id);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.generateCctPaperUrl, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }


    public void generatedCctUrls(String paper_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "generated_cct_urls");
        map.put("PaperID", paper_id);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.generateCctPaperUrl, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void readyReviewPaper(String standard_id, String subject_id, String assign_date, String paper_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "ready_for_review");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        map.put("paper_date", Utils.changeDateToYYYYMMDD(paper_date));
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.readyReviewPaper, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void generateReadyReviewUrl(String planner_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "ready_for_review_urls");
        map.put("paper_id", planner_id);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.generateReadyReviewUrl, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void readyReviewCct(String standard_id, String subject_id, String assign_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "cct_ready_for_review");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.readyReviewCct, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }
    public void getChapters(String ClassLessonPlannerID){
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "planner_detail");
        map.put("ClassLessonPlannerID", ClassLessonPlannerID);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getChapters, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }
    public void updatePlanerRemark(String Status,String remark,String ClassLessonPlannerID){
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "update_planner_remark");
        map.put("Status", Status);
        map.put("Remark", remark);
        map.put("ClassLessonPlannerID", ClassLessonPlannerID);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getplanerremark, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void generateCctReadyReview(String planner_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "cct_ready_for_review_urls");
        map.put("paper_id", planner_id);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.generateCctReadyReview, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void get_standard() {
        Log.i("Class Id", "==>" + mySharedPref.getClassId());
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_standard");
        map.put("class_id", mySharedPref.getClassId());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_standard, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void get_subject(String standard_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_subject");
        map.put("standard_id", standard_id);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_subject, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void readyForApproval(String standard_id, String subject_id, String assign_date, String paper_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "ready_for_approval");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        map.put("paper_date", Utils.changeDateToYYYYMMDD(paper_date));
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.readyForApproval, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void readyForApprovalActivity(String standard_id, String subject_id, String assign_date, String paper_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "ready_for_approval");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        map.put("paper_date", Utils.changeDateToYYYYMMDD(paper_date));
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.readyForApproval, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void cctReadyForApproval(String standard_id, String subject_id, String assign_date) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "cct_ready_for_approval");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.cctReadyForApproval, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }
    public void GetREVIEWAPPROVEDCCT(String standard_id, String subject_id, String assign_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "cct_approved");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.GetREVIEWAPPROVEDCCT, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void GetREVIEWAPPROVEDPAPER(String standard_id, String subject_id, String assign_date, String paper_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "approved_paper");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        map.put("paper_date", Utils.changeDateToYYYYMMDD(paper_date));
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.GetREVIEWAPPROVEDPAPER, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void genratedCctPaper(String standard_id, String subject_id, String assign_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "genrated_cct_paper");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.genratedCctPaper, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void assignedCctPaperft(String standard_id, String subject_id, String assign_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "assigned_cct_paper");
        map.put("standard_id", standard_id);
        map.put("subject_id", subject_id);
        map.put("assign_date", Utils.changeDateToYYYYMMDD(assign_date));
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.assignedCctPaper, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void getDates(boolean isShow, String date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "paper_dates");
        map.put("Month", date);
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.getDate, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }
    public void getDataByDate(boolean isShow, String date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "dashboard");
        map.put("Date", date);
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.getDataByDate, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }
    public void getReport(String ClassMCQTestHDRID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "cct_report");
        map.put("ClassMCQTestHDRID", ClassMCQTestHDRID);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getReport, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }
    public void getstudentwisecctreport(String ClassMCQTestHDRID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "student_wise_cct_report");
        map.put("ClassMCQTestHDRID", ClassMCQTestHDRID);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getstudentwisecctreport, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }
    public void getstudentSummary(String PlannerID,int StudentID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "student_wise_cct_report");
        map.put(Constant.PlannerID, PlannerID);
        map.put(Constant.StudentID, String.valueOf(StudentID));
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getstudentSummary, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }
}
