package com.parshvaa.teacherapp.Models;

import java.io.Serializable;

public class AssignedData implements Serializable {

    public String ClassLessonPlannerID ;
    public String ClassID ;
    public String ClassName ;
    public String TeacherID ;
    public String SubjectID ;
    public String BranchID ;
    public String BatchID ;
    public String Duration ;
    public String Status ;
    public String PaperDate ;
    public String CreatedBy ;
    public String CreatedOn ;
    public String ModifiedBy ;
    public String ModifiedOn ;
    public String SubjectName ;
    public String BoardName ;
    public String MediumName ;
    public String StandardName ;
    public String ClassTestPlannerID ;
    public String ClassCCTPlannerID ;
    public String ClassMCQTestHDRID ;
    public String AssignDate ;
    public String PaperMark ;
    public String StandardID ;
    public String  AssignType;
    public String  first_name;
    public String  last_name;
    public String  PaperStatus;
    public String ClassQuestionPaperID ;
    public String  Type;
    public String  SubmissionDate;
    public String  Time;
    public String  AssignLactureLink;
    public String  LectureStatus;
    public String  HomeWork;

    public String getLectureStatus() {
        return LectureStatus;
    }

    public void setLectureStatus(String lectureStatus) {
        LectureStatus = lectureStatus;
    }

    public String getHomeWork() {
        return HomeWork;
    }

    public void setHomeWork(String homeWork) {
        HomeWork = homeWork;
    }

    public String getAssignLactureLink() {
        return AssignLactureLink;
    }

    public void setAssignLactureLink(String assignLactureLink) {
        AssignLactureLink = assignLactureLink;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getSubmissionDate() {
        return SubmissionDate;
    }

    public void setSubmissionDate(String submissionDate) {
        SubmissionDate = submissionDate;
    }

    public String getClassLessonPlannerID() {
        return ClassLessonPlannerID;
    }

    public void setClassLessonPlannerID(String classLessonPlannerID) {
        ClassLessonPlannerID = classLessonPlannerID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getTeacherID() {
        return TeacherID;
    }

    public void setTeacherID(String teacherID) {
        TeacherID = teacherID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getBranchID() {
        return BranchID;
    }

    public void setBranchID(String branchID) {
        BranchID = branchID;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String batchID) {
        BatchID = batchID;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getPaperDate() {
        return PaperDate;
    }

    public void setPaperDate(String paperDate) {
        PaperDate = paperDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getClassTestPlannerID() {
        return ClassTestPlannerID;
    }

    public void setClassTestPlannerID(String classTestPlannerID) {
        ClassTestPlannerID = classTestPlannerID;
    }

    public String getClassCCTPlannerID() {
        return ClassCCTPlannerID;
    }

    public void setClassCCTPlannerID(String classCCTPlannerID) {
        ClassCCTPlannerID = classCCTPlannerID;
    }

    public String getClassMCQTestHDRID() {
        return ClassMCQTestHDRID;
    }

    public void setClassMCQTestHDRID(String classMCQTestHDRID) {
        ClassMCQTestHDRID = classMCQTestHDRID;
    }

    public String getAssignDate() {
        return AssignDate;
    }

    public void setAssignDate(String assignDate) {
        AssignDate = assignDate;
    }

    public String getPaperMark() {
        return PaperMark;
    }

    public void setPaperMark(String paperMark) {
        PaperMark = paperMark;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getAssignType() {
        return AssignType;
    }

    public void setAssignType(String assignType) {
        AssignType = assignType;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPaperStatus() {
        return PaperStatus;
    }

    public void setPaperStatus(String paperStatus) {
        PaperStatus = paperStatus;
    }

    public String getClassQuestionPaperID() {
        return ClassQuestionPaperID;
    }

    public void setClassQuestionPaperID(String classQuestionPaperID) {
        ClassQuestionPaperID = classQuestionPaperID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
