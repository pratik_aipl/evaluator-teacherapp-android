package com.parshvaa.teacherapp.Models;

import java.io.Serializable;
import java.util.ArrayList;


public class UserDetail implements Serializable {
    public String id = "";
    public String first_name = "";
    public String last_name = "";

    public String getClassLogo() {
        return ClassLogo;
    }

    public void setClassLogo(String classLogo) {
        ClassLogo = classLogo;
    }

    public String getClassRoundLogo() {
        return ClassRoundLogo;
    }

    public void setClassRoundLogo(String classRoundLogo) {
        ClassRoundLogo = classRoundLogo;
    }

    public String ClassLogo="";
    public String ClassRoundLogo="";
    public ArrayList<Standard> standardArray=new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegister_mobile() {
        return register_mobile;
    }

    public void setRegister_mobile(String register_mobile) {
        this.register_mobile = register_mobile;
    }

    public String username = "";
    public String email = "";
    public String register_mobile = "";
}
