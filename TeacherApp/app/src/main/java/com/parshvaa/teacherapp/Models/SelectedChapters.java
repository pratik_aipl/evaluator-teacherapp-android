package com.parshvaa.teacherapp.Models;

import java.io.Serializable;


public class SelectedChapters implements Serializable {
    public String ChapterID = "";
    public String ChapterName = "";

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getChapterName() {
        return ChapterName;
    }

    public void setChapterName(String chapterName) {
        ChapterName = chapterName;
    }
}
