package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.Models.DashboardMainModel;
import com.parshvaa.teacherapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashBoardDetailsAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "OrderDetailsAdpater";
    Context context;
    List<DashboardMainModel> dataList;
    DashboardRowDetailsAdpater dashboardRowDetailsAdpater;

    public DashBoardDetailsAdpater(Context context, List<DashboardMainModel> dataLists) {
        this.context = context;
        this.dataList = dataLists;
        Log.d(TAG, "DashBoardDetailsAdpater: "+dataLists.size());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.searchdatalist, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        DashboardMainModel rawModel = dataList.get(position);

        holder.RecyclerList.setHasFixedSize(true);
        holder.RecyclerList.setItemAnimator(new DefaultItemAnimator());
        holder.RecyclerList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        holder.tvTittle.setText(rawModel.getTitle().toUpperCase());
        if(rawModel.getTitle().equalsIgnoreCase("TEST ASSIGNED")){
            holder.img_tittle_icone.setImageResource(R.drawable.pen);
        }else if(rawModel.getTitle().equalsIgnoreCase("LESSON ASSIGNED")){
            holder.img_tittle_icone.setImageResource(R.drawable.book);
        }
        Log.d(TAG, "DashBoardDetailsAdpater>>> "+rawModel.getAssigned().size());
        if (rawModel.getAssigned().size() != 0) {
            dashboardRowDetailsAdpater = new DashboardRowDetailsAdpater(context, rawModel.getAssigned(),rawModel.getTitle());
            holder.RecyclerList.setAdapter(dashboardRowDetailsAdpater);
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    static
    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_tittle_icone)
        ImageView img_tittle_icone;
        @BindView(R.id.tv_tittle)
        TextView tvTittle;
        @BindView(R.id.mrawDataList)
        RecyclerView RecyclerList;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}