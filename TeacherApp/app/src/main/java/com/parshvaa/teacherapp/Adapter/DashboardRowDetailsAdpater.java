package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Activities.CCT.CctGenerateActivity;
import com.parshvaa.teacherapp.Activities.Paper.PaperGenerateActivity;
import com.parshvaa.teacherapp.LessonAssignedDetailsActivity;
import com.parshvaa.teacherapp.MainReportsActivity;
import com.parshvaa.teacherapp.Models.AssignedData;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.RobotoBoldTextview;
import com.parshvaa.teacherapp.Utils.RobotoTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardRowDetailsAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "OrderDetailsAdpater";
    Context context;
    List<AssignedData> dataList;
    String tittle;


    public DashboardRowDetailsAdpater(Context context, List<AssignedData> dataLists, String title) {
        this.context = context;
        this.dataList = dataLists;
        this.tittle = title;
        Log.d(TAG, "DashboardRowDetailsAdpater: "+dataLists.size());
        Log.d(TAG, "ttttt: "+tittle);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_raw_data, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        AssignedData rawModel = dataList.get(position);


        Log.d(TAG, "onBindViewHolder: "+tittle);
        Log.d(TAG, "onBindViewHolderdataaa: "+rawModel.getBoardName());
        if (tittle.equalsIgnoreCase("TEST ASSIGNED")) {
                holder.lin_submission.setVisibility(View.VISIBLE);
                holder.lin_hours.setVisibility(View.GONE);
                holder.lin_time.setVisibility(View.GONE);

                holder.tvBoard.setText(rawModel.getBoardName());
                holder.tvMedium.setText(rawModel.getMediumName());
                holder.tvStdanderd.setText(rawModel.getStandardName());
                holder.tvSubject.setText(rawModel.getSubjectName());
                holder.tvSubmissionDt.setText(rawModel.getSubmissionDate());


            if(rawModel.getType().equalsIgnoreCase("cct")){
                holder.tv_type_tittle.setVisibility(View.VISIBLE);
                holder.imgMain.setImageResource(R.drawable.gnrt);
                holder.tv_type_tittle.setText("CCT");
            }else if(rawModel.getType().equalsIgnoreCase("planner")){
                holder.tv_type_tittle.setVisibility(View.VISIBLE);
                holder.imgMain.setImageResource(R.drawable.icon_gen_cct);
                holder.tv_type_tittle.setText("GP");
            }else{
                holder.tv_type_tittle.setVisibility(View.GONE);
            }
            if(rawModel.getStatus().equals("generated")){
                holder.img_right.setVisibility(View.VISIBLE);
            }else{
                holder.img_right.setVisibility(View.GONE);
            }

        } else if (tittle.equalsIgnoreCase("LESSON ASSIGNED")) {
            Log.d(TAG, "onBindViewHolderdataaa: "+rawModel.getBoardName());
            holder.lin_submission.setVisibility(View.GONE);
            holder.lin_hours.setVisibility(View.VISIBLE);
            holder.lin_time.setVisibility(View.VISIBLE);
            holder.imgMain.setImageResource(R.drawable.book);

            holder.tvBoard.setText(rawModel.getBoardName());
            holder.tvMedium.setText(rawModel.getMediumName());
            holder.tvStdanderd.setText(rawModel.getStandardName());
            holder.tvSubject.setText(rawModel.getSubjectName());
            holder.tv_time.setText(rawModel.getTime());


            int totalMinutesInt = Integer.parseInt(rawModel.getDuration());
            int hours = totalMinutesInt / 60;
            int hoursToDisplay = hours;
            if (hours > 12) {
                hoursToDisplay = hoursToDisplay - 12;
            }
            int minutesToDisplay = totalMinutesInt - (hours * 60);
            String minToDisplay = null;
            if(minutesToDisplay == 0 ) minToDisplay = "00";
            else if( minutesToDisplay < 10 ) minToDisplay = "00" + minutesToDisplay ;
            else minToDisplay = "" + minutesToDisplay ;
            String displayValue = hoursToDisplay + ":" + minToDisplay;

            holder.tv_hours.setText(displayValue);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tittle.equalsIgnoreCase("LESSON ASSIGNED")){

                    Intent intent;
                    intent = new Intent(context, LessonAssignedDetailsActivity.class);
                    intent.putExtra(Constant.fromAssign, true);
                    intent.putExtra("DATALIST", rawModel);
                    context.startActivity(intent);
                    /*if(!TextUtils.isEmpty(rawModel.getAssignLactureLink())){

                        Intent httpIntent = new Intent(Intent.ACTION_VIEW);
                        httpIntent.setData(Uri.parse(""+rawModel.getAssignLactureLink()));
                        context.startActivity(httpIntent);

                    }else{
                        Toast.makeText(context, "Lecture was classroom, not available online", Toast.LENGTH_SHORT).show();
                    }*/

                }else if (tittle.equalsIgnoreCase("TEST ASSIGNED")){

                    if(rawModel.getType().equals("planner")){
                        if (rawModel.getStatus().equals("generated")){
                        /*    context.startActivity(new Intent(context, MainReportsActivity.class)
                                    .putExtra("CLASSMCQTESTHDRID", rawModel.getClassMCQTestHDRID()));*/
                        }else{
                            Intent intent;
                            intent = new Intent(context, PaperGenerateActivity.class);
                            intent.putExtra("PlannerID", rawModel.getClassTestPlannerID());
                            intent.putExtra("PaperID", rawModel.getClassQuestionPaperID());
                            context.startActivity(intent);
                        }
                    }else {
                        if (rawModel.getStatus().equals("generated")) {
                            context.startActivity(new Intent(context, MainReportsActivity.class)
                                    .putExtra("CLASSMCQTESTHDRID", rawModel.getClassMCQTestHDRID()));
                        }else {
                            Intent intent;
                            intent = new Intent(context, CctGenerateActivity.class);
                            intent.putExtra(Constant.fromAssign, true);
                            intent.putExtra("PlannerID", rawModel.getClassCCTPlannerID());
                            intent.putExtra("PaperId", rawModel.getClassMCQTestHDRID());
                            context.startActivity(intent);
                        }

                    }


                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_main)
        ImageView imgMain;
        @BindView(R.id.tv_type_tittle)
        TextView tv_type_tittle;
        @BindView(R.id.img_right)
        ImageView img_right;
        @BindView(R.id.tv_board)
        RobotoBoldTextview tvBoard;
        @BindView(R.id.tv_medium)
        RobotoTextView tvMedium;
        @BindView(R.id.tv_stdanderd)
        RobotoBoldTextview tvStdanderd;
        @BindView(R.id.tv_subject)
        RobotoTextView tvSubject;
        @BindView(R.id.tv_submission_dt)
        RobotoBoldTextview tvSubmissionDt;
        @BindView(R.id.tv_time)
        RobotoBoldTextview tv_time;
        @BindView(R.id.tv_hours)
        RobotoBoldTextview tv_hours;
        @BindView(R.id.lin_submission)
        LinearLayout lin_submission;
        @BindView(R.id.lin_time)
        LinearLayout lin_time;
        @BindView(R.id.lin_hours)
        LinearLayout lin_hours;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}