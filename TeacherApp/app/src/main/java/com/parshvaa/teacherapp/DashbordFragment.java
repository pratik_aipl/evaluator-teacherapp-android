package com.parshvaa.teacherapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.Adapter.DashBoardDetailsAdpater;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.AssignedData;
import com.parshvaa.teacherapp.Models.DashboardMainModel;
import com.parshvaa.teacherapp.ObservScroll.BaseFragment;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.calendar.Utils;
import com.parshvaa.teacherapp.calendar.component.CalendarAttr;
import com.parshvaa.teacherapp.calendar.component.CalendarViewAdapter;
import com.parshvaa.teacherapp.calendar.interf.OnSelectDateListener;
import com.parshvaa.teacherapp.calendar.model.CalendarDate;
import com.parshvaa.teacherapp.calendar.model.PaperDates;
import com.parshvaa.teacherapp.calendar.view.Calendar;
import com.parshvaa.teacherapp.calendar.view.CustomDayView;
import com.parshvaa.teacherapp.calendar.view.MonthPager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DashbordFragment extends BaseFragment implements AsynchTaskListner {

    private static final String TAG = "DashBoardFragment";
    @BindView(R.id.mPrev)
    ImageView mPrev;
    @BindView(R.id.mMonthName)
    TextView mMonthName;
    @BindView(R.id.tv_notfound)
    TextView tv_notfound;
    @BindView(R.id.mNext)
    ImageView mNext;
    @BindView(R.id.calendar_view)
    MonthPager monthPager;
    @BindView(R.id.mDataList)
    RecyclerView mDataList;
    @BindView(R.id.content)
    CoordinatorLayout content;

    private int selectedMonth = 0;
    private RecyclerView.LayoutManager layoutManager;
    public JsonParserUniversal jParser;

    List<PaperDates> dates = new ArrayList<>();

    private OnSelectDateListener onSelectDateListener;
    private ArrayList<Calendar> currentCalendars = new ArrayList<>();
    private CalendarViewAdapter calendarAdapter;
    private CalendarDate currentDate;

    List<DashboardMainModel> dashboardmainmodel = new ArrayList<>();
    DashBoardDetailsAdpater dashBoardDetailsAdpater;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashbord, container, false);
        ButterKnife.bind(this, view);
        jParser = new JsonParserUniversal();

        layoutManager = new LinearLayoutManager(getActivity());
        mDataList.setLayoutManager(layoutManager);
        mDataList.setItemAnimator(new DefaultItemAnimator());

        monthPager.setViewHeight(Utils.dpi2px(getActivity(), 270));


        initCurrentDate();
        initCalendarView();
        initToolbarClickListener();
        Utils.scrollTo(content, mDataList, monthPager.getCellHeight(), 200);
        calendarAdapter.switchToWeek(monthPager.getRowIndex());
        monthPager.setCurrentItem(monthPager.getCurrentPosition() - 1);
        return view;
    }

    private void initCurrentDate() {
        currentDate = new CalendarDate();
        selectedMonth = currentDate.getMonth();
        mMonthName.setText(currentDate.getMonthName() + " " + currentDate.getYear());
        refreshClickDate(currentDate);
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.set(currentDate.getYear(), currentDate.getMonth() - 1, currentDate.getDay());
        showProgress(true);
        new CallRequest(DashbordFragment.this).getDates(false, new SimpleDateFormat("MM/yyyy").format(cal.getTime()));


    }

    private void initListener() {
        onSelectDateListener = new OnSelectDateListener() {
            @Override
            public void onSelectDate(CalendarDate date) {
                refreshClickDate(date);
            }

            @Override
            public void onSelectOtherMonth(int offset) {
                //Offset -1 means refreshing to last month's data, 1 means refreshing to next month's data
                Log.d(TAG, "onSelectOtherMonth: 00 " + offset);
                Log.d(TAG, "onSelectOtherMonth: 11 " + monthPager.getRowIndex());
                monthPager.selectOtherMonth(offset);
            }
        };
    }

    private void initCalendarView() {
        initListener();
        CustomDayView customDayView = new CustomDayView(getActivity(), R.layout.custom_day);
        calendarAdapter = new CalendarViewAdapter(getActivity(),
                onSelectDateListener,
                CalendarAttr.WeekArrayType.Sunday,
                customDayView);
        initMonthPager();
    }

    private void initToolbarClickListener() {
        mPrev.setOnClickListener((View view) -> {
            Log.d(TAG, "initToolbarClickListener: pre " + monthPager.getCurrentPosition());
            Log.d(TAG, "initToolbarClickListener: pre " + (monthPager.getCurrentPosition() - 1));
            monthPager.setCurrentItem(monthPager.getCurrentPosition() - 1);
        });

        mNext.setOnClickListener((View view) -> {
            Log.d(TAG, "initToolbarClickListener: next " + monthPager.getCurrentPosition());
            Log.d(TAG, "initToolbarClickListener: next " + (monthPager.getCurrentPosition() + 1));
            monthPager.setCurrentItem(monthPager.getCurrentPosition() + 1);
        });

    }

    private void initMarkData() throws ParseException {
        HashMap<String, String> markData = new HashMap<>();
        for (int i = 0; i < dates.size(); i++) {
            markData.put(formateDateFromstring("yyyy-MM-dd", "yyyy-M-dd", dates.get(i).getDate()), "0");
        }
        calendarAdapter.setMarkData(markData);
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) throws ParseException {
        return new SimpleDateFormat(outputFormat, java.util.Locale.getDefault()).format(new SimpleDateFormat(inputFormat, java.util.Locale.getDefault()).parse(inputDate));
    }

    private void initMonthPager() {
        monthPager.setAdapter(calendarAdapter);
        monthPager.setCurrentItem(MonthPager.CURRENT_DAY_INDEX);
        monthPager.setPageTransformer(false, (page, position) -> {
            position = (float) Math.sqrt(1 - Math.abs(position));
            page.setAlpha(position);
        });
        monthPager.addOnPageChangeListener(new MonthPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentCalendars = calendarAdapter.getPagers();
                if (currentCalendars.get(position % currentCalendars.size()) != null) {
                    CalendarDate date = currentCalendars.get(position % currentCalendars.size()).getSeedDate();
                    currentDate = date;
                    if (selectedMonth != date.getMonth()) {
                        java.util.Calendar cal = java.util.Calendar.getInstance();
                        cal.set(date.getYear(), date.getMonth() - 1, date.getDay());
                        showProgress(true);
                        new CallRequest(DashbordFragment.this).getDates(false, new SimpleDateFormat("MM/yyyy").format(cal.getTime()));

                    }
                    selectedMonth = date.getMonth();
                    mMonthName.setText(date.getMonthName() + " " + date.getYear());
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void refreshClickDate(CalendarDate date) {
        currentDate = date;
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.set(date.getYear(), date.getMonth() - 1, date.getDay());
           // getDataByDate(true, new SimpleDateFormat("yyyy/MM/dd").format(cal.getTime()));
      //  new CallRequest(DashbordFragment.this).getDataByDate(true, new SimpleDateFormat("yyyy/MM/dd").format(cal.getTime()));
        showProgress(true);
        new CallRequest(DashbordFragment.this).getDataByDate(false, new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime()));

    }

    @Override
    public void onDestroyView() {
      /*  if (subscriptionPaperList != null && !subscriptionPaperList.isUnsubscribed()) {
            subscriptionPaperList.unsubscribe();
            subscriptionPaperList = null;
        }*/
        super.onDestroyView();
    }


/*
    private void getDataByDate(boolean isShow, String date) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.Date, date);
        //showProgress(isShow);
        showProgress(true);
        Log.d(TAG, "getPaperByDate: " + map);
        subscriptionPaperList = NetworkRequest.performAsyncRequest(restApi.getDataListByDate(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Log.d(TAG, "getPaperByDateresponse: " + jsonResponse.toString());

                    dashboardmainmodel.clear();
                    //dates.clear();
                     dashboardmainmodel = LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), DashboardMainModel.class);
                    dashBoardDetailsAdpater = new DashBoardDetailsAdpater(getActivity(), dashboardmainmodel);
                    mDataList.setAdapter(dashBoardDetailsAdpater);

                   // paperBeanList.addAll(dashboardmainmodel.getPaperInfo());
                    //dates.addAll(dashboardmainmodel.getDates());
                  *//*  if (dates.size() > 0)
                        initMarkData();*//*
                   // dashBoardDetailsAdpater.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getActivity(), data);
            }

        }, e -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

  */


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        JSONObject jObj;
        Log.d(TAG, "onTaskCompleted: "+result);
        if (result != null && !result.isEmpty()) {
            switch (request) {
                case getDate:
                    try {
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            dates.clear();
                            JSONArray jData = jObj.getJSONArray("data");
                            Log.d(TAG, "onTaskCompleted: " + jData.length());
                            if (jData.length() > 0) {
                                for (int i = 0; i < jData.length(); i++) {
                                    PaperDates pprdate=new PaperDates();
                                    pprdate.setDate(jData.getJSONObject(i).getString("Date"));
                                   // dates.add((PaperDates) jParser.parseJson(jData.getJSONObject(i), new PaperDates()));
                                    dates.add(pprdate);
                                }
                            }
                            Log.d(TAG, "onTaskCompleted: "+dates.size());
                            if (dates.size() > 0)
                                initMarkData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                    case getDataByDate:
                    try {
                        Log.d(TAG, "onTaskCompletedDash " +result);

                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Log.d(TAG, "onTaskCompletedDash " +result);

                            JSONArray lession = jObj.getJSONArray("lesson_planner");
                            JSONArray testassign = jObj.getJSONArray("planner_and_cct");

                            dashboardmainmodel.clear();
                            List<AssignedData> lessionList=new ArrayList();
                            List<AssignedData> testassignList=new ArrayList();

                            if (testassign.length() > 0) {
                                for (int i = 0; i < testassign.length(); i++) {

                                    AssignedData assignedData=new AssignedData();
                                    assignedData.setClassTestPlannerID(testassign.getJSONObject(i).getString("ClassTestPlannerID"));
                                    assignedData.setClassCCTPlannerID(testassign.getJSONObject(i).getString("ClassCCTPlannerID"));
                                    assignedData.setClassMCQTestHDRID(testassign.getJSONObject(i).getString("ClassMCQTestHDRID"));
                                    assignedData.setCreatedOn(testassign.getJSONObject(i).getString("CreatedOn"));
                                    assignedData.setAssignDate(testassign.getJSONObject(i).getString("AssignDate"));
                                    assignedData.setPaperDate(testassign.getJSONObject(i).getString("PaperDate"));
                                    assignedData.setPaperMark(testassign.getJSONObject(i).getString("PaperMark"));
                                    assignedData.setBoardName(testassign.getJSONObject(i).getString("BoardName"));
                                    assignedData.setMediumName(testassign.getJSONObject(i).getString("MediumName"));
                                    assignedData.setStandardID(testassign.getJSONObject(i).getString("StandardID"));
                                    assignedData.setStandardName(testassign.getJSONObject(i).getString("StandardName"));
                                    assignedData.setSubjectID(testassign.getJSONObject(i).getString("SubjectID"));
                                    assignedData.setSubjectName(testassign.getJSONObject(i).getString("SubjectName"));
                                    assignedData.setAssignType(testassign.getJSONObject(i).getString("AssignType"));
                                    assignedData.setFirst_name(testassign.getJSONObject(i).getString("first_name"));
                                    assignedData.setLast_name(testassign.getJSONObject(i).getString("last_name"));
                                    assignedData.setStatus(testassign.getJSONObject(i).getString("Status"));
                                    assignedData.setPaperStatus(testassign.getJSONObject(i).getString("PaperStatus"));
                                    assignedData.setSubmissionDate(testassign.getJSONObject(i).getString("SubmissionDate"));
                                    assignedData.setClassQuestionPaperID(testassign.getJSONObject(i).getString("ClassQuestionPaperID"));
                                    assignedData.setType(testassign.getJSONObject(i).getString("Type"));
                                    testassignList.add(assignedData);
                                }
                            }
                            if (lession.length() > 0) {
                                for (int i = 0; i < lession.length(); i++) {

                                    AssignedData assignedData=new AssignedData();
                                    assignedData.setClassLessonPlannerID(lession.getJSONObject(i).getString("ClassLessonPlannerID"));
                                    assignedData.setClassID(lession.getJSONObject(i).getString("ClassID"));
                                    assignedData.setClassName(lession.getJSONObject(i).getString("ClassName"));
                                    assignedData.setTeacherID(lession.getJSONObject(i).getString("TeacherID"));
                                    assignedData.setSubjectID(lession.getJSONObject(i).getString("SubjectID"));
                                    assignedData.setBranchID(lession.getJSONObject(i).getString("BranchID"));
                                    assignedData.setBatchID(lession.getJSONObject(i).getString("BatchID"));
                                    assignedData.setDuration(lession.getJSONObject(i).getString("Duration"));
                                    assignedData.setStatus(lession.getJSONObject(i).getString("Status"));
                                    assignedData.setPaperDate(lession.getJSONObject(i).getString("PaperDate"));
                                    assignedData.setCreatedBy(lession.getJSONObject(i).getString("CreatedBy"));
                                    assignedData.setCreatedOn(lession.getJSONObject(i).getString("CreatedOn"));
                                    assignedData.setModifiedBy(lession.getJSONObject(i).getString("ModifiedBy"));
                                    assignedData.setModifiedOn(lession.getJSONObject(i).getString("ModifiedOn"));
                                    assignedData.setSubjectName(lession.getJSONObject(i).getString("SubjectName"));
                                    assignedData.setBoardName(lession.getJSONObject(i).getString("BoardName"));
                                    assignedData.setMediumName(lession.getJSONObject(i).getString("MediumName"));
                                    assignedData.setStandardName(lession.getJSONObject(i).getString("StandardName"));
                                    assignedData.setTime(lession.getJSONObject(i).getString("Time"));
                                    assignedData.setLectureStatus(lession.getJSONObject(i).getString("LectureStatus"));
                                    assignedData.setHomeWork(lession.getJSONObject(i).getString("HomeWork"));
                                    assignedData.setAssignLactureLink(lession.getJSONObject(i).getString("AssignLactureLink"));
                                    lessionList.add(assignedData);
                                }
                            }


                            if(testassignList.size()==0&&lessionList.size()==0){
                                tv_notfound.setVisibility(View.VISIBLE);
                                mDataList.setVisibility(View.GONE);
                            }else{
                                tv_notfound.setVisibility(View.GONE);
                                mDataList.setVisibility(View.VISIBLE);
                                dashboardmainmodel.add(new DashboardMainModel("TEST ASSIGNED",testassignList));
                                dashboardmainmodel.add(new DashboardMainModel("LESSON ASSIGNED",lessionList));

                                dashBoardDetailsAdpater = new DashBoardDetailsAdpater(getActivity(), dashboardmainmodel);
                                mDataList.setAdapter(dashBoardDetailsAdpater);
                            }


                        }else{
                            tv_notfound.setVisibility(View.VISIBLE);
                            mDataList.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case readyForApproval:
                    try {
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }
    }
}
