package com.parshvaa.teacherapp.Models;


import java.io.Serializable;
import java.util.List;

public class LevelQuestion implements Serializable {

    int LevelQuestion;
    int MCQQuestionID;
    int AnswerID;
    int IsCorrect=-1;
    int Correct;
    int InCorrect =-1;
    int NotAttempt;
    String Question;
    List<OptionBean> Options;
    long QuestionMarkers;

    public int getLevelQuestion() {
        return LevelQuestion;
    }

    public void setLevelQuestion(int levelQuestion) {
        LevelQuestion = levelQuestion;
    }

    public int getMCQQuestionID() {
        return MCQQuestionID;
    }

    public void setMCQQuestionID(int MCQQuestionID) {
        this.MCQQuestionID = MCQQuestionID;
    }

    public int getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(int answerID) {
        AnswerID = answerID;
    }

    public int getIsCorrect() {
        return IsCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        IsCorrect = isCorrect;
    }

    public int getCorrect() {
        return Correct;
    }

    public void setCorrect(int correct) {
        Correct = correct;
    }

    public int getInCorrect() {
        return InCorrect;
    }

    public void setInCorrect(int inCorrect) {
        InCorrect = inCorrect;
    }

    public int getNotAttempt() {
        return NotAttempt;
    }

    public void setNotAttempt(int notAttempt) {
        NotAttempt = notAttempt;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public List<OptionBean> getOptions() {
        return Options;
    }

    public void setOptions(List<OptionBean> options) {
        Options = options;
    }

    public long getQuestionMarkers() {
        return QuestionMarkers;
    }

    public void setQuestionMarkers(long questionMarkers) {
        QuestionMarkers = questionMarkers;
    }
}

