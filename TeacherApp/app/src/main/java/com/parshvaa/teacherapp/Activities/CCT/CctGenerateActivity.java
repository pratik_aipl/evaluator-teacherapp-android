package com.parshvaa.teacherapp.Activities.CCT;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class CctGenerateActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "CctGenerateActivity";
    public String PlannerID = "", PaperID = "";
    public ImageView img_back;
    public TextView tv_title;
    public String go_back_url = "";
    WebView web_cct;
    boolean fromAssign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cct_generate);
        PlannerID = getIntent().getStringExtra("PlannerID");
        fromAssign = getIntent().getBooleanExtra(Constant.fromAssign, true);
        PaperID = getIntent().getStringExtra("PaperId");
        web_cct = findViewById(R.id.web_cct);
        web_cct.setWebViewClient(new myWebClient());
        Utils.setWebViewSettings(web_cct);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        showProgress(true);
        if (fromAssign) {
            tv_title.setText("CCT ASSIGNED PAPER");
            new CallRequest(CctGenerateActivity.this).generateCctPaperUrl(PlannerID, PaperID);
        } else {
            tv_title.setText("CCT GENERATED PAPER");
            new CallRequest(CctGenerateActivity.this).generatedCctUrls(PaperID);
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i(result, "RESULT");
            Log.d(TAG, "onTaskCompleted: " + result);
            switch (request) {
                case generateCctPaperUrl:
                    showProgress(false);
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            if (Utils.isNetworkAvailableWebView(CctGenerateActivity.this)) {
                                web_cct.loadUrl(jObj.getString("generate_paper_url"));
                            } else {
                                Utils.showNetworkAlert(CctGenerateActivity.this);
                            }
                            go_back_url = jObj.getString("go_back_url");
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        showProgress(false);
                        e.printStackTrace();
                    }
                    break;
            }

        }
    }

    @Override
    public void onBackPressed() {
        if (web_cct.canGoBack()) {
            if (Utils.isNetworkAvailableWebView(CctGenerateActivity.this)) {
                web_cct.goBack();
            } else {
                Utils.showNetworkAlert(CctGenerateActivity.this);
            }
        } else {
           /* Intent intent = new Intent(CctGenerateActivity.this, CctAssignedActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();*/
            super.onBackPressed();

        }
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgress(true);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            if (Utils.isNetworkAvailableWebView(CctGenerateActivity.this)) {
//                view.loadUrl(url);
//            } else {
//                Utils.showNetworkAlert(CctGenerateActivity.this);
//            }
            return false;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            showProgress(false);
            if (url.equalsIgnoreCase(go_back_url)) {
                Intent intent = new Intent(CctGenerateActivity.this, CctAssignedActivity.class);
                startActivity(intent);
                finish();
            }
            if (url.equalsIgnoreCase("https://test.pemevaluater.parshvaa.com/auth/login")) {
                Intent intent = new Intent(CctGenerateActivity.this, CctAssignedActivity.class);
                startActivity(intent);
                finish();

            }
        }
    }
}


