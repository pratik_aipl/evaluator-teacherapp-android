package com.parshvaa.teacherapp.Models;

import java.io.Serializable;

public class StudentMarks implements Serializable {

    int StudentID;
    String FirstName;
    String LastName;
    String StudentMark;

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getStudentMark() {
        return StudentMark;
    }

    public void setStudentMark(String studentMark) {
        StudentMark = studentMark;
    }
}
