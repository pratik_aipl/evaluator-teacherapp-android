package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.teacherapp.Models.NotificationList;
import com.parshvaa.teacherapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;


public class NotiAdapter extends RecyclerView.Adapter<NotiAdapter.MyViewHolder> {

    private List<NotificationList> notiList;
    public Context context;
    public int SubjectID;
    public String SubjectName;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_discription, tv_date;

        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_discription = (TextView) view.findViewById(R.id.tv_discription);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
        }
    }


    public NotiAdapter(List<NotificationList> notiList, Context context) {
        this.notiList = notiList;

        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_notification_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final NotificationList cons = notiList.get(position);
        holder.tv_title.setText(cons.getType());
        holder.tv_discription.setText(cons.getNotification());
        holder.tv_date.setText(printDifference(cons.getCreatedOn()));



    }


    @Override
    public int getItemCount() {
        return notiList.size();
    }


    public CharSequence printDifference(String start) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        long time = 0;

        try {
            time = sdf.parse(start).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long now = System.currentTimeMillis();

        CharSequence ago =
                DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
        return ago;
    }
}

