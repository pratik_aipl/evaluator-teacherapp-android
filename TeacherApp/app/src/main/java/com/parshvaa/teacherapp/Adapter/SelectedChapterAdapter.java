package com.parshvaa.teacherapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.Paper.PaperAssigned;
import com.parshvaa.teacherapp.Activities.ReadyReviewCct.ReadyCct;
import com.parshvaa.teacherapp.Activities.ReadyReviewCct.ReadyReviewGenCctActivity;
import com.parshvaa.teacherapp.Models.SelectedChapters;
import com.parshvaa.teacherapp.R;

import java.util.List;

public class SelectedChapterAdapter extends RecyclerView.Adapter<SelectedChapterAdapter.MyViewHolder> {

    private static final String TAG = "SelectedChapterAdapter";
    public Context context;
    private List<SelectedChapters> optList;

    public SelectedChapterAdapter(List<SelectedChapters> moviesList, Context context) {
        this.optList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_chapter_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SelectedChapters selectedChapters=optList.get(position);
        holder.tv_chapter_name.setText(selectedChapters.getChapterName());
    }

    @Override
    public int getItemCount() {
        return optList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_chapter_name;

        public MyViewHolder(View view) {
            super(view);
            tv_chapter_name = view.findViewById(R.id.tv_chapter_name);

        }
    }
}

