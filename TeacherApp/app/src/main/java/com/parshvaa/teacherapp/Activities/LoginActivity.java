package com.parshvaa.teacherapp.Activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.BuildConfig;
import com.parshvaa.teacherapp.DB.DBTables;
import com.parshvaa.teacherapp.DB.MyDBManager;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.MainUser;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends BaseActivity implements AsynchTaskListner {

    private static final String TAG = "LoginActivity";
    public LoginActivity instance;
    public Button btn_login;
    public EditText et_password, et_username;
    public TextView tv_forgot_pass;
    public String device_id = "";
    public String PleyaerID = "";
    public MyDBManager mDb;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA/*,
            Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS*/};
    int PERMISSION_ALL = 1;
    public JsonParserUniversal jParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (!Utils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        instance = LoginActivity.this;
        jParser = new JsonParserUniversal();

        mDb = MyDBManager.getInstance(this);
        mDb.open(this);

        device_id = Utils.getIMEInumber(instance);
        et_password = findViewById(R.id.et_password);
        et_username = findViewById(R.id.et_username);
        if (BuildConfig.DEBUG) {
            //LIVE
//            et_username.setText("C597248-T04");
//            et_password.setText("123456");
            //Local
            et_username.setText("C563942-T01");
            et_username.setText("C263974-T02");
            et_password.setText("123456");
        }
        tv_forgot_pass = findViewById(R.id.tv_forgot_pass);
        btn_login = findViewById(R.id.btn_login);
        PleyaerID = Utils.OneSignalPlearID();

        tv_forgot_pass.setOnClickListener(view -> {
            Intent intent = new Intent(instance, ForgotPasswordActivity.class);
            startActivity(intent);
        });

        btn_login.setOnClickListener(view -> {

            if (et_username.getText().toString().equals("")) {
                et_username.setError("Please enter valid email-id");
            } else if (et_password.getText().toString().equals("")) {
                et_password.setError("Please Enter Password");
            } else {
                if (Utils.isNetworkAvailable(instance)) {
                    if (PleyaerID.equalsIgnoreCase("")) {
                        PleyaerID = Utils.OneSignalPlearID();
                        showProgress(true);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Utils.removeWorkingDialog(instance);
                            }
                        }, 5000);
                    } else {
                        showProgress(true);
                        new CallRequest(instance).getLogin(et_username.getText().toString(), et_password.getText().toString(), device_id);
                    }
                }
            }


        });
    }

    public void removeAllTables() {
        mDb.removeTable("notification");
        DBTables dbTables = new DBTables();
        dbTables.createAll(mDb);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
                showProgress(false);
        if (result != null && !result.isEmpty()) {
            Log.i(result, "RESULT");

            switch (request) {
                case getLogin:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject jDAta = jObj.getJSONObject("data");
                            JSONObject jUser = jDAta.getJSONObject("user");
                            Utils.showToast(jObj.getString("message"), this);

                            mySharedPref.setIsLoggedIn(true);
                            mySharedPref.setLoginToken(jDAta.getString("login_token"));
                            mySharedPref.setClassId(jUser.getString("ClassID"));
                            Log.d(TAG, "getLoginToken: "+jDAta.getString("login_token"));

                            mainUser = (MainUser) jParser.parseJson(jUser, new MainUser());
                            Gson gson = new Gson();
                            String json = gson.toJson(mainUser);
                            mySharedPref.setUserModel(json);

                            mainUser = mainUser;
                            mySharedPref.setUserId(mainUser.getId());

                            Intent intent = new Intent(instance, DashboardActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Utils.showToast(jObj.getString("message"), this);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                  /*  if (jObj.getBoolean("status") == true) {
                        removeAllTables();

                        JSONObject jData = jObj.getJSONObject("data");
                        //  Utils.showToast(jObj.getString("message"), this);
                        App.user_id = jData.getString("user_id");
                        mySharedPref.setUserId(App.user_id);

                        Intent intent = new Intent(instance, DashboardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("Login", "Login");
                        intent.putExtra("PleyaerID", PleyaerID);
                        intent.putExtra("otp", jData.getString("otp"));
                        intent.putExtra("MobileNo", jData.getString("mobile"));
                        startActivity(intent);
                        finish();
                    } else {
                        Utils.showToast(jObj.getString("message"), this);
                    }*/
                    break;
            }
        }
    }

}
