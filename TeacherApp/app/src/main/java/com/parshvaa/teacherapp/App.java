package com.parshvaa.teacherapp;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.parshvaa.teacherapp.Activities.CCT.CctPaper;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssigned;
import com.parshvaa.teacherapp.Models.MainUser;
import com.parshvaa.teacherapp.Models.Standard;
import com.parshvaa.teacherapp.Models.UserDetail;
import com.parshvaa.teacherapp.Utils.MySharedPref;

import io.fabric.sdk.android.Fabric;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 10/12/2016.
 */
public class App extends MultiDexApplication {

    public static App app;
    public static App instance;
    public static String user_id = "";
   // public static String login_token = "";
    public  MySharedPref mySharedPref;
    public static String myPref = "teacherapp_pref";
    public  MainUser mainUser;
    public static UserDetail userDetail;
    public static Standard standard;
    public static ArrayList<PaperAssigned> paperAssignedArrayList = new ArrayList<>();
    public static ArrayList<CctPaper> cctPaperArrayList = new ArrayList<>();
    public static PaperAssigned paperAssigned;
    public static CctPaper cctPaper;
    public static boolean isNoti = false;
    public int NotificationID;
    Context mContext;
    public static String isType = "";
    public String bigPicture;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        Stetho.initializeWithDefaults(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        mContext = this;
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                        try {
                            isNoti = true;
                            JSONObject jsonObject = new JSONObject();
                            jsonObject = result.toJSONObject();
                            Log.i("One Signal", "==> " + result.stringify());
                            JSONObject jNoti = jsonObject.getJSONObject("notification");
                            JSONObject jpayload = jNoti.getJSONObject("payload");
                            NotificationID = jNoti.getInt("androidNotificationId");
                            JSONObject additionalData = jpayload.getJSONObject("additionalData");
                            if (additionalData.getString("Type").equalsIgnoreCase("Assigned Test Paper")) {
                                isType = "NewTestPlannerCreated";
                            } else if (additionalData.getString("Type").equalsIgnoreCase("Assigned CCT")) {
                                isType = "NewCctPlannerCreated";
                            } else if (additionalData.getString("Type").equalsIgnoreCase("Assigned Paper For Review")) {
                                isType = "GeneratedPaperAssigned";
                            } else if (additionalData.getString("Type").equalsIgnoreCase(" Assigned CCT For Review")) {
                                isType = "GeneratedCCTAssigned ";
                            } else if (additionalData.getString("Type").equalsIgnoreCase("zooki")) {
                                isType = "zooki";
                            } else {
                                isType = "1";
                            }
                            try {
                                bigPicture = jpayload.getString("bigPicture");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent(mContext, SplashActivity.class);
                          //  intent.putExtra("Notification_type_id", additionalData.getString("notification_type_id"));
                          //  intent.putExtra("paper_type", additionalData.getString("paper_type"));
                          //  intent.putExtra("NotificationID", NotificationID);
                            intent.putExtra("bigPicture", bigPicture);
                            intent.putExtra("title", jpayload.getString("title"));
                            intent.putExtra("created_on", additionalData.getString("CreatedOn"));
                            intent.putExtra("body", jpayload.getString("body"));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {
                    }
                })
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();
        try {
            mySharedPref = new MySharedPref(instance);
            app = this;
            Class.forName("android.os.AsyncTask");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public App() {
        setInstance(this);
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    public static App getInstance() {
        return instance;
    }
}
