package com.parshvaa.teacherapp.Activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.Utils;

public class ConcernActivity extends AppCompatActivity {

    public Button btn_next;
    public ConcernActivity instance;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            /*  Manifest.permission.RECORD_AUDIO,Manifest.permission.SYSTEM_ALERT_WINDOW,
              Manifest.permission.CAMERA,
              Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS*/};
    int PERMISSION_ALL = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_concern);
        if (!Utils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);

        }
        instance = this;
        btn_next = findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(instance, TourActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.rotate_in, R.anim.rotate_out);
                finish();

            }
        });
    }
}
