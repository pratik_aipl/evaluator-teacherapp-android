package com.parshvaa.teacherapp.Models;

import java.io.Serializable;

public class Subject implements Serializable {
    public String SubjectID = "";

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String SubjectName = "";
}
