package com.parshvaa.teacherapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.teacherapp.Adapter.ReportAttendAdapter;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Interface.StudentOnClick;
import com.parshvaa.teacherapp.Models.AttendStudent;
import com.parshvaa.teacherapp.Models.LevelQuestion;
import com.parshvaa.teacherapp.Models.OptionBean;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportStudentActivity extends BaseActivity implements AsynchTaskListner, StudentOnClick {

    private static final String TAG = "ReportStudentActivity";
    @BindView(R.id.img_back)
    ImageView mBackBtn;
    @BindView(R.id.tv_title)
    TextView mPageTitle;

    @BindView(R.id.mStudent)
    TextView mStudent;
    @BindView(R.id.mStudentViewLine)
    View mStudentViewLine;
    @BindView(R.id.mNotAttempt)
    TextView mNotAttemptStudent;
    @BindView(R.id.mComplete)
    TextView mComplete;
    @BindView(R.id.mCompleteLabel)
    TextView mCompleteLabel;
    @BindView(R.id.mNotCompleteLabel)
    TextView mNotCompleteLabel;
    @BindView(R.id.mQuestionsViewLine)
    View mQuestionsViewLine;
    @BindView(R.id.mAttemptStudents)
    RecyclerView mAttemptStudents;
    @BindView(R.id.mNotAttemptStudents)
    RecyclerView mNotAttemptStudents;

    List<AttendStudent> attendStudentList = new ArrayList<>();
    List<AttendStudent> notAttendStudentList = new ArrayList<>();

    String CLASSMCQTESTHDRID;
    @BindView(R.id.tv_attemt_zero)
    TextView tvAttemtZero;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_student);
        ButterKnife.bind(this);

        mPageTitle.setText("Student");
        CLASSMCQTESTHDRID = getIntent().getStringExtra("CLASSMCQTESTHDRID");
        Log.d(TAG, "IDD: " + CLASSMCQTESTHDRID);


        if (Utils.isNetworkAvailable(ReportStudentActivity.this)) {
            showProgress(true);
            new CallRequest(ReportStudentActivity.this).getstudentwisecctreport(CLASSMCQTESTHDRID);
        }

     /*   ReportAttendAdapter reportAttendAdapter = new ReportAttendAdapter(this, attendStudentList);
        mAttemptStudents.setAdapter(reportAttendAdapter);

        ReportNotAttendAdapter reportNotAttendAdapter = new ReportNotAttendAdapter(this, notAttendStudentList);
        mNotAttemptStudents.setAdapter(reportNotAttendAdapter);*/

        mBackBtn.setOnClickListener(view -> onBackPressed());
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case getstudentwisecctreport:
                        jObj = new JSONObject(result);

                        Log.d(TAG, "onTaskCompleted:attemp " + result);
                        if (jObj.getBoolean("status")) {
                            JSONObject data = jObj.getJSONObject("Data");

                            mNotAttemptStudent.setText("" + data.getString("StudentNotCompleted"));
                            mComplete.setText("" + data.getString("StudentCompletedCount"));


                            JSONArray complete = data.getJSONArray("StudentCompleted");
                            for (int i = 0; i < complete.length(); i++) {
                                JSONObject detail = complete.getJSONObject(i);
                                AttendStudent attendStudent = new AttendStudent();
                                // attendStudent = (AttendStudent) jParser.parseJson(complete.getJSONObject(i), new AttendStudent());
                                attendStudent.setFirstName(detail.getString("StudentName"));
                                //  attendStudent.setLastName(detail.getString("LastName"));
                                attendStudent.setCorrect(detail.getInt("CorrectQuestion"));
                                attendStudent.setIncorrect(detail.getInt("IncorectQuestion"));
                                attendStudent.setTotalQquestion(detail.getInt("TotalQuestion"));
                                attendStudent.setTotalAttempt(detail.getInt("NotAttemptQuestion"));
                                attendStudent.setAvgTime(detail.getString("AvgTime"));

                                JSONArray Question=detail.getJSONArray("Question");
                                List<LevelQuestion> questlist=new ArrayList<>();
                                for (int j = 0; j <Question.length() ; j++) {
                                    JSONObject questionobject=Question.getJSONObject(j).getJSONObject("Question");
                                    LevelQuestion levelQuestion=new LevelQuestion();

                                    levelQuestion.setQuestion(questionobject.getString("Question"));
                                    levelQuestion.setAnswerID(Question.getJSONObject(j).getInt("StudentAnsID"));

                                    JSONArray Opntion=questionobject.getJSONArray("Question_option");
                                    List<OptionBean> optionlist=new ArrayList<>();
                                    int ansId = 0;
                                    for (int k = 0; k <Opntion.length() ; k++) {
                                        JSONObject optionobject=Opntion.getJSONObject(k);
                                        OptionBean optionBean=new OptionBean();

                                        optionBean.setOptions(optionobject.getString("Options"));
                                        optionBean.setMCQOptionID(optionobject.getInt("MCQOPtionID"));
                                        optionBean.setOptions(optionobject.getString("Options"));
                                        optionBean.setIsCorrect(optionobject.getString("isCorrect"));
                                        if(optionobject.getInt("isCorrect") == 1){
                                            ansId=optionobject.getInt("MCQOPtionID");
                                        }
                                        optionlist.add(optionBean);
                                    }
                                    levelQuestion.setIsCorrect(Question.getJSONObject(j).getInt("StudentAnsID") == ansId ? 1:0);
                                    levelQuestion.setOptions(optionlist);

                                    questlist.add(levelQuestion);
                                }

                                attendStudent.setQuestion(questlist);
                                attendStudent.setTotalAttempt(detail.getInt("NotAttemptQuestion"));
                                attendStudentList.add(attendStudent);
                            }

                            Log.d(TAG, "Student Detail>>>>: "+attendStudentList.get(0).getQuestion().size());

                            ReportAttendAdapter reportAttendAdapter = new ReportAttendAdapter(this, attendStudentList);
                            mAttemptStudents.setAdapter(reportAttendAdapter);

                            if (attendStudentList.size() == 0) {
                                tvAttemtZero.setVisibility(View.VISIBLE);
                                mAttemptStudents.setVisibility(View.GONE);
                            }else{
                                tvAttemtZero.setVisibility(View.GONE);
                                mAttemptStudents.setVisibility(View.VISIBLE);
                            }

                            /*JSONArray notcomplete = jObj.getJSONArray("student_incomplete_data");
                            for (int j = 0; j < notcomplete.length(); j++) {
                                JSONObject notdetail = notcomplete.getJSONObject(j);
                                AttendStudent notAttendStudent = new AttendStudent();

                                notAttendStudent.setFirstName(notdetail.getString("FirstName"));
                                notAttendStudent.setLastName(notdetail.getString("LastName"));
                                notAttendStudentList.add(notAttendStudent);
                            }

                            ReportNotAttendAdapter reportNotAttendAdapter = new ReportNotAttendAdapter(this, notAttendStudentList);
                            mNotAttemptStudents.setAdapter(reportNotAttendAdapter);*/

                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void callbackStudent(int pos, AttendStudent attendStudent) {
        startActivity(new Intent(this, ReportStudentDetailsActivity.class)
               // .putExtra(Constant.PlannerID, this.paperBean.getMCQPlannerID())
                .putExtra(Constant.PlannerID, CLASSMCQTESTHDRID)
                .putExtra(Constant.StudentID, attendStudent));
    }
  /*  private void showView(boolean isAttend) {
        if (isAttend) {
            mCompleteLabel.setVisibility(View.VISIBLE);
            mAttemptStudents.setVisibility(View.VISIBLE);
            mNotCompleteLabel.setVisibility(View.GONE);
            mNotAttemptStudents.setVisibility(View.GONE);
        } else {
            mCompleteLabel.setVisibility(View.GONE);
            mAttemptStudents.setVisibility(View.GONE);
            mNotCompleteLabel.setVisibility(View.VISIBLE);
            mNotAttemptStudents.setVisibility(View.VISIBLE);
        }
    }
*/

}
