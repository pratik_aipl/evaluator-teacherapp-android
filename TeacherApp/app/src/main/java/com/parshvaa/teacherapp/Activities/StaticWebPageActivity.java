package com.parshvaa.teacherapp.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.teacherapp.R;

public class StaticWebPageActivity extends AppCompatActivity {
    public StaticWebPageActivity indtance;
    public ImageView img_back;
    public TextView tv_title;
    public String action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_web_page);

        indtance = this;
        action = getIntent().getExtras().getString("action");
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        WebView webView = (WebView) findViewById(R.id.web_legal);

        if (action.equalsIgnoreCase("l")) {
            tv_title.setText(R.string.Licenses);
            webView.loadUrl("file:///android_asset/Licenses&Copyrights.html");
        } else if (action.equalsIgnoreCase("t")) {
            tv_title.setText(R.string.termsAndCondition);
            webView.loadUrl("file:///android_asset/Terms&Conditions.html");
        } else if (action.equalsIgnoreCase("p")) {
            tv_title.setText(R.string.privacy);
            webView.loadUrl("file:///android_asset/privacy-policy.html");
        }
    }
}
