package com.parshvaa.teacherapp.Models;


public class LevelBean {

    int LevelID;
    String LevelName;
    int NoOfQuestions;
    int TotalCorrect;
    int TotalStudents;
    int TotalAvarage;

    public LevelBean(int levelID, String levelName, int noOfQuestions, int totalCorrect, int totalStudents, int totalAvarage) {
        LevelID = levelID;
        LevelName = levelName;
        NoOfQuestions = noOfQuestions;
        TotalCorrect = totalCorrect;
        TotalStudents = totalStudents;
        TotalAvarage = totalAvarage;
    }

    public int getTotalCorrect() {
        return TotalCorrect;
    }

    public void setTotalCorrect(int totalCorrect) {
        TotalCorrect = totalCorrect;
    }

    public int getTotalStudents() {
        return TotalStudents;
    }

    public void setTotalStudents(int totalStudents) {
        TotalStudents = totalStudents;
    }

    public int getTotalAvarage() {
        return TotalAvarage;
    }

    public void setTotalAvarage(int totalAvarage) {
        TotalAvarage = totalAvarage;
    }

    public int getLevelID() {
        return LevelID;
    }

    public void setLevelID(int levelID) {
        LevelID = levelID;
    }

    public String getLevelName() {
        return LevelName;
    }

    public void setLevelName(String levelName) {
        LevelName = levelName;
    }

    public int getNoOfQuestions() {
        return NoOfQuestions;
    }

    public void setNoOfQuestions(int noOfQuestions) {
        NoOfQuestions = noOfQuestions;
    }
}
