package com.parshvaa.teacherapp.Models;

import java.io.Serializable;


public class NotificationList implements Serializable {

    public String CNID = "";
    public String Role = "";

    public String getCNID() {
        return CNID;
    }

    public void setCNID(String CNID) {
        this.CNID = CNID;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getNotification() {
        return Notification;
    }

    public void setNotification(String notification) {
        Notification = notification;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getIsView() {
        return IsView;
    }

    public void setIsView(String isView) {
        IsView = isView;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String UserID = "";
    public String Notification = "";
    public String Link = "";
    public String Type = "";
    public String IsView = "";
    public String CreatedBy = "";
    public String CreatedOn = "";
}
