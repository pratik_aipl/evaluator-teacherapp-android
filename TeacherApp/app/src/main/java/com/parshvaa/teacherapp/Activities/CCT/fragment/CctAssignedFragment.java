package com.parshvaa.teacherapp.Activities.CCT.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.CCT.CctAssignedAdapter;
import com.parshvaa.teacherapp.Activities.CCT.CctPaper;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssigned;
import com.parshvaa.teacherapp.Activities.SendApproval.Adapter.SendPaperAdapter;
import com.parshvaa.teacherapp.App;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.Standard;
import com.parshvaa.teacherapp.ObservScroll.BaseFragment;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class CctAssignedFragment extends BaseFragment implements AsynchTaskListner {
    private static final String TAG = "CctAssignedFragment";
    public RecyclerView rv_gen_paper;
    public PaperAssigned paperAssigneds;
    public JsonParserUniversal jParser;
    public LinearLayout empty_view;
    public TextView tv_empty;
    public Standard standard;
    public String standard_id = "", subject_id = "", assign_date = "", exam_date = "";
    public ArrayList<CctPaper> cctPaperArrayList = new ArrayList<>();
    public CctAssignedAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_paper, container, false);
        jParser = new JsonParserUniversal();
        rv_gen_paper = v.findViewById(R.id.rv_gen_paper);
        tv_empty = (TextView) v.findViewById(R.id.tv_empty);
        empty_view = v.findViewById(R.id.empty_view);
        Log.d(TAG, "onCreateView: "+(getActivity() !=null));
//        new CallRequest(getActivity()).assignedCctPaper(standard_id, subject_id, assign_date);
        showProgress(true);
        new CallRequest(CctAssignedFragment.this).assignedCctPaperft("", "", "");

        return v;
    }

    public void setupRecyclerView() {
        Context context = rv_gen_paper.getContext();
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rv_gen_paper.setLayoutAnimation(controller);
        rv_gen_paper.scheduleLayoutAnimation();
        rv_gen_paper.setLayoutManager(new LinearLayoutManager(context));
        Log.d(TAG, "setupRecyclerView: " + cctPaperArrayList.size());
        adapter = new CctAssignedAdapter(cctPaperArrayList, context, "Assign");
        rv_gen_paper.setAdapter(adapter);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        JSONObject jObj;
        Log.d(TAG, "onTaskCompleted: "+result);
        if (result != null && !result.isEmpty()) {
            switch (request) {
                case assignedCctPaper:
                    try {
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            cctPaperArrayList.clear();
                            JSONArray jData = jObj.getJSONArray("assigned_cct_paper");
                            Log.d(TAG, "onTaskCompleted: " + jData.length());
                            if (jData.length() > 0) {
                                for (int i = 0; i < jData.length(); i++) {
                                    cctPaperArrayList.add((CctPaper) jParser.parseJson(jData.getJSONObject(i), new CctPaper()));
                                }
                                Log.d(TAG, "onTaskCompleted: " + cctPaperArrayList.size());
                                setupRecyclerView();
                                empty_view.setVisibility(View.GONE);
                            } else {
                                empty_view.setVisibility(View.VISIBLE);
                                tv_empty.setText("No CCT Assigned by Institute");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case readyForApproval:
                    try {
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            App.paperAssignedArrayList.clear();
                            App.cctPaperArrayList.clear();
                            JSONArray jData = jObj.getJSONArray("assigned_paper");
                            if (jData.length() > 0 || jData != null) {
                                for (int i = 0; i < jData.length(); i++) {
                                    paperAssigneds = (PaperAssigned) jParser.parseJson(jData.getJSONObject(i), new PaperAssigned());
                                    App.paperAssignedArrayList.add(paperAssigneds);
                                }
                                rv_gen_paper.setVisibility(View.VISIBLE);
                                setupRecyclerView();
                                empty_view.setVisibility(View.GONE);

                            } else {
                                empty_view.setVisibility(View.VISIBLE);
                                rv_gen_paper.setVisibility(View.GONE);
                                tv_empty.setText("No Data");

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }
    }
}
