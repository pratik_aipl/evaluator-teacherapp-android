package com.parshvaa.teacherapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parshvaa.teacherapp.Models.LevelBean;
import com.parshvaa.teacherapp.R;

import java.util.List;


public class LevelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<LevelBean> testBeans;
    int size = 0;
    // LevelOnClick levelOnClick;
    boolean fromPaperScreen;


    public LevelAdapter(Context context, List<LevelBean> testBeans) {
        this.context = context;
        this.testBeans = testBeans;
    }

 /*   public LevelAdapter(ReportActivity context, List<LevelBean> testBeans, boolean fromPaperScreen) {
        this.context = context;
        this.testBeans = testBeans;
        this.levelOnClick = (LevelOnClick) context;
        this.fromPaperScreen = fromPaperScreen;
    }*/

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolderBatch(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_batch_perform_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        LevelBean levelBean = testBeans.get(position);
        ViewHolderBatch holder = (ViewHolderBatch) holderIn;

        holder.mLevelName.setText(levelBean.getLevelName());
        //  holder.mLevelValue.setText(levelBean.getTotalCorrect() + "-" + levelBean.getNoOfQuestions());
        holder.mPerformance.setText(levelBean.getTotalAvarage() + " %");
        //holder.mLevelProgress.setProgress();

        if (position == (testBeans.size() -1)) {
            holder.mLine.setVisibility(View.GONE);
        } else {
            holder.mLine.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        // return 3;
        return size == 0 ? testBeans.size() : size;
    }


    public static class ViewHolderBatch extends RecyclerView.ViewHolder {

        TextView mLevelName, mPerformance;
        View mLine;
        ProgressBar mLevelProgress;

        public ViewHolderBatch(View v) {
            super(v);

            mLevelName = v.findViewById(R.id.mLevelName);
            mPerformance = v.findViewById(R.id.mPerformance);
            mLine = v.findViewById(R.id.mLineraw);
            mLevelProgress = v.findViewById(R.id.mLevelProgress);
        }
    }

}
