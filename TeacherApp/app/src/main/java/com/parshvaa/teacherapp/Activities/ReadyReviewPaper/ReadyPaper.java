package com.parshvaa.teacherapp.Activities.ReadyReviewPaper;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 6/12/2018.
 */

public class ReadyPaper implements Serializable {
    public String ClassQuestionPaperID = "";
    public String AssignDate = "";
    public String PaperDate = "";
    public String PaperMark = "";
    public String BoardName = "";
    public String MediumName = "";
    public String StandardID = "";
    public String StandardName = "";
    public String SubjectID = "";
    public String SubjectName = "";
    public String PaperStatus = "";

    public String getClassQuestionPaperID() {
        return ClassQuestionPaperID;
    }

    public void setClassQuestionPaperID(String classQuestionPaperID) {
        ClassQuestionPaperID = classQuestionPaperID;
    }

    public String getAssignDate() {
        return AssignDate;
    }

    public void setAssignDate(String assignDate) {
        AssignDate = assignDate;
    }

    public String getPaperDate() {
        return PaperDate;
    }

    public void setPaperDate(String paperDate) {
        PaperDate = paperDate;
    }

    public String getPaperMark() {
        return PaperMark;
    }

    public void setPaperMark(String paperMark) {
        PaperMark = paperMark;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getPaperStatus() {
        return PaperStatus;
    }

    public void setPaperStatus(String paperStatus) {
        PaperStatus = paperStatus;
    }
}
