package com.parshvaa.teacherapp.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.teacherapp.Activities.Paper.PaperAssigned;
import com.parshvaa.teacherapp.Activities.Paper.PaperAssignedAdapter;
import com.parshvaa.teacherapp.Adapter.NotiAdapter;
import com.parshvaa.teacherapp.Interface.AsynchTaskListner;
import com.parshvaa.teacherapp.Models.NotificationList;
import com.parshvaa.teacherapp.Models.Standard;
import com.parshvaa.teacherapp.Models.Subject;
import com.parshvaa.teacherapp.ObservScroll.BaseActivity;
import com.parshvaa.teacherapp.R;
import com.parshvaa.teacherapp.Utils.CallRequest;
import com.parshvaa.teacherapp.Utils.Constant;
import com.parshvaa.teacherapp.Utils.JsonParserUniversal;
import com.parshvaa.teacherapp.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class NotificationActivity extends BaseActivity implements AsynchTaskListner {

    public NotificationActivity instance;
    public ImageView img_back;
    public TextView tv_title, tv_empty;
    public RecyclerView rv_notification;
    public NotiAdapter adapter;
    public ArrayList<NotificationList> notificationArrayList = new ArrayList<>();
    public NotificationList notificationListl;
    public JsonParserUniversal jParser;
    public LinearLayout empty_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        instance = this;
        jParser = new JsonParserUniversal();
        rv_notification = (RecyclerView) findViewById(R.id.rv_notification);
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_empty = (TextView) findViewById(R.id.tv_empty);
        empty_view = findViewById(R.id.empty_view);
        tv_title.setText("NOTIFICATION");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        showProgress(true);
        new CallRequest(instance).getNotifition();
    }

    public void setupRecyclerView() {
        final Context context = rv_notification.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rv_notification.setLayoutAnimation(controller);
        rv_notification.scheduleLayoutAnimation();
        rv_notification.setLayoutManager(new LinearLayoutManager(context));
        Collections.reverse(notificationArrayList);
        adapter = new NotiAdapter(notificationArrayList, context);
        rv_notification.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case getNotifition:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            notificationArrayList.clear();
                            JSONArray jData = jObj.getJSONArray("data");
                            if (jData.length() > 0 && jData != null) {
                                for (int i = 0; i < jData.length(); i++) {
                                    notificationListl = (NotificationList) jParser.parseJson(jData.getJSONObject(i), new NotificationList());
                                    notificationArrayList.add(notificationListl);
                                }
                                setupRecyclerView();
                                rv_notification.setVisibility(View.VISIBLE);
                                empty_view.setVisibility(View.GONE);

                            } else {
                                empty_view.setVisibility(View.VISIBLE);
                                rv_notification.setVisibility(View.GONE);
                                tv_empty.setText("No Data");
                            }
                        }
                        break;


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
